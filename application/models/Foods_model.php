<?php

class Foods_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    //////////////save detail///////////
    function save_detail($data) {
//        pre($data);

        $this->db->update("foods", array("detail" => mullti_lang_todb($data->detail), 'details' => serialize($data->details),'zone'=> serialize($data->zone), 'stock' => serialize($data->stock), 'cooks' => serialize($data->cooks), 'no_cook' => $data->no_cook, 'prices' => serialize($data->prices),'show_cus'=>$data->show_cus, 'unit_sale' => $data->unit_sale, 'toppings' => serialize($data->toppings)), array('f_id' => $data->f_id, 'id_res_auto' => res_id()));
//        pre($data);
        $this->edit_tag($data->tags, $data->f_id);
//        $this->edit_size($data->size, $data->f_id);
//        $this->edit_option($data->options, $data->f_id);
//        $this->edit_extra($data->extra, $data->f_id);
//        $this->edit_pic($data->pic, $data->f_id);
//        $this->edit_cooks($data->cooks, $data->f_id);
        return 1;
    }

    function edit_tag($tags, $f_id) {
        $have = array();
        foreach ($tags as $s) {
            $have[] = $s;
            $a = $this->db->get_where("food_tag_pk", array('f_id' => $f_id, 'ft_id' => $s, 'id_res_auto' => res_id()));
            if ($a->num_rows() == 0) {
                $this->db->set('c_date', 'NOW()', FALSE);
                $this->db->insert("food_tag_pk", array('f_id' => $f_id, 'ft_id' => $s, 'id_res_auto' => res_id()));
                $have[] = $this->db->insert_id();
            }
        }
        ///////////delete no not
        if (count($have) > 0) {
            $this->db->where_not_in('ft_id', $have);
        }
        $this->db->where("id_res_auto", res_id());
        $this->db->where("f_id", $f_id);
        $this->db->delete('food_tag_pk');
    }

    function edit_cooks($cooks, $f_id) {
        ////////////////////////////
    }

    function edit_pic($pic, $f_id) {
        $have = array();
        foreach ($pic as $s) {
            $have[] = $s;
            $a = $this->db->get_where("food_pic", array('f_id' => $f_id, 'pic_id' => $s, 'id_res_auto' => res_id()));
            if ($a->num_rows() == 0) {
                $this->db->insert("food_pic", array('f_id' => $f_id, 'pic_id' => $s, 'id_res_auto' => res_id()));
                $have[] = $this->db->insert_id();
            }
        }
        ///////////delete no not
        if (count($have)) {
            $this->db->where_not_in('pic_id', $have);
        }
        $this->db->where("id_res_auto", res_id());
        $this->db->where("f_id", $f_id);
        $this->db->delete('food_pic');
    }

    function edit_extra($extra, $f_id) {
        $have = array();
        foreach ($extra as $s) {
            if (!isset($s->default_val)) {
                $s->default_val = 0;
            }

            if (isset($s->sf_id) && $s->sf_id - 0 !== 0) {//Edit
                $have[] = $s->sf_id;
                $this->db->update("food_extra", array("name" => mullti_lang_todb($s->name), 'default_val' => $s->default_val, 'price' => $s->price), array('sf_id' => $s->sf_id));
            } else {//Add(new)
                $this->db->set('c_date', 'NOW()', FALSE);
                $this->db->insert("food_extra", array(
                    'name' => mullti_lang_todb($s->name),
                    'f_id' => $f_id,
                    'id_res_auto' => res_id(),
                    'price' => $s->price,
                    'default_val' => $s->default_val,
                    'sort' => $this->get_sort_extra($f_id)
                ));
                $have[] = $this->db->insert_id();
            }
        }
        ///////////delete no not
        if (count($have) > 0) {
            $this->db->where_not_in('sf_id', $have);
        }
        $this->db->where("id_res_auto", res_id());
        $this->db->where("f_id", $f_id);
        $this->db->delete('food_extra');
    }

    function get_sort_extra($f_id) {
        $a = $this->db->select_max("sort")->get_where("food_extra", array("f_id" => $f_id, 'id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            return $a->row()->sort - 0 + 1;
        } else {
            return 1;
        }
    }

    function edit_option($option, $f_id) {
        $have = array();
        foreach ($option as $s) {
            if (!isset($s->default_val)) {
                $s->default_val = 0;
            }
            if ($s->id - 0 !== 0) {//Edit
                $have[] = $s->id;
                $this->db->update("food_option", array("name" => mullti_lang_todb($s->name), 'default_val' => $s->default_val - 0), array('sf_id' => $s->id));
            } else {//Add(new)
                $this->db->set('c_date', 'NOW()', FALSE);
                $this->db->insert("food_option", array(
                    'name' => mullti_lang_todb($s->name),
                    'f_id' => $f_id,
                    'id_res_auto' => res_id(),
                    'default_val' => $s->default_val - 0,
                    'sort' => $this->get_sort_option($f_id)
                ));
                $have[] = $this->db->insert_id();
            }
        }
        ///////////delete no not
        if (count($have) > 0) {
            $this->db->where_not_in('sf_id', $have);
        }
        $this->db->where("id_res_auto", res_id());
        $this->db->where('f_id', $f_id);
        $this->db->delete('food_option');
    }

    function get_sort_option($f_id) {
        $a = $this->db->select_max("sort")->get_where("food_option", array("f_id" => $f_id, 'id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            return $a->row()->sort - 0 + 1;
        } else {
            return 1;
        }
    }

    function edit_size($size, $f_id) {
        $have = array();
        foreach ($size as $s) {
            if ($s->id - 0 !== 0) {//Edit
                $have[] = $s->id;
                $this->db->update("food_size", array("name" => mullti_lang_todb($s->name), 'price' => $s->price, 'default_val' => $s->default_val - 0), array('sf_id' => $s->id));
            } else {//Add(new)
                $this->db->set('c_date', 'NOW()', FALSE);
                $this->db->insert("food_size", array(
                    'name' => mullti_lang_todb($s->name),
                    'price' => $s->price,
                    'default_val' => $s->default_val - 0,
                    'f_id' => $f_id,
                    'id_res_auto' => res_id(),
                    'sort' => $this->get_sort_size($f_id)
                ));
                $have[] = $this->db->insert_id();
            }
        }

        ///////////delete no not
        if (count($have) > 0) {
            $this->db->where_not_in('sf_id', $have);
        }
        $this->db->where("id_res_auto", res_id());
        $this->db->delete('food_size');
    }

    function get_sort_size($f_id) {
        $a = $this->db->select_max("sort")->get_where("food_size", array("f_id" => $f_id, 'id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            return $a->row()->sort - 0 + 1;
        } else {
            return 1;
        }
    }

    //////end save detail///////////

    function get_tagfood($id) {
        $a = $this->db->select("ft_id as id")->get_where("food_tag_pk", array('f_id' => $id, 'id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            $output = array();
            foreach ($a->result_array() as $value) {
                $output[] = $value['id'] - 0;
            }
            return $output;
        } else {
            return array();
        }
    }

    function get_sizefood($id) {
        $a = $this->db->select('sf_id as id ,name,price,default_val')->get_where("food_size", array('f_id' => $id, 'id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            $output = $a->result_array();
            foreach ($output as $i => $value) {
                $output[$i]['default_val'] = $value['default_val'] - 0 == 1 ? true : false;
                $output[$i]['name'] = get_name_show($value['name']);
                $output[$i]['price'] = $value['price'] - 0;
                $output[$i]['id'] = $value['id'] - 0;
            }
            return $output;
        } else {
            return array();
        }
    }

    function get_optionfood($id) {
        $a = $this->db->select("sf_id as id,name,default_val,sort")->get_where("food_option", array('id_res_auto' => res_id(), 'f_id' => $id));
        if ($a->num_rows() > 0) {
            $output = $a->result_array();
            foreach ($output as $key => $value) {
                $output[$key]['name'] = get_name_show($value['name']);
                $output[$key]['default_val'] = $value['default_val'] - 0 == 1 ? true : false;
                $output[$key]['sort'] = $value['sort'] - 0;
                $output[$key]['id'] = $value['id'] - 0;
            }
            return $output;
        } else {
            return array();
        }
    }

    function get_extrafood($id) {
        $a = $this->db->select("sf_id,name,price,default_val,sort")->get_where("food_extra", array('id_res_auto' => res_id(), 'f_id' => $id));
        if ($a->num_rows() > 0) {
            $output = $a->result_array();
            foreach ($output as $key => $value) {
                $output[$key]['name'] = get_name_show($value['name']);
                $output[$key]['price'] = $value['price'] - 0;
                $output[$key]['sort'] = $value['sort'] - 0;
                $output[$key]['default_val'] = $value['default_val'] - 0 == 1 ? true : false;
            }
            return $output;
        } else {
            return array();
        }
    }

    function get_picfood($id) {
        $a = $this->db->select("pic_id")->get_where("food_pic", array('f_id' => $id, 'id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            $output = array();
            foreach ($a->result_array() as $value) {
                $output[] = $value['pic_id'] - 0;
            }
            return $output;
        } else {
            return array();
        }
    }

}
