<?php

class Reports_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function report_01($date, $type) {
        $date1 = explode(" - ", urldecode($date));
        $sql1 = "SET time_zone = '+7:00'";
        $this->db->query($sql1);
        $out = array();
        $g2 = array();
        if ($type == 'year') {
            $sql1 = "select extract(year from `date`) as  year,sum(price) as sum from expenditure where  `id_res_auto` = '" . res_id() . "' AND `date`  >= '" . $date1[0] . "' AND `date` <= '" . $date1[1] . "'  group by year";
            $ex = $this->db->query($sql1);
            $sql1 = "select extract(year from `up_date`) as  year,sum(sum) as sum from stock_update where  `id_res_auto` = '" . res_id() . "' AND date(up_date)  >= '" . $date1[0] . "' AND date(up_date) <= '" . $date1[1] . "'  group by year";
            $stock_update = $this->db->query($sql1);
            $sql = "SELECT order_id,sum(sum_price) as sum, count(*) as count,`up_date`, extract(year from `up_date`- INTERVAL 4 HOUR) as  date FROM `order_data` WHERE `id_res_auto` = '" . res_id() . "' AND `up_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND `up_date` - INTERVAL 28 HOUR < '" . $date1[1] . "' and status = 1 group by date  ";
            $data = $this->db->query($sql);
//            echo $this->db->last_query();
            $out['data'] = array(
                'labels' => array(),
                'datasets' => array(
                    array(
                        'label' => l('summery'),
                        'backgroundColor' => 'rgba(54, 162, 235, 0.5)',
                        'borderColor' => 'rgb(54, 162, 235)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => l('number_order'),
                        'backgroundColor' => 'rgba(254, 162, 235, 0.5)',
                        'borderColor' => 'rgb(254, 162, 235)',
                        'borderWidth' => 1,
                        'data' => array()
                    ), array(
                        'label' => l('expenditure'),
                        'backgroundColor' => 'rgba(100, 250, 12, 0.5)',
                        'borderColor' => 'rgb(100, 250, 12)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => l('sale_m_p'),
                        'backgroundColor' => 'rgba(200, 50, 100, 0.5)',
                        'borderColor' => 'rgb(200, 50, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    )
                )
            );

            foreach ($data->result() as $value) {
                array_push($out['data']['labels'], date_to_str($value->date));
                array_push($out['data']['datasets'][0]['data'], $value->sum - 0);
                array_push($out['data']['datasets'][1]['data'], $value->count - 0);
                $flag = true;
                $temp_g2 = array();
                foreach ($ex->result() as $e) {
                    if ($e->year == $this->get_y($value->date)) {
                        array_push($out['data']['datasets'][2]['data'], $e->sum - 0);
                        $flag = false;
                        $temp_g2 = array('date' => date_to_str(trim($value->date)), 'sum' => $value->sum - 0, 'count' => $value->count - 0, 'ex' => $e->sum - 0, 'mat' => 0);
                    }
                }
                if ($flag) {
                    array_push($out['data']['datasets'][2]['data'], 0);
                    $temp_g2 = array('date' => date_to_str(trim($value->date)), 'sum' => $value->sum - 0, 'count' => $value->count - 0, 'ex' => 0, 'mat' => 0);
                }
                $flag2 = true;
                foreach ($stock_update->result() as $s) {
                    if ($s->year == $this->get_y($value->date)) {
                        $flag2 = false;
                        array_push($out['data']['datasets'][3]['data'], $s->sum - 0);
                        $temp_g2['mat'] = $s->sum;
                    }
                }
                if ($flag2) {
                    array_push($out['data']['datasets'][3]['data'], 0);
                }
            }

            $out['title'] = l('re_01') . " " . date_to_str(trim($date1[0])) . " " . l('to') . " " . date_to_str(trim($date1[1])) . " (" . l('group_year') . ")";
        } else if ($type == 'month') {
            $sql1 = "select  CONCAT(extract(year from `up_date`),'-',  extract(month from `up_date`)) as  month,sum(price) as sum from expenditure where  `id_res_auto` = '" . res_id() . "' AND `date`  >= '" . $date1[0] . "' AND `date` <= '" . $date1[1] . "'  group by month";
            $ex = $this->db->query($sql1);
            $sql1 = "select  CONCAT(extract(year from `up_date`),'-',  extract(month from `up_date`)) as  month,sum(sum) as sum from stock_update where  `id_res_auto` = '" . res_id() . "' AND date(up_date)  >= '" . $date1[0] . "' AND date(up_date) <= '" . $date1[1] . "'  group by month";
            $stock_update = $this->db->query($sql1);
            $sql = "SELECT order_id,sum(sum_price) as sum,count(*) as count, `up_date`, CONCAT(extract(year from `up_date`- INTERVAL 4 HOUR),'-',  extract(month from `up_date`- INTERVAL 4 HOUR)) as  date FROM `order_data` WHERE `id_res_auto` = '" . res_id() . "' AND `up_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND `up_date` - INTERVAL 28 HOUR < '" . $date1[1] . "' and status = 1 group by date  ";
            $data = $this->db->query($sql);
            $out['data'] = array(
                'labels' => array(),
                'datasets' => array(
                    array(
                        'label' => l('summery'),
                        'backgroundColor' => 'rgba(54, 162, 235, 0.5)',
                        'borderColor' => 'rgb(54, 162, 235)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => l('number_order'),
                        'backgroundColor' => 'rgba(254, 162, 235, 0.5)',
                        'borderColor' => 'rgb(254, 162, 235)',
                        'borderWidth' => 1,
                        'data' => array()
                    ), array(
                        'label' => l('expenditure'),
                        'backgroundColor' => 'rgba(100, 250, 12, 0.5)',
                        'borderColor' => 'rgb(100, 250, 12)',
                        'borderWidth' => 1,
                        'data' => array()
                    )
                    , array(
                        'label' => l('sale_m_p'),
                        'backgroundColor' => 'rgba(200, 50, 100, 0.5)',
                        'borderColor' => 'rgb(200, 50, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    )
                )
            );
            foreach ($data->result() as $value) {
                array_push($out['data']['labels'], date_to_str(trim($value->date)));
                array_push($out['data']['datasets'][0]['data'], $value->sum - 0);
                array_push($out['data']['datasets'][1]['data'], $value->count - 0);
                $flag = true;
                $temp_g2 = array();
                foreach ($ex->result() as $e) {
                    if ($e->month == $this->get_y_m($value->date)) {
                        array_push($out['data']['datasets'][2]['data'], $e->sum - 0);
                        $flag = false;
                        $temp_g2 = array('date' => date_to_str(trim($value->date)), 'sum' => $value->sum - 0, 'count' => $value->count - 0, 'ex' => $e->sum - 0, 'mat' => 0);
                    }
                }
                if ($flag) {
                    array_push($out['data']['datasets'][2]['data'], 0);
                    $temp_g2 = array('date' => date_to_str(trim($value->date)), 'sum' => $value->sum - 0, 'count' => $value->count - 0, 'ex' => 0, 'mat' => 0);
                }
                $flag2 = true;
                foreach ($stock_update->result() as $s) {
                    if ($s->month == $this->get_y_m($value->date)) {
                        $flag2 = false;
                        array_push($out['data']['datasets'][3]['data'], $s->sum - 0);
                        $temp_g2['mat'] = $s->sum;
                    }
                }
                if ($flag2) {
                    array_push($out['data']['datasets'][3]['data'], 0);
                }
                array_push($g2, $temp_g2);
            }
            $out['title'] = l('re_01') . " " . date_to_str(trim($date1[0])) . " " . l('to') . " " . date_to_str(trim($date1[1])) . " (" . l('group_month') . ")";
        } else {//day
            $sql1 = "select `date`,sum(price) as sum from expenditure where  `id_res_auto` = '" . res_id() . "' AND `date`  >= '" . $date1[0] . "' AND `date` <= '" . $date1[1] . "'  group by date";
            $ex = $this->db->query($sql1);
            $sql1 = "select date(up_date) as date,sum(sum) as sum from stock_update where  `id_res_auto` = '" . res_id() . "' AND date(up_date)  >= '" . $date1[0] . "' AND date(up_date) <= '" . $date1[1] . "'  group by date";
            $stock_update = $this->db->query($sql1)->result();
            $sql = "SELECT order_id,sum(sum_price) as sum,count(*)as count, `up_date`, date(`up_date`- INTERVAL 4 HOUR) as date FROM `order_data` WHERE `id_res_auto` = '" . res_id() . "' AND `up_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND `up_date` - INTERVAL 28 HOUR <= '" . $date1[1] . "' and status = 1 group by date ";
            $data = $this->db->query($sql);

            $g2 = array();
            $out['data'] = array(
                'labels' => array(),
                'datasets' => array(
                    array(
                        'label' => l('summery'),
                        'backgroundColor' => 'rgba(54, 162, 235, 0.5)',
                        'borderColor' => 'rgb(54, 162, 235)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => l('number_order'),
                        'backgroundColor' => 'rgba(254, 162, 100, 0.5)',
                        'borderColor' => 'rgb(254, 162, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    ), array(
                        'label' => l('expenditure'),
                        'backgroundColor' => 'rgba(100, 250, 12, 0.5)',
                        'borderColor' => 'rgb(100, 250, 12)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => l('sale_m_p'),
                        'backgroundColor' => 'rgba(200, 50, 100, 0.5)',
                        'borderColor' => 'rgb(200, 50, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    )
                )
            );
            foreach ($data->result() as $value) {
                array_push($out['data']['labels'], date_to_str($value->date));
                array_push($out['data']['datasets'][0]['data'], $value->sum - 0);
                array_push($out['data']['datasets'][1]['data'], $value->count - 0);
                $flag = true;
                $temp_g2 = array();
                foreach ($ex->result() as $e) {
                    if ($e->date == $value->date) {
                        array_push($out['data']['datasets'][2]['data'], $e->sum - 0);
                        $flag = false;
                        $temp_g2 = array('date' => date_to_str(trim($value->date)), 'sum' => $value->sum - 0, 'count' => $value->count - 0, 'ex' => $e->sum - 0, 'mat' => 0);
                    }
                }
                if ($flag) {
                    array_push($out['data']['datasets'][2]['data'], 0);
                    $temp_g2 = array('date' => date_to_str(trim($value->date)), 'sum' => $value->sum - 0, 'count' => $value->count - 0, 'ex' => 0, 'mat' => 0);
                }
                $flag2 = true;
                foreach ($stock_update as $s) {
                    if ($s->date == $value->date) {
                        $flag2 = false;
                        array_push($out['data']['datasets'][3]['data'], $s->sum - 0);
                        $temp_g2['mat'] = $s->sum;
                    }
                }
                if ($flag2) {
                    array_push($out['data']['datasets'][3]['data'], 0);
//                    $temp_g2['mat'] = 0;
                }
                array_push($g2, $temp_g2);
            }
            $out['title'] = l('re_01') . " " . date_to_str(trim($date1[0])) . " " . l('to') . " " . date_to_str(trim($date1[1]));
        }
        return array('g1' => $out, 'g2' => $g2);
    }

    public function get_all_user() {
        $user = $this->db->select("admin_id,name")->get_where('admin', array('id_res_auto' => res_id(), 'del' => 0));
        return $user->result();
    }

    public function report_04($day, $month, $year) {

        $date1[0] = $year . "-" . $month . "-" . $day;
        $date1[1] = $date1[0];
        $users_id = array();
        $sql = "select d.d_id,d.cre_date,d.cre_by,d.amount,a.name from drawer d left join admin a on a.admin_id = d.cre_by where d.id_res_auto = " . res_id() . " AND `cre_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND `cre_date` - INTERVAL 28 HOUR < '" . $date1[1] . "' order by d_id desc ";
        $drawer = $this->db->query($sql);
        $out = array();
        foreach ($drawer->result() as $value) {
            if (!in_array($value->cre_by, $users_id)) {
                array_push($out, $value);
                array_push($users_id, $value->cre_by);
            }
        }
        foreach ($out as $value) {
            $pay = $this->db->select('id_pay_type,bank')->get_where("payment_type", array('id_res_auto' => res_id()))->result();
            foreach ($pay as $v) {
                $v->sum = 0;
            }
            $value->exp = $this->db->select("sum(price) as sum")->get_where("expenditure", array('id_res_auto' => res_id(), 'up_by' => $value->cre_by, 'date' => $date1[0]))->row()->sum - 0;
            array_push($pay, (Object) array('id_pay_type' => 0, 'bank' => l('cash'), 'sum' => 0));
            $sql = "select payments,sum_price from order_data  where id_res_auto = " . res_id() . " AND `up_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND `up_date` - INTERVAL 28 HOUR < '" . $date1[1] . "' and payment_by  = " . $value->cre_by . " and status = 1 and del = 0  ";
            $a = $this->db->query($sql);
            $value->data = $a->result();

            $value->sum_charge = 0;
            $value->sum_order = count($value->data);
            $money = 0;
            foreach ($value->data as $da) {
                $da->payments = unserialize($da->payments);
                $da->payment_sum = 0;
                foreach ($da->payments as $pa) {
                    foreach ($pay as $p) {
                        if ($p->id_pay_type == $pa['id_pay_type']) {
                            if ($pa['id_pay_type'] - 0 == 0) {
                                $money += $pa['amount'] - 0;
                            }
                            $p->sum += $pa['amount'] - 0;
                            $da->payment_sum += $pa['amount'] - 0;
                        }
                    }
                }
                $da->change = $da->payment_sum - $da->sum_price;
                $value->sum_charge += $da->change;
            }


            $value->payment = array();
            foreach ($pay as $va) {
                if ($va->sum != 0) {
                    array_push($value->payment, $va);
                }
            }
            foreach ($value->payment as $payment_temp) {
                if ($payment_temp->id_pay_type == 0) {
                    $payment_temp->sum -= $value->sum_charge;
                }
            }
            $value->drawer = $value->amount - 0 + $money - $value->exp - $value->sum_charge;
            unset($value->data);
        }
        $sql = "SELECT sum(count)as count,sum(total_price) as sum,`f`.`name`,`f`.f_id FROM `order_detail` od join `order_data` `orda` on orda.order_id = od.order_id join `foods` `f` on `f`.`f_id` = `od`.`f_id` WHERE `od`.`id_res_auto` = '" . res_id() . "' AND orda.`time_end` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND `orda`.`time_end` - INTERVAL 28 HOUR <= '" . $date1[1] . "' and `orda`.status=1 and od.status !=4 and od.del = 0 group by f_id order by count desc ";
        $data = $this->db->query($sql)->result();
        foreach ($data as $key => $value) {
            $value->name = array_lang($value->name);
        }
        return array('data' => $out, 'product' => $data);
    }

    public function report_07($date1, $users) {
        $u = str_replace("-", ",", $users);
        $out = array();
        $g2 = array();
        $user = explode(',', $u);
        if (count($user) > 0) {
            $this->db->where_in('admin_id', $user);
            $admin = $this->db->select('admin_id,name')->get_where("admin", array('id_res_auto' => res_id()))->result();
            foreach ($admin as $value) {
                $value->user_cook = 0;
                $value->serve = 0;
                $value->noti = 0;
                $value->cancel = 0;
            }


            $sql = "SELECT user_cook,count(*)as count,a.name FROM order_detail od join admin a on a.admin_id=od.user_cook  WHERE od.del=0 and od.user_cook in (" . $u . ")  and od.status !=4 and od.id_res_auto = '" . res_id() . "' AND od.`up_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND od.`up_date` - INTERVAL 28 HOUR <= '" . $date1[1] . "' group by user_cook ";
            $ex = $this->db->query($sql);
            $user_cook = $ex->result();
            foreach ($user_cook as $value) {
                foreach ($admin as $a) {
                    if ($value->user_cook == $a->admin_id) {
                        $a->user_cook = $value->count;
                    }
                }
            }
            $sql = "SELECT user_serve,count(*)as count,a.name FROM order_detail od join admin a on a.admin_id=od.user_serve  WHERE od.del=0 and od.user_serve in (" . $u . ")  and od.status !=4 and od.id_res_auto = '" . res_id() . "' AND od.`up_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND od.`up_date` - INTERVAL 28 HOUR <= '" . $date1[1] . "' group by user_serve ";
            $ex = $this->db->query($sql);
            $serve = $ex->result();
            foreach ($serve as $value) {
                foreach ($admin as $a) {
                    if ($value->user_serve == $a->admin_id) {
                        $a->serve = $value->count;
                    }
                }
            }
            $sql = "SELECT user_id,count(*)as count,a.name FROM notification od join admin a on a.admin_id=od.user_id  WHERE od.user_id in (" . $u . ")  and od.id_res_auto = '" . res_id() . "' AND od.`date_time` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND od.`date_time` - INTERVAL 28 HOUR <= '" . $date1[1] . "' group by user_id ";
            $ex = $this->db->query($sql);
            $noti = $ex->result();
            foreach ($noti as $value) {
                foreach ($admin as $a) {
                    if ($value->user_id == $a->admin_id) {
                        $a->noti = $value->count;
                    }
                }
            }
            $sql = "SELECT od.up_by,count(*)as count,a.name FROM order_detail od join admin a on a.admin_id=od.up_by  WHERE od.del=0 and od.up_by in (" . $u . ")  and od.status =-1 and od.id_res_auto = '" . res_id() . "' AND od.`up_date` - INTERVAL 4 HOUR >= '" . $date1[0] . "' AND od.`up_date` - INTERVAL 28 HOUR <= '" . $date1[1] . "' group by od.up_by ";
            $ex = $this->db->query($sql);
            $cancel = $ex->result();
            foreach ($cancel as $value) {
                foreach ($admin as $a) {
                    if ($value->up_by == $a->admin_id) {
                        $a->cancel = $value->count;
                    }
                }
            }
            $g2 = $admin;
            $out['data'] = array(
                'labels' => array(),
                'datasets' => array(
                    array(
                        'label' => "ทำอาหาร",
                        'backgroundColor' => 'rgba(54, 162, 235, 0.5)',
                        'borderColor' => 'rgb(54, 162, 235)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => "เสริฟอาหาร",
                        'backgroundColor' => 'rgba(254, 162, 100, 0.5)',
                        'borderColor' => 'rgb(254, 162, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    ), array(
                        'label' => "เรียกพนักงาน",
                        'backgroundColor' => 'rgba(100, 250, 12, 0.5)',
                        'borderColor' => 'rgb(100, 250, 12)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => "ยกเลิกออเดอร์",
                        'backgroundColor' => 'rgba(200, 50, 100, 0.5)',
                        'borderColor' => 'rgb(200, 50, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    )
                )
            );
            foreach ($admin as $value) {
                array_push($out['data']['labels'], $value->name);
                array_push($out['data']['datasets'][0]['data'], $value->user_cook - 0);
                array_push($out['data']['datasets'][1]['data'], $value->serve - 0);
                array_push($out['data']['datasets'][2]['data'], $value->noti - 0);
                array_push($out['data']['datasets'][3]['data'], $value->cancel - 0);
            }
        } else {
            $g2 = [];
            $out['data'] = array(
                'labels' => array(),
                'datasets' => array(
                    array(
                        'label' => "ทำอาหาร",
                        'backgroundColor' => 'rgba(54, 162, 235, 0.5)',
                        'borderColor' => 'rgb(54, 162, 235)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => "เสริฟอาหาร",
                        'backgroundColor' => 'rgba(254, 162, 100, 0.5)',
                        'borderColor' => 'rgb(254, 162, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    ), array(
                        'label' => "เรียกพนักงาน",
                        'backgroundColor' => 'rgba(100, 250, 12, 0.5)',
                        'borderColor' => 'rgb(100, 250, 12)',
                        'borderWidth' => 1,
                        'data' => array()
                    ),
                    array(
                        'label' => "ยกเลิกออเดอร์",
                        'backgroundColor' => 'rgba(200, 50, 100, 0.5)',
                        'borderColor' => 'rgb(200, 50, 100)',
                        'borderWidth' => 1,
                        'data' => array()
                    )
                )
            );
        }
        $out['title'] = "รายงานการทำพนักงาน วันที่ " . date_to_str(trim($date1[0])) . " ถึง " . date_to_str(trim($date1[1]));
        return array('g1' => $out, 'g2' => $g2);
    }

}
