<?php

class Pos_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_drawer_emp($d_id = '0') {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $out = array();
        $dra = $this->db->get_where("drawer", array('d_id' => $d_id));
        $pay = $this->db->select('id_pay_type,bank')->get_where("payment_type", array('id_res_auto' => res_id()))->result();
        foreach ($pay as $v) {
            $v->sum = 0;
            $v->count = 0;
        }
//        pre($pay);
        array_push($pay, (Object) array('id_pay_type' => 0, 'bank' => l('cash'), 'sum' => 0, 'count' => 0));
        if ($dra->num_rows() > 0) {
            $drawer = $dra->row();
            $out['data'] = $drawer;
            $drawer->name = l('notknow');
            $na = $this->db->select("name,pro_file_url,p_group")->get_where("admin", array('admin_id' => $drawer->cre_by));
            if ($na->num_rows() > 0) {
                $drawer->name = $na->row()->name;
                $drawer->pro_file_url = $na->row()->pro_file_url;
                $g = $this->db->select('name')->get_where("g_permission", array('id_res_auto' => res_id(), 'id' => $na->row()->p_group));
                $drawer->group = '';
                if ($g->num_rows() > 0) {
                    $drawer->group = $g->row()->name;
                }
            }
            $out['exp'] = $this->db->select('sum(e.price) as sum,eg.title')->join('expenditure_group eg', "eg.exg_id=e.exg_id")->group_by('e.exg_id')->get_where("expenditure e", array('e.id_res_auto' => res_id(), 'e.up_by' => $drawer->cre_by, 'e.date' => date("Y-m-d", strtotime($drawer->cre_date))))->result();
            $exp_sum = (object) array('sum' => 0, 'title' => 'รวม');
            foreach ($out['exp'] as $value) {
                $exp_sum->sum += $value->sum;
            }
            $out['exp_sum'] = 0;
            foreach ($out['exp'] as $value) {
                $out['exp_sum'] += $value->sum - 0;
            }
            array_push($out['exp'], $exp_sum);

//            $order = $this->db->get_where("order_data", array('payment_by' => $drawer->cre_by, 'time_end >=' => $drawer->cre_date))->result();
            $cre_date = date('Y-m-d', strtotime($drawer->cre_date));
            $sql = "select * from order_data  where id_res_auto = " . res_id() . " AND `up_date` - INTERVAL 4 HOUR >= '" . $cre_date . "' AND `up_date` - INTERVAL 28 HOUR < '" . $cre_date . "' and payment_by  = " . $drawer->cre_by . " and status = 1 and del = 0  ";
            $order = $this->db->query($sql)->result();
            $buffet = array();
            $out['sum_table'] = 0;
            $out['sum_bill'] = 0;
            $out['fine'] = 0;

            $order_id = array();
            $out['sum_change'] = 0;
            foreach ($order as $value) {
                array_push($order_id, $value->order_id);
                $value->people = unserialize($value->people);
                if ($value->sum_table == 'a:0:{}') {
                    $out['sum_bill'] += $value->sum_price;
                } else {
                    $out['sum_table'] += $value->sum_price;
                }
                $out['sum_change'] += $value->change_money;
                foreach ($pay as $payyyy) {
                    if ($payyyy->id_pay_type == 0) {
                        $payyyy->sum -= $value->change_money;
                    }
                }
                $value->payments = unserialize($value->payments);
                $value->payment_sum = 0;
                foreach ($value->payments as $pa) {
                    foreach ($pay as $p) {
                        if ($p->id_pay_type == $pa['id_pay_type']) {
                            $p->count++;
                            $p->sum += $pa['amount'] - 0;
                            $value->payment_sum += $pa['amount'] - 0;
                        }
                    }
                }
                $out['paymets'] = array();
                $pay_all = (object) array('id_pay_type' => -1, 'bank' => 'รวม', 'sum' => 0, 'count' => 0);
                $out['drawer_money'] = 0;
                foreach ($pay as $va) {
                    if ($va->sum != 0) {
                        array_push($out['paymets'], $va);
                        if ($va->id_pay_type - 0 == 0) {
                            $out['drawer_money'] = $va->sum;
                        }
                        $pay_all->sum += $va->sum;
                        $pay_all->count += $va->count;
                    }
                }
                array_push($out['paymets'], $pay_all);
                $out['drawer_money'] += $out['data']->amount - $out['exp_sum'];

                $out['fine'] += $value->fine;
                foreach ($value->people as $peo) {
                    $flag = true;
                    foreach ($buffet as $key => $buf) {
                        if ($buf['buffet_id'] - 0 == $peo['buffet_id']) {
                            $buffet[$key]['number'] += $peo['number'];
                            $buffet[$key]['sum_price'] += $peo['number'] * $peo['price'];
                            $flag = false;
                            break;
                        }
                    }
                    if ($flag) {
                        array_push($buffet, array('buffet_id' => $peo['buffet_id'], 'number' => $peo['number'], 'sum_price' => $peo['price'] * $peo['number']));
                    }
                }
            }
            $buffet_all = array();
            foreach ($buffet as $key => $value) {
                if ($value['number'] - 0 != 0) {
                    $buffet_db = $this->db->get_where('buffet', array('buffet_id' => $value['buffet_id'], 'id_res_auto' => res_id()));
                    $buffet[$key]['title'] = '';
                    if ($buffet_db->num_rows() > 0) {
                        $buffet[$key]['title'] = array_lang($buffet_db->row()->name);
                    }
                    array_push($buffet_all, $buffet[$key]);
                }
            }
            if (count($order_id) > 0) {
                $this->db->where_in("od.order_id", $order_id);
            } else {
                $this->db->where_in("od.order_id", array(0));
            }
            $out['food'] = $this->db->select('sum(total_price) sum,f.name,f.f_id,count(*) as count')->join('foods f', 'f.f_id=od.f_id')->order_by('count', 'desc')->group_by('f.f_id')->get_where("order_detail od", array('od.status !=' => 4, 'od.del' => 0, 'od.is_buffet' => 0, 'od.id_res_auto' => res_id()))->result();
            foreach ($out['food'] as $key => $food) {
                $out['food'][$key]->name = array_lang($food->name);
            }
            if (count($order_id) > 0) {
                $this->db->where_in("od.order_id", $order_id);
            } else {
                $this->db->where_in("od.order_id", array(0));
            }
            $out['group'] = $this->db->select('sum(total_price) sum,fg.name,fg.fg_id,count(*) as count')->join('foods f', 'f.f_id=od.f_id')->join('foods_group fg', 'fg.fg_id=f.f_group')->order_by('count', 'desc')->group_by('f.f_group')->get_where("order_detail od", array('od.status !=' => 4, 'od.del' => 0, 'od.is_buffet' => 0, 'od.id_res_auto' => res_id()))->result();
            foreach ($out['group'] as $key => $food) {
                $out['group'][$key]->name = array_lang($food->name);
            }
            $all = array('buffet_id' => 0, 'number' => 0, 'sum_price' => 0, 'title' => 'รวม');
            foreach ($buffet_all as $value) {
                $all['number'] += $value['number'];
                $all['sum_price'] += $value['sum_price'];
            }
            array_push($buffet_all, $all);
            $out['buffet'] = $buffet_all;

            return $out;
//            echo json_encode($out);
        }
    }

    function count_items() {
        $id_res_auto = res_id();
        $this->db->where_not_in('d.status', array(3, -1, 4));
        $sub = $this->db->order_by("d.status", 'desc')->select("d.od_id,d.price,f.name,d.count,d.toppings,d.q,d.status,f.pic_id,d.ok_id,d.comment,d.is_buffet,od.title,od.sum_table,d.details")->join("order_data od", 'od.order_id=d.order_id')->join("foods f", "f.f_id=d.f_id")->get_where("order_detail d", array('d.id_res_auto' => $id_res_auto, 'd.del' => 0, 'od.del' => 0, 'od.status' => 2))->result();
        $out = array();
        $count = 0;
        foreach ($sub as $key => $v) {
            if ($v->status - 0 == 3 || $v->status - 0 == -1) {
                continue;
            }
            $count++;
        }
        echo $count;
    }

    public function load_items() {
        $id_res_auto = res_id();
        $this->db->where_not_in('d.status', array(3, -1, 4));
        $sub = $this->db->limit(20)->order_by("d.status", 'desc,ok_id')->select("d.od_id,d.price,f.name,d.count,d.toppings,d.q,d.status,f.pic_id,d.ok_id,d.comment,d.is_buffet,od.title,od.sum_table,d.details")->join("order_data od", 'od.order_id=d.order_id')->join("foods f", "f.f_id=d.f_id")->get_where("order_detail d", array('d.id_res_auto' => $id_res_auto, 'd.del' => 0, 'od.del' => 0, 'od.status' => 2))->result();
        $out = array();
        $res = $this->db->select('pay_before')->get_where('restaurant', array('id_res_auto' => $id_res_auto));
        $pay_before = false;
        if ($res->num_rows() > 0) {
            if ($res->row()->pay_before - 0 === 1) {
                $pay_before = true;
            } else {
                $pay_before = false;
            }
        }
        foreach ($sub as $key => $v) {
            if ($v->status - 0 == 3 || $v->status - 0 == -1) {
                continue;
            }
            $sub[$key]->price_net = $v->price;
            $sub[$key]->sum_table = unserialize($v->sum_table);
            $sub[$key]->table = '';
            if (count($sub[$key]->sum_table) > 0) {
                $this->db->where_in('table_id_auto', $sub[$key]->sum_table);
                $table = $this->db->select("name")->get_where('table_table', array('id_res_auto' => $id_res_auto))->result();
                foreach ($table as $k => $ta) {
                    if ($k > 0) {
                        $sub[$key]->table .= ",";
                    }
                    $sub[$key]->table .= $ta->name;
                }
            }
            $sub[$key]->check = false;
            $sub[$key]->status -= 0;
            $sub[$key]->is_buffet -= 0;
            if ($v->status - 0 == 1 || $v->status - 0 == 2) {
                if ($v->status - 0 == 1) {
                    $v->q = 0;
                } else {
                    $v->q = -1;
                }
            } else {
                $sub[$key]->q = $this->get_q($v->ok_id, $id_res_auto, $pay_before);
            }
            $sub[$key]->pic_id = base_url() . "pic/" . $v->pic_id . "?type=small";
            $sub[$key]->name = array_lang($v->name);
            $toppings = $this->db->select("tps_id,title")->get_where("topping_sub", array('id_res_auto' => $id_res_auto))->result();
            foreach ($toppings as $k => $value) {
                $toppings[$k]->title = array_lang($value->title);
            }
            $sub[$key]->toppings = $this->get_topping($v->toppings, $toppings);
            foreach ($sub[$key]->toppings as $ext) {
                $sub[$key]->price_net += $ext['price'] * $ext['count'];
            }
            $v->details = unserialize($v->details);
            if ($v->details) {
                foreach ($v->details as $de) {
                    foreach ($de->sub as $s) {
                        if ($s->selected) {
                            $v->price_net += ($s->price - 0);
                        }
                    }
                }
            } else {
                $v->details = array();
            }
            $sub[$key]->price_net *= $sub[$key]->count;

            if ($sub[$key]->is_buffet == 1) {
                $sub[$key]->price_net = 0;
            }
            array_push($out, $v);
        }
        usort($out, function($a, $b) {
            return $a->q - $b->q;
        });

        echo json_encode($out);
    }

    public function get_payment_bill($payments, $id_res_auto) {
        $p = unserialize($payments);
        foreach ($p as $key => $value) {
            if ($value['id_pay_type'] - 0 != 0) {
                $pay = $this->db->get_where("payment_type", array('id_res_auto' => $id_res_auto, 'id_pay_type' => $value['id_pay_type']));
                if ($pay->num_rows()) {
                    $p[$key]['bank'] = $pay->row()->bank;
                } else {
                    $p[$key]['bank'] = l('not_set');
                }
            } else {
                $p[$key]['bank'] = l('cash');
            }
        }
        return $p;
    }

    public function sum_people($p1, $p2) {
        $output = $p1;
        foreach ($p1 as $key => $p1_t) {
            foreach ($p2 as $p2_t) {
                if ($p1_t['buffet_id'] == $p2_t['buffet_id']) {
                    $output[$key]['number'] += $p2_t['number'] - 0;
                }
            }
        }
        foreach ($p2 as $value) {
            $flag = true;
            foreach ($p1 as $val1) {
                if ($val1['buffet_id'] == $value['buffet_id']) {
                    $flag = false;
                }
            }
            if ($flag) {
                $output[] = $value;
            }
        }
        return $output;
    }

    function sum_table($table_select) {
        if (count($table_select) > 0) {
            $this->db->trans_start();
            $order_all = array();
            $order_id_all = array();
            $table = array();
            $people = array();
            $date_start = "";
            $od = $this->db->order_by("order_id", 'desc')->get_where("order_data", array('id_res_auto' => res_id(), 'status' => 2))->result();
            foreach ($table_select as $value) {
                $flag = true;
                foreach ($od as $od_v) {
                    foreach (unserialize($od_v->sum_table) as $tablea) {
                        if ($tablea - 0 == $value - 0 && !in_array($od_v->order_id, $order_id_all)) {
                            $order_id_all[] = $od_v->order_id;
                            $order_all[] = $od_v;
                            $flag = false;
                            $date_start = $od_v->time_start;
                        }
                    }
                }
                if ($flag) {
                    if (!in_array($value - 0, $table)) {
                        $table[] = $value - 0;
                    }
                }
            }
            if (count($order_id_all) > 0) {
                $peoplea = $this->db->select("buffet_id,name,price")->get_where("buffet", array('id_res_auto' => res_id(), 'del' => 0))->result();
                foreach ($peoplea as $value) {
                    $temp = array('buffet_id' => $value->buffet_id, 'price' => $value->price, 'number' => 0, 'name' => $this->get_name($value->buffet_id, $peoplea));
                    array_push($people, $temp);
                }
            }
            foreach ($order_all as $order) {
                foreach (unserialize($order->sum_table) as $table_a) {
                    if (!in_array($table_a, $table)) {
                        $table[] = $table_a;
                    }
                }
                $people = $this->sum_people($people, unserialize($order->people));
            }
            $insert = array('id_res_auto' => res_id(), 'sum_table' => serialize($table), 'order_id_res' => $this->get_order_id(), 'people' => serialize($people), 'status' => 2);
            if (count($order_id_all) > 0) {
                $insert['time_start'] = $date_start;
            } else {
                $insert['time_start'] = date_time_now();
            }
            $this->db->insert("order_data", $insert);
            $order_id_new = $this->db->insert_id();
            $this->add_charge($order_id_new);
            if (count($order_id_all) > 0) {
                $this->db->where_in('order_id', $order_id_all);
                $this->db->update("order_detail", array('order_id' => $order_id_new), array('id_res_auto' => res_id()));
                $this->db->where_in('order_id', $order_id_all);
                $this->db->delete('order_data', array('id_res_auto' => res_id()));
                $this->db->where_in("order_id", $order_id_all);
                $this->db->delete("order_charge", array('id_res_auto' => res_id()));
            }
//        pre($table);
//        pre($people);
//        pre($order_id_all);
            $this->db->trans_complete();
            return $order_id_new;
        } else {
            return 0;
        }
    }

    public function get_order_id() {
        $order = $this->db->order_by('order_id_res', 'desc')->limit(1)->select("order_id_res")->get_where("order_data", array('id_res_auto' => res_id()));
        if ($order->num_rows() > 0) {
            return $order->row()->order_id_res + 1;
        } else {
            return 1;
        }
    }

    public function get_order_id_by_res($id_res_auto) {
        $order = $this->db->order_by('order_id_res', 'desc')->limit(1)->select("order_id_res")->get_where("order_data", array('id_res_auto' => $id_res_auto));
        if ($order->num_rows() > 0) {
            return $order->row()->order_id_res + 1;
        } else {
            return 1;
        }
    }

    function edit_item($item) {
        $toppings = array();
        foreach ($item->toppings as $topping) {
            $toppings[] = array('tps_id' => $topping->tps_id, 'price' => $topping->price, 'count' => $topping->count);
        }
        $data = array('up_by' => user_id(), 'cre_date' => date_time_now(), 'comment' => $item->detail->comment, 'total_price' => $item->detail->price, 'count' => $item->detail->count,
            'details' => serialize($item->details),
            'toppings' => serialize($toppings));
        $where = array('od_id' => $item->od_id, 'id_res_auto' => res_id());
        $this->db->update("order_detail", $data, $where);
        return $this->db->select('order_id')->get_where('order_detail', array('od_id' => $item->od_id, 'id_res_auto' => res_id()))->row()->order_id;
    }

    public function get_start_hour() {
        $res = $this->db->select("hour_start")->get_where("restaurant", array('id_res_auto' => res_id()));
        if ($res->num_rows() > 0) {
            $time = explode(".", $res->row()->hour_start . "");
            $mi = 0;
            if (count($time) === 1) {
                $mi = $time[0] * 60;
            } else if (count($time) === 2) {
                if (strlen($time[1]) == 1) {
                    $time[1] *= 10;
                }
                $mi = (($time[0] - 0) * 60) + ($time[1] - 0);
            }
            return $mi;
        } else {
            return 0;
        }
    }

    public function get_order_q_kitchen() {
        $ok = $this->db->order_by('ok_id', 'desc')->limit(1)->get_where("order_kitchen", array('id_res_auto' => res_id()));
        if ($ok->num_rows() == 0) {
            return 1;
        } else {
            $o = $ok->row();
            $date = explode(" ", $o->date_time)[0];
            if ($date != date('Y-m-d')) {
                return 1;
            } else {
                return $o->order_q + 1;
            }
        }
    }

    function add_item($data) {
        $order_id = $data->detail->order_id;
        if ($data->detail->order_id == 0) {//Add
            $price_start = 0;
            $price_per_hour = 0;
            if (count($data->detail->table_id) > 0) {
                $table = $data->detail->table_id[0];
                $a = $this->db->get_where("table_table", array('table_id_auto' => $table));
                if ($a->num_rows() > 0) {
                    $price_start = $a->row()->price_start;
                    $price_per_hour = $a->row()->price_per_hour;
                }
            }
            $data1 = array('people' => serialize(array()), 'hour_number' => $this->get_start_hour(), 'sum_table' => serialize($data->detail->table_id), 'order_id_res' => $this->get_order_id(), 'id_res_auto' => res_id(), 'status' => 2, 'time_start' => date_time_now(), 'time_end' => date_time_now(), 'price_start' => $price_start, 'price_per_hour' => $price_per_hour);
            $this->db->insert("order_data", $data1);
            $order_id = $this->db->insert_id();
            $this->add_charge($order_id);
        } else {//update      
        }
        $this->db->insert("order_kitchen", array('id_res_auto' => res_id(), 'close' => 0, 'date_time' => date_time_now(), 'order_id' => $order_id, 'order_q' => $this->get_order_q_kitchen()));
        $ok_id = $this->db->insert_id();
        $temp = array();
        foreach ($data->data as $item) {
            $toppings = array();
            foreach ($item->toppings as $topping) {
                $toppings[] = array('tps_id' => $topping->tps_id, 'price' => $topping->price, 'count' => $topping->count);
            }
            $temp[] = array('up_date' => date_time_now(), 'up_by' => user_id(), 'cre_date' => date_time_now(), 'cre_by' => user_id(), 'id_res_auto' => res_id(), 'order_id' => $order_id, 'f_id' => $item->f_id,
                'price' => $item->price, 'count' => $item->detail->count, 'comment' => $item->detail->comment, 'total_price' => $item->detail->price,
                'details' => serialize($item->details),
                'toppings' => serialize($toppings), 'status' => 0, 'ok_id' => $ok_id, 'is_buffet' => $item->is_buffet);
        }
        if (count($temp) > 0) {
            $this->db->insert_batch('order_detail', $temp);
        }
        return json_encode(array('order_id' => $order_id, 'ok_id' => $ok_id));
    }

    public function add_people($data) {
        $people = array();
        foreach ($data->detail->people as $p) {
            $temp = array('buffet_id' => $p->buffet_id, 'price' => $p->price, 'number' => $p->number);
            array_push($people, $temp);
        }
        if ($data->order_id == 0) {//add
            $price_start = 0;
            $price_per_hour = 0;
            if (count($data->sum_table) > 0) {
                $table = $data->sum_table[0];
                $a = $this->db->get_where("table_table", array('table_id_auto' => $table));
                if ($a->num_rows() > 0) {
                    $price_start = $a->row()->price_start;
                    $price_per_hour = $a->row()->price_per_hour;
                }
            }
            $data = array('people' => serialize($people), 'hour_number' => $this->get_start_hour(), 'order_id_res' => $this->get_order_id(), 'sum_table' => serialize($data->sum_table), 'id_res_auto' => res_id(), 'up_by' => user_id(), 'cre_by' => user_id(), 'up_date' => date_time_now(), 'status' => 2, 'time_start' => date_time_now(), 'time_end' => date_time_now(), 'price_start' => $price_start, 'price_per_hour' => $price_per_hour);
            $this->db->insert("order_data", $data);
            $order_id = $this->db->insert_id();
            $this->add_charge($order_id);
            return $order_id;
        } else {//edit
            $this->db->update("order_data", array('people' => serialize($people), 'up_date' => date_time_now(), 'up_by' => user_id()), array('id_res_auto' => res_id(), 'order_id' => $data->order_id));
            return $data->order_id;
        }
    }

    public function add_charge($order_id) {
        $cha = $this->db->order_by('sort', 'asc')->get_where("charge", array('id_res_auto' => res_id(), 'status' => 0, 'del' => 0));
        foreach ($cha->result() as $ch) {
            $row = $this->db->get_where("order_charge", array('c_id' => $ch->c_id, 'order_id' => $order_id, 'id_res_auto' => res_id()))->num_rows();
            if ($row == 0) {
                $this->db->insert("order_charge", array('c_id' => $ch->c_id, 'order_id' => $order_id, 'number' => $ch->number, 'id_res_auto' => res_id(), 'per' => $ch->per, 'type' => $ch->type, 'sort' => 0));
            }
        }
    }

    public function get_charge($order_id) {
        $out = array();
        $a = $this->db->select("c_id")->get_where("order_charge", array('order_id' => $order_id));
        foreach ($a->result() as $value) {
            $out[] = $value->c_id - 0;
        }
        return $out;
    }

    public function table_detail($table, $tableAll) {
        $out = array('price_start' => 0, 'price_per_hour' => 0);
        $table = unserialize($table);
        foreach ($tableAll as $value) {
//            pre(unserialize($table));
            if (count($table) > 0 && in_array($value->table_id_auto - 0, array($table[0]))) {
                $out['price_start'] += $value->price_start - 0;
                $out['price_per_hour'] += $value->price_per_hour - 0;
//                $out['zone_id'] = $value->zone_id - 0;
            }
        }
        return $out;
    }

    public function get_zone_id($table, $tableAll) {
        $zone_id = 0;
        $table = unserialize($table);
        foreach ($tableAll as $value) {
//            pre(unserialize($table));
            if (count($table) > 0 && in_array($value->table_id_auto - 0, array($table[0]))) {
                $zone_id = $value->zone_id - 0;
            }
        }
        return $zone_id;
    }

    public function get_hr($minutes) {
        $out = '0';
        $hr = floor($minutes / 60);
        if ($hr > 0) {
            $out = $hr;
        }
        $m = $minutes % 60;
        if ($m > 0) {
            if ($hr > 0) {
                $out .= "." . $m;
            } else {
                $out = "0." . $m;
            }
        }
        return $out - 0;
    }

    public function time_text($minutes) {
        $out = l('not_set');
        $hr = floor($minutes / 60);
        if ($hr > 0) {
            $out = $hr . " " . l('hr') . " ";
        }
        $m = $minutes % 60;
        if ($m > 0) {
            if ($hr > 0) {
                $out .= $m . " " . l('minute');
            } else {
                $out = $m . " " . l('minute');
            }
        }
        return $out;
    }

    public function data_pay_dashboard() {
        $output = array();
        //   $people = $this->db->select("buffet_id,name")->get_where("buffet", array('id_res_auto' => res_id(), 'del' => 0))->result();
        $order = $this->db->order_by("order_id", 'asc')->get_where("order_data", array('del' => 0, 'id_res_auto' => res_id(), 'status' => 2));
        $user = $this->db->select('admin_id as user_id,nick_name,name,username as user_name')->get_where("admin", array('id_res_auto' => res_id()))->result();
        $order_detail = $this->db->order_by("od_id", "desc")->get_where("order_detail", array('id_res_auto' => res_id(), 'del' => 0, 'status >' => -1))->result();
        $food = $this->db->select("f_id,name,pic_id,is_buffet")->get_where("foods", array('del' => 0, 'id_res_auto' => res_id()))->result();
        $topping_sub = $this->db->select('tps_id,tp_id,price,title')->order_by("sort", 'asc')->get_where("topping_sub", array('id_res_auto' => res_id(), 'del' => 0))->result();
        //    $table = $this->db->select('price_start,price_per_hour,table_id_auto,zone_id')->get_where("table_table", array('id_res_auto' => res_id(), 'active' => 2, 'del' => 0))->result();
        foreach ($order->result() as $or) {
            $discounts = array();
            if ($or->discounts != '') {
                $discounts = unserialize($or->discounts);
            }
            $items = $this->get_item($or->order_id, $order_detail, array(
                'f' => $food,
                'ts' => $topping_sub), $user);

            $price_start = $this->get_price_start($or->order_id);
            $sum_item_price = $this->sum_item_price($items);
            $temp = array(
                'sum_items_price' => $sum_item_price,
                'sum_charge' => $this->sum_charge($sum_item_price + $price_start, $or->order_id, $discounts),
                'price_start' => $price_start
            );
            array_push($output, $temp);
        }
        return $output;
    }

    public function data_pay() {
        $output = array();
        $people = $this->db->select("buffet_id,name")->get_where("buffet", array('id_res_auto' => res_id(), 'del' => 0))->result();
        $order = $this->db->order_by("order_id", 'asc')->limit(30)->get_where("order_data", array('del' => 0, 'id_res_auto' => res_id(), 'status' => 2));
        $user = $this->db->select('admin_id as user_id,nick_name,name,username as user_name')->get_where("admin", array('id_res_auto' => res_id()))->result();
        $order_detail = $this->db->order_by("od_id", "desc")->get_where("order_detail", array('id_res_auto' => res_id(), 'del' => 0, 'status >' => -1))->result();
        $food = $this->db->select("f_id,name,pic_id,is_buffet")->get_where("foods", array('del' => 0, 'id_res_auto' => res_id()))->result();
        $topping_sub = $this->db->select('tps_id,tp_id,price,title')->order_by("sort", 'asc')->get_where("topping_sub", array('id_res_auto' => res_id(), 'del' => 0))->result();
        $table = $this->db->select('price_start,price_per_hour,table_id_auto,zone_id')->get_where("table_table", array('id_res_auto' => res_id(), 'active' => 2, 'del' => 0))->result();
        foreach ($order->result() as $or) {
            $or->timeout = floor((strtotime($or->time_start) + ($or->hour_number * 60) - time()) / 60);
            $or->class = '';
            if ($or->timeout < 10 && $or->hour_number != 0) {
                if ($or->timeout < 0) {
                    $or->class = 'bg-danger';
                } else {
                    $or->class = 'bg-warning dker';
                }
            }
            if ($or->member_id - 0 == 0) {
                $member = array(
                    'id' => 0,
                    'name' => $or->title,
                    'pic_id' => 160,
                );
            } else {
                ///select member
                $m = $this->db->get_where("member", array('member_id' => $or->member_id));
                if ($m->num_rows() > 0) {
                    $member = array(
                        'id' => $or->member_id,
                        'name' => $m->row()->n_name,
                        'pic_id' => 160,
                    );
                } else {
                    $member = array(
                        'id' => 0,
                        'name' => $or->title,
                        'pic_id' => 160,
                    );
                }
            }
            $discounts = array();
            if ($or->discounts != '') {
                $discounts = unserialize($or->discounts);
            }
            $items = $this->get_item($or->order_id, $order_detail, array(
                'f' => $food,
                'ts' => $topping_sub), $user);

            $price_start = $this->get_price_start($or->order_id);
            $sum_item_price = $this->sum_item_price($items);
            $temp = array(
                'order_id' => $or->order_id - 0,
                'class' => $or->class,
                'fine' => +$or->fine,
                'discounts' => $discounts,
                'charge' => $this->get_charge($or->order_id - 0),
                'table_detail' => $this->table_detail($or->sum_table, $table),
                'zone_id' => $this->get_zone_id($or->sum_table, $table),
                'sum_table' => unserialize($or->sum_table),
                'items' => $items,
                'hour' => array('minutes' => $or->hour_number - 0, 'hr' => $this->get_hr($or->hour_number), 'time_text' => $this->time_text($or->hour_number)),
                'sum_items_price' => $sum_item_price,
                'type' => count(unserialize($or->sum_table)) > 0 ? 1 : 2, //1 table 2 bill
                'sum_charge' => $this->sum_charge($sum_item_price + $price_start, $or->order_id, $discounts),
                'detail' => array(
                    'title' => $or->title,
                    'member' => $member,
                    'comment' => $or->comment,
                    'table' => $this->get_table_name(unserialize($or->sum_table)),
                    'time' => substr($or->time_start, 11),
                    'timestamp' => strtotime($or->time_start),
                    'people' => $this->get_people($or->people, $people),
                    'price_start' => $price_start,
                    'discount' => $this->get_discount($or->order_id)
                )
            );
            array_push($output, $temp);
        }
        return $output;
    }
  
    public function data_pay2($zone_id) {
        $output = array();
        $id_res_auto = res_id();
        if ($zone_id - 0 != 0) {
            $this->db->where("zone_id", $zone_id);
        }
        if ($zone_id - 0 == -1) {
            $this->db->where("zone_id", 0);
        }
        $table = $this->db->select('table_id_auto')->get_where("table_table", array('id_res_auto' => $id_res_auto, 'active' => 2, 'del' => 0))->result();
        $table_all = array();
        foreach ($table as $value) {
            array_push($table_all, serialize(array($value->table_id_auto - 0)));
        }

        $people = $this->db->select("buffet_id,name")->get_where("buffet", array('id_res_auto' => $id_res_auto, 'del' => 0))->result();
        if (count($table_all) > 0) {
            $this->db->where_in('sum_table', $table_all);
        }
        if ($zone_id - 0 == -1) {
            $this->db->where("sum_table", 'a:0:{}');
        }

        if ($zone_id - 0 == -2) {
            $this->db->where('sum_table', '0');
        }
        $pay_before = $this->session->userdata('pay_before');
        if ($pay_before - 0 == 1) {
            $this->db->where_in("status", array(1, 2));
            $this->db->where("finish", 0);
        } else {
            $this->db->where_in("status", array(2));
        }
//        $this->db->where_in("status", array(2));
        $order = $this->db->order_by("order_id", 'asc')->get_where("order_data", array('del' => 0, 'id_res_auto' => $id_res_auto))->result();

        $order_id_all = array();
        foreach ($order as $or) {
            array_push($order_id_all, $or->order_id - 0);
        }
        $user = $this->db->select('admin_id as user_id,nick_name,name,username as user_name')->get_where("admin", array('id_res_auto' => $id_res_auto))->result();
        if (count($order_id_all) > 0) {
            $this->db->where_in('order_id', $order_id_all);
        } else {
            $this->db->where_in("order_id", array(0));
        }
        $order_detail = $this->db->order_by("od_id", "desc")->get_where("order_detail", array('id_res_auto' => $id_res_auto, 'del' => 0, 'status >' => -1))->result();
        $f_id_all = array();
        foreach ($order_detail as $od) {
            if (!in_array($od->f_id - 0, $f_id_all)) {
                array_push($f_id_all, $od->f_id - 0);
            }
        }
        if (count($f_id_all) > 0) {
            $this->db->where_in('f_id', $f_id_all);
        } else {
            $this->db->where_in('f_id', array(0));
        }
        $food = $this->db->select("f_id,name,pic_id,is_buffet")->get_where("foods", array('del' => 0, 'id_res_auto' => $id_res_auto))->result();
        $topping_sub = $this->db->select('tps_id,tp_id,price,title')->order_by("sort", 'asc')->get_where("topping_sub", array('id_res_auto' => $id_res_auto, 'del' => 0))->result();
//        $table = $this->db->select('price_start,price_per_hour,table_id_auto,zone_id')->get_where("table_table", array('id_res_auto' => res_id(), 'active' => 2, 'del' => 0))->result();
        foreach ($order as $or) {            
            $or->timeout = floor((strtotime($or->time_start) + ($or->hour_number * 60) - time()) / 60);
            $or->class = '';
            if ($or->timeout < 10 && $or->hour_number != 0) {
                if ($or->timeout < 0) {
                    $or->class = 'bg-danger';
                } else {
                    $or->class = 'bg-warning dker';
                }
            }
            if ($or->member_id - 0 == 0) {
                $member = array(
                    'id' => 0,
                    'name' => $or->title,
                    'pic_id' => 160,
                );
            } else {
                ///select member
                $m = $this->db->get_where("member", array('member_id' => $or->member_id));
                if ($m->num_rows() > 0) {
                    $member = array(
                        'id' => $or->member_id,
                        'name' => $m->row()->n_name,
                        'pic_id' => 160,
                    );
                } else {
                    $member = array(
                        'id' => 0,
                        'name' => $or->title,
                        'pic_id' => 160,
                    );
                }
            }
            $discounts = array();
            if ($or->discounts != '') {
                $discounts = unserialize($or->discounts);
            }
            $items = $this->get_item($or->order_id, $order_detail, array(
                'f' => $food,
                'ts' => $topping_sub), $user);
            $process = 0;
//            pre($items);
            foreach ($items as $i) {
                $status =  $i['status']-0;
                if($status==0||$status==1||$status==2){
                    $process= 1;
                }
            }
            $price_start = $this->get_price_start($or->order_id);
            $sum_item_price = $this->sum_item_price($items);
            
            $temp = array(
                'order_id' => $or->order_id - 0,
                'in_process' =>$process,
                'class' => $or->class,
                'paymented' => $or->status - 0 == 1,
                'fine' => +$or->fine,
                'discounts' => $discounts,
                'charge' => $this->get_charge($or->order_id - 0),
                'table_detail' => $this->table_detail($or->sum_table, $table),
                'zone_id' => $zone_id - 0, ////$this->get_zone_id($or->sum_table, $table),
                'sum_table' => unserialize($or->sum_table),
                'items' => $items,
                'hour' => array('minutes' => $or->hour_number - 0, 'hr' => $this->get_hr($or->hour_number), 'time_text' => $this->time_text($or->hour_number)),
                'sum_items_price' => $sum_item_price,
                'type' => count(unserialize($or->sum_table)) > 0 ? 1 : 2, //1 table 2 bill
                'sum_charge' => $this->sum_charge($sum_item_price + $price_start, $or->order_id, $discounts),
                'detail' => array(
                    'title' => $or->title,
                    'member' => $member,
                    'comment' => $or->comment,
                    'table' => $this->get_table_name(unserialize($or->sum_table)),
                    'time' => substr($or->time_start, 11),
                    'timestamp' => strtotime($or->time_start),
                    'people' => $this->get_people($or->people, $people),
                    'price_start' => $price_start,
                    'discount' => $this->get_discount($or->order_id)
                )
            );
            array_push($output, $temp);
        }
        return $output;
    }

    public function sum_charge($price, $order_id, $discounts) {
//        pre($discounts);
        $sum = 0;
        foreach ($discounts as $value) {
            if ($value->op == '-') {
                $price -= $value->money;
                $sum -= $value->money;
            } else {
                $sum += +$value->money;
                $price += +$value->money;
            }
        }

        $a = $this->db->order_by("sort", 'asc')->get_where('order_charge', array('id_res_auto' => res_id(), 'order_id' => $order_id))->result();
        foreach ($a as $value) {

            if ($value->per == 'persen') {
                $per = ceil(($price / 100) * $value->number);
                if ($value->type == "-") {
                    $sum -= $per;
                    $price -= $per;
                } else {
                    $sum += $per;
                    $price += $per;
                }
            } else {
                if ($value->type == "-") {
                    $sum -= $value->number;
                    $price -= $value->number;
                } else {
                    $sum += $value->number;
                    $price += $value->number;
                }
            }
        }

        return round($sum);
    }

    public function sum_charge_bill($price, $order_id, $id_res_auto) {
        $out = array();
        $a = $this->db->order_by("o.sort", 'asc')->select("o.per,o.number,o.type,c.title")->join("charge c", "c.c_id=o.c_id")->get_where('order_charge o', array('o.id_res_auto' => $id_res_auto, 'o.order_id' => $order_id))->result();
        foreach ($a as $value) {
            if ($value->per == 'persen') {
                $per = ($price / 100) * $value->number;
                if ($value->type == '-') {
                    $price -= ceil($per);
                } else {
                    $price += ceil($per);
                }
                array_push($out, array('name' => array_lang($value->title), 'value' => ceil($per), 'per' => $value->per, 'number' => $value->number, 'type' => $value->type));
            } else {
                if ($value->type == '-') {
                    $price -= ceil($value->number);
                } else {
                    $price += ceil($value->number);
                }
                array_push($out, array('name' => array_lang($value->title), 'value' => ceil($value->number), 'per' => $value->per, 'number' => $value->number, 'type' => $value->type));
            }
        }
        return $out;
    }

    function get_table_name($id_table) {
        $name = "";
        if (count($id_table) > 0) {
            $a = $this->db->where_in("table_id_auto", $id_table)->select("name")->get("table_table")->result();
            foreach ($a as $value) {
                if ($name != "") {
                    $name .= "," . $value->name;
                } else {
                    $name .= $value->name;
                }
            }
            return $name;
        } else {
            return "-";
        }
    }

    function get_pic_id($f_id, $food) {
        foreach ($food as $f) {
            if ($f->f_id == $f_id) {
                return $f->pic_id - 0;
            }
        }
        return 0;
    }

    function get_food_name($f_id, $food) {
        foreach ($food as $f) {
            if ($f->f_id == $f_id) {
                return array_lang($f->name);
            }
        }
        return "";
    }

    function get_unit($f_id, $product) {
        foreach ($product as $f) {
            if ($f->material_id == $f_id) {
                return $f->unit;
//                return array_lang($f->name);
            }
        }
        return "";
    }

    function get_product_name($f_id, $product) {
        foreach ($product as $f) {
            if ($f->material_id == $f_id) {
                return $f->name;
//                return array_lang($f->name);
            }
        }
        return "";
    }

    function get_name_eso($id, $a) {
        foreach ($a as $value) {
            if ($value->sf_id == $id) {
                return array_lang($value->name);
            }
        }
        return "";
    }

    function get_extra($a, $b) {
        $ex = unserialize($a);
        $output = array();
        foreach ($ex as $value) {
            $output[] = array('sf_id' => $value['sf_id'], 'price' => $value['price'], 'selected' => $value['selected'] == 1 ? true : false, 'name' => $this->get_name_eso($value['sf_id'], $b), 'dv' => $value['dv']);
        }
        return $output;
    }

    function get_size($a, $b) {
        $si = unserialize($a);
        $output = array();
        foreach ($si as $value) {
            $output[] = array('sf_id' => $value['sf_id'], 'price' => $value['price'], 'selected' => $value['selected'] == 1 ? true : false, 'name' => $this->get_name_eso($value['sf_id'], $b), 'dv' => $value['dv']);
        }
        return $output;
    }

    function get_options($a, $b) {
        $op = unserialize($a);
        $output = array();
        foreach ($op as $value) {
            $output[] = array('sf_id' => $value['sf_id'], 'selected' => $value['selected'] == 1 ? true : false, 'name' => $this->get_name_eso($value['sf_id'], $b), 'dv' => $value['dv']);
        }
        return $output;
    }

    function get_n_name($user, $cre_by) {
        $name = "";
        foreach ($user as $value) {
            if ($value->user_id == $cre_by) {
                if ($value->nick_name !== "") {
                    $name = $value->nick_name;
                } else if ($value->name !== "") {
                    $name = $value->name;
                } else {
                    $name = $value->user_name;
                }
            }
        }
        return $name;
    }

    public function sum_item_price($items) {
        $sum = 0;
        foreach ($items as $value) {
            if ($value['status'] - 0 != 4 && $value['status'] - 0 != -1 && $value['is_buffet'] - 0 != 1) {
                $sum += $value['detail']['price'] * $value['detail']['count'];
            }
        }
        return $sum;
    }

    function get_price_sub($del) {
        $price = 0;
        foreach ($del->sub as $val) {
            if ($val->selected) {
                $price += $val->price - 0;
            }
        }
        return $price;
    }

    function get_price($price, $topping, $details) {
        $temp = 0;
        if (!($topping == '' || $topping == 'a:0:{}')) {
            $toppings = unserialize($topping);
            foreach ($toppings as $value) {
                $temp += ($value['price'] - 0) * $value['count'];
            }
        }
        if (count($details) && $details != '') {
            foreach ($details as $value) {
                $temp += $this->get_price_sub($value);
            }
        }
        return $temp + $price;
    }

    public function get_toppings($topping, $ts) {
        if ($topping == '' || $topping == 'a:0:{}') {
            return array();
        } else {
            $to = unserialize($topping);
            foreach ($to as $key => $value) {
                foreach ($ts as $t) {
                    if ($t->tps_id == $value['tps_id']) {
                        $to[$key]['title'] = array_lang($t->title);
                    }
                }
            }
            return $to;
        }
    }

    function get_item($order_id, $order_detail, $food, $user) {
        $output = array();
        foreach ($order_detail as $or) {
            if ($or->order_id == $order_id) {
                if ($or->type - 0 == 0) {//food
                    $output[] = array(
                        'od_id' => $or->od_id - 0,
                        'f_id' => $or->f_id - 0,
                        'status' => $or->status - 0,
                        'price' => $or->is_buffet != 1 ? $or->price - 0 : 0,
                        'is_buffet' => $or->is_buffet - 0,
                        'name' => $this->get_food_name($or->f_id, $food['f']), 'pic_id' => $this->get_pic_id($or->f_id, $food['f']),
                        'cre_by' => $this->get_n_name($user, $or->cre_by),
                        'detail' => array('count' => $or->count - 0, 'price' => $this->get_price($or->price - 0, $or->toppings, unserialize($or->details)), 'comment' => $or->comment),
                        'details' => unserialize($or->details),
                        'toppings' => $this->get_toppings($or->toppings, $food['ts'])
                    );
                } else if ($or->type - 0 == 1) {//product
                    $output[] = array(
                        'od_id' => $or->od_id - 0,
                        'f_id' => $or->f_id - 0,
                        'status' => $or->status - 0,
                        'price' => $or->price - 0,
                        'is_buffet' => $or->is_buffet - 0,
                        'name' => '', // $this->get_product_name($or->f_id, $food['p']),
                        'unit' => '', //$this->get_unit($or->f_id, $food['p']),
                        'pic_id' => 0,
                        'cre_by' => $this->get_n_name($user, $or->cre_by),
                        'detail' => array('count' => $or->count - 0, 'price' => $or->price - 0, 'comment' => $or->comment),
                        'details' => [],
                        'toppings' => []
                    );
                }
            }
        }


        return $output;
    }

    function get_people($people, $people_all) {
        if ($people == "") {
            return array();
        }
        $p = unserialize($people);
        $output = array();
        foreach ($p as $value) {
            $temp = array('buffet_id' => $value['buffet_id'], 'price' => $value['price'], 'number' => $value['number'] - 0, 'name' => $this->get_name($value['buffet_id'], $people_all));
            array_push($output, $temp);
        }
        return $output;
    }

    private function get_name($buffet_id, $buffet_all) {
        foreach ($buffet_all as $value) {
            if ($value->buffet_id == $buffet_id) {
                return array_lang($value->name);
            }
        }
        return "";
    }

    function get_discount($order_id) {
        return 50;
    }

    function get_vat($order_id) {
        return 7;
    }

    function get_price_start($order_id) {
        $sum = 0;
        $a = $this->db->get_where("order_data", array('order_id' => $order_id, 'id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            $order_data = $a->row();
            $table = unserialize($order_data->sum_table);
            if (count($table) > 0) {
                foreach ($table as $value) {
                    $ta = $this->db->select("price_per_hour,price_start")->get_where("table_table", array('table_id_auto' => $value));
                    if ($ta->num_rows() > 0) {
                        if ($a->row()->hour_number - 0 == 0) {
                            $time = time() - strtotime($a->row()->time_start);
                            $sum += $ta->row()->price_per_hour / 60 * ($time / 60);
                        } else {
                            $sum += ($ta->row()->price_per_hour / 60 * $a->row()->hour_number);
                        }
                        $sum += $ta->row()->price_start - 0;
                    }
                }
            }
            $people = unserialize($order_data->people);
            foreach ($people as $value) {
                $sum += $value['price'] * $value['number'];
            }
            if ($order_data->fine - 0 > 0) {
                $sum += $order_data->fine - 0;
            }
        }

        return $sum;
    }

    public function get_order($order_data, $id_res_auto, $table) {
        $data = $order_data->row();
        $data->price_net = 0;
        $data->flag = true;
        $data->table = $table;
        $data->price_hour = 0;
        $data->price_buffet = 0;
        $data->price_food = 0;
        $data->price_details = array();
        if (isset($table->price_start) && $table->price_start - 0 != 0) {
            array_push($data->price_details, array('text' => l('start_price'), 'price' => +$table->price_start));
        }
        $toppings = $this->db->select("tps_id,title")->get_where("topping_sub", array('id_res_auto' => $id_res_auto))->result();
        foreach ($toppings as $key => $value) {
            $toppings[$key]->title = array_lang($value->title);
        }
        $res = $this->db->select('pay_before')->get_where('restaurant', array('id_res_auto' => $id_res_auto));
        $pay_before = false;
        if ($res->num_rows() > 0) {
            if ($res->row()->pay_before - 0 === 1) {
                $pay_before = true;
            } else {
                $pay_before = false;
            }
        }

        if (isset($table->price_per_hour) && $table->price_per_hour - 0 != 0) {
            if ($data->hour_number - 0 != 0) {
                $hour = floor($data->hour_number / 60);
                $min = $data->hour_number % 60;
                $text = l('price_house');
                if ($hour > 0) {
                    $text .= $hour . " " . l('hr') . " ";
                }
                if ($min > 0) {
                    $text .= $min . " " . l('minute');
                }
                $text .= " x " . $table->price_per_hour . " " . get_currency() . "/" . l('hr');
                $data->price_hour = ceil($table->price_per_hour / 60 * $data->hour_number);
                array_push($data->price_details, array('text' => $text, 'price' => $data->price_hour));
            } else {
                $start = time() - strtotime($data->time_start);
                $start = floor($start / 60);
                $text = l('price_house') . " ";
                $hour = floor($start / 60);
                $min = floor($start % 60);
                if ($hour > 0) {
                    $text .= $hour . " " . l('hr') . " ";
                }
                if ($min > 0) {
                    $text .= $min . " " . l('minute');
                }
                $text .= " x " . $table->price_per_hour . " " . get_currency() . "/" . l('hr');
                $data->price_hour = ceil($table->price_per_hour / 60 * $start);
                array_push($data->price_details, array('text' => $text, 'price' => $data->price_hour));
            }
        }
        $data->sum_table = unserialize($data->sum_table);
        $people = unserialize($data->people);
        $data->people = array();
        foreach ($people as $k => $p) {
            if ($p['number'] - 0 != 0) {
                $name = $this->db->select("name")->get_where("buffet", array('buffet_id' => $p['buffet_id'], 'id_res_auto' => $id_res_auto));
                if ($name->num_rows() > 0) {
                    $p['name'] = array_lang($name->row()->name);
                    $data->price_net += $p['price'] * ($p['number'] - 0);
                    array_push($data->people, $p);
                }
            }
        }
        if (count($data->people) > 0) {
            $buffet_sum = 0;
            foreach ($data->people as $peo) {
                $buffet_sum += $peo['price'] * $peo['number'];
            }
            $data->price_buffet = +$buffet_sum;
            array_push($data->price_details, array('text' => l('buffet'), 'price' => +$buffet_sum));
        }
        $data->sub = $this->db->order_by("d.status", 'asc')->select("d.od_id,d.price,f.name,d.count,d.details,d.toppings,d.q,d.status,f.pic_id,d.ok_id,d.comment,d.is_buffet,d.up_by")->join("foods f", "f.f_id=d.f_id")->get_where("order_detail d", array('d.order_id' => $data->order_id, 'd.id_res_auto' => $id_res_auto, 'd.status !=' => -1, 'd.del' => 0))->result();
        $sum_food_price = 0;
        foreach ($data->sub as $key => $v) {
            $v->price_net = $v->price;
            $v->status -= 0;
            $v->is_buffet -= 0;
            $v->details = unserialize($v->details);
            if (!$v->details) {
                $v->details = array();
            }
            if ($v->details) {
                foreach ($v->details as $de) {
                    foreach ($de->sub as $s) {
                        if ($s->selected) {
                            $v->price_net += ($s->price - 0);
                        }
                    }
                }
            } else {
                $v->details = array();
            }
            $data->sub[$key]->q = $this->get_q($v->ok_id, $id_res_auto, $pay_before);
            $data->sub[$key]->pic_id = base_url() . "pic/" . $v->pic_id . "?type=small";
            $data->sub[$key]->name = array_lang($v->name);
            $data->sub[$key]->toppings = $this->get_topping($v->toppings, $toppings);

            foreach ($data->sub[$key]->toppings as $ext) {
                $data->sub[$key]->price_net += $ext['price'] * $ext['count'];
            }
            $data->sub[$key]->price_net *= $data->sub[$key]->count;
            if ($data->sub[$key]->status != 4 && $data->sub[$key]->is_buffet != 1) {
                $data->price_net += $data->sub[$key]->price_net;
            } else {
                $data->sub[$key]->price_net = 0;
            }
            if ($data->sub[$key]->is_buffet == 1) {
                $data->sub[$key]->price_net = 0;
            }
            $sum_food_price += $data->sub[$key]->price_net;
        }

        if ($sum_food_price > 0) {
            $data->price_food = +$sum_food_price;
            array_push($data->price_details, array('text' => l('price_food_total'), 'price' => $sum_food_price));
        }
        if ($data->fine - 0 > 0) {
            array_push($data->price_details, array('text' => l('fine'), 'price' => $data->fine - 0));
        }
        $data->change = $this->db->select('oc.number,oc.per,oc.type,c.title')->join("charge c", "c.c_id=oc.c_id")->get_where("order_charge oc", array('oc.order_id' => $data->order_id, 'oc.id_res_auto' => $id_res_auto, 'c.id_res_auto' => $id_res_auto))->result();
        $data->price_net_final = 0;
        foreach ($data->price_details as $pd) {
            $data->price_net_final += $pd['price'];
        }
        $data->price_charge = 0;
        if ($data->discounts == '') {
            $data->discounts = array();
        } else {
            $data->discounts = unserialize($data->discounts);
        }
        foreach ($data->discounts as $dis) {
            if ($dis->op == '-') {
                $data->price_charge -= $dis->money;
                $data->price_net_final -= $dis->money;
                array_push($data->price_details, array('text' => $dis->title, 'price' => $dis->money));
            } else {
                $data->price_charge += +$dis->money;
                $data->price_net_final += +$dis->money;
                array_push($data->price_details, array('text' => $dis->title, 'price' => "-" . $dis->money));
            }
        }
        foreach ($data->change as $key => $value) {
            $data->change[$key]->title = array_lang($value->title);
            $data->change[$key]->price = ceil($value->per == 'persen' ? $value->number * $data->price_net_final / 100 : $value->number);
            $data->change[$key]->text = $value->title . " (" . $value->type . " " . $value->number . ($value->per == 'persen' ? "%" : get_currency()) . ") ";
            if ($value->type == '-') {
                $data->price_net_final -= $data->change[$key]->price;
                $data->price_charge -= $data->change[$key]->price;
            } else {
                $data->price_net_final += $data->change[$key]->price;
                $data->price_charge += $data->change[$key]->price;
            }
            array_push($data->price_details, array('text' => $data->change[$key]->text, 'price' => $data->change[$key]->price));
        }

        array_push($data->price_details, array('text' => "<b>" . l('net_price') . "</b>", 'price' => $data->price_net_final));
        return $data;
    }

    public function get_q($ok_id, $id_res_auto, $pay_before) {
        $date = date('Y-m-d H:i:s', strtotime("-1 day", time()));
        $this->db->where("od.up_date >", $date);
//        $res = $this->db->select('pay_before')->get_where('restaurant', array('id_res_auto' => $id_res_auto));
//        if ($res->num_rows() > 0) {
        if ($pay_before) {
            $this->db->where_in('o.status', array(2, 1));
        } else {
            $this->db->where_in('o.status', array(2));
        }
//        }
//        $this->db->where_in('o.status', array(2));
        $ok = $this->db->group_by("od.ok_id")->join("order_data o", 'o.order_id=od.order_id')->get_where("order_detail od", array('od.id_res_auto' => $id_res_auto, 'od.ok_id <' => $ok_id, 'od.status' => 0, 'o.del' => 0, 'od.del' => 0));
        return $ok->num_rows() + 1;

//        $ok = $this->db->select("count(*) as count")->get_where("order_kitchen", array('ok_id <=' => $ok_id, 'id_res_auto' => $id_res_auto, 'close' => 0));
//        return $ok->row()->count + 1;
    }

    public function get_topping($topping, $toppings) {
        if ($topping == "") {
            return array();
        } else {
            $t = unserialize($topping);
            foreach ($t as $key => $value) {
                foreach ($toppings as $k2 => $v2) {
                    if ($value['tps_id'] == $v2->tps_id) {
                        $t[$key]['title'] = $v2->title;
                    }
                }
            }
            return $t;
        }
    }

    public function bill_cus($order_id) {
        $order = $this->db->get_where("order_data", array('order_id' => $order_id));
        if ($order->num_rows() > 0) {
            $data = $order->row();
            $res = $this->db->select("notice,name,address,logo_url")->get_where("restaurant", array('id_res_auto' => $data->id_res_auto));
            if ($res->num_rows() > 0) {
                $bill = $res->row();
                $bill->name = array_lang($bill->name);
                $bill->address = array_lang($bill->address);
                $bill->bill = unserialize($bill->notice);
                $bill->bill->footer = array_lang($bill->bill->footer);


                $table = unserialize($data->sum_table);
                $buffet_temp = unserialize($data->people);
                $buffet = array();
                $sum = 0;
                foreach ($buffet_temp as $bu) {
                    $name = $this->db->select("name")->get_where("buffet", array('buffet_id' => $bu['buffet_id'], 'id_res_auto' => $data->id_res_auto));
                    if ($name->num_rows() > 0 && $bu['price'] - 0 != 0 && $bu['number'] - 0 != 0) {
                        $sum += $bu['price'] * $bu['number'];
                        array_push($buffet, array('name' => array_lang($name->row()->name), 'price' => $bu['price'], 'number' => $bu['number']));
                    }
                }
                $table_text = $data->title;
                $t = array();
                $price_hour = array();
                $price_start = 0;
                $is_table = false;
                if (count($table) > 0) {
                    $is_table = true;
                    foreach ($table as $t_v) {
                        array_push($t, $t_v);
                    }
                    $table_temp = $this->db->select("name,price_per_hour,price_start")->where_in("table_id_auto", $t)->get("table_table")->result();
                    foreach ($table_temp as $key => $t_temp) {
                        if ($key == 0) {
                            $table_text = $t_temp->name;
                            if ($t_temp->price_per_hour - 0 > 0) {
                                if ($data->hour_number != 0) {
                                    array_push($price_hour, array('text' => l("price_house"), 'number' => $data->hour_number, 'price' => $t_temp->price_per_hour));
                                } else {
                                    $time = time() - strtotime($data->time_start);
                                    array_push($price_hour, array('text' => l("price_house"), 'number' => floor($time / 60), 'price' => $t_temp->price_per_hour));
                                }
                            }
                        } else {
                            $table_text .= ", " . $t_temp->name;
                        }
                        $price_start += $t_temp->price_start - 0;
                    }
                } else {
//                    $table_text = $data->title;
                }
                $order_detail = $this->db->select("d.price,f.name,d.count,d.details,d.toppings,f.f_id")->join("foods f", "f.f_id=d.f_id")->get_where("order_detail d", array('d.order_id' => $order_id, 'd.status!=' => -1, 'd.del' => 0, 'd.status !=' => 4, 'd.is_buffet' => 0))->result();
                ?>
                <table border="0" style="width: <?php
                if ($paper_width == '1') {
                    echo '270px';
                } else if ($paper_width == '2') {
                    echo '170px';
                }
                ?>;">
                    <thead>
                        <?php
                        if ($bill->bill->show_logo - 0 == 1) {
                            ?>
                            <tr>
                                <th colspan="3" class="text-center">                                 
                                    <img src="<?php
                                    echo get_server();
                                    echo $bill->logo_url;
                                    ?>">

                                </th>
                            </tr>
                            <?php
                        }
                        if ($bill->bill->show_name - 0 == 1) {
                            ?>
                            <tr>
                                <th colspan="3" class="text-center">
                                    <?php echo $bill->name; ?>
                                </th>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th colspan="3" style="text-align: center;">
                                <b> <?php echo l("food_bill"); ?>  </b>
                            </th>
                        </tr>
                        <?php
                        if ($bill->bill->show_add_top - 0 == 1) {
                            ?>
                            <tr>
                                <th colspan="3" class="text-center" style="font-weight: 100;">
                                    <?php echo $bill->address; ?>
                                </th>
                            </tr>
                            <?php
                        }
                        ?>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3" > Order ID : <?php echo $data->order_id_res; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" > <?php echo l("date"); ?> :<?php echo date("d/m/") . (date("Y") + 543) . " " . date("H:i:s"); ?></td>
                        </tr>
                        <?php if ($is_table) { ?>
                            <tr>
                                <td colspan="3" > <?php echo l("table"); ?> : <?php echo $table_text; ?></td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="3" > <?php echo l("cus"); ?> : <?php echo $table_text; ?></td>
                            </tr>
                        <?php } ?>
                        <tr class="items">
                            <td class="text-bold"><?php echo l("menu_goods"); ?></td>
                            <td class="text-center text-bold"><?php echo l("number"); ?></td>
                            <td class="text-center text-bold" ><?php echo l("price"); ?></td>
                        </tr>
                        <?php
                        if ($price_start != 0) {
                            $sum += $price_start;
                            ?>
                            <tr >
                                <td><?php echo l('open_table'); ?></td>
                                <td></td>
                                <td  class="right"><?php echo number_format($price_start, 2); ?></td>
                            </tr>
                            <?php
                        }
                        foreach ($price_hour as $value) {
                            $price_hour2 = round(($value['price'] / 60) * $value['number']);
                            $sum += $price_hour2;
                            ?>
                            <tr >
                                <td><?php echo $value['text']; ?></td>
                                <td class="text-center"><?php echo get_time_text($value['number']); ?></td>
                                <td  class="right"><?php echo number_format($price_hour2, 2); ?></td>
                            </tr>
                            <?php
                        }
                        foreach ($buffet as $value) {
                            ?>
                            <tr >
                                <td><?php echo l("buffet"); ?> <?php echo $value['name']; ?></td>
                                <td  class="text-center"><?php echo $value['number']; ?></td>
                                <td class="right"><?php echo number_format($value['price'] * $value['number'], 2); ?></td>
                            </tr>
                            <?php
                        }
                        $out1 = array();

                        foreach ($order_detail as $key => $value) {
                            $details = unserialize($value->details);
                            if (!$details) {
                                $details = array();
                            }
                            if ($value->toppings != '') {
                                $toppings = unserialize($value->toppings);
                            } else {
                                $toppings = array();
                            }
                            $value->count -= 0;
                            $is_details = false;
                            $new_price = 0;
                            $new_price += $value->price;
                            $detail_new = array();

                            foreach ($details as $e) {
                                $flag = false;
                                foreach ($e->sub as $value5) {
                                    if ($value5->selected - 0 == 1) {
                                        $flag = true;
                                    }
                                }
                                if ($flag) {
                                    $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                                    foreach ($e->sub as $v) {
                                        if ($v->selected - 0 == 1) {
                                            if ($flag) {
                                                $flag = false;
                                                $tt['text'] .= array_lang_byarray($v->name);
                                            } else {
                                                $tt['text'] .= ", " . array_lang_byarray($v->name);
                                            }
                                            $tt['price'] += $v->price;
                                        }
                                    }
                                    if ($tt['price'] - 0 != 0) {
                                        $new_price += $tt['price'];
                                        array_push($detail_new, $tt);
                                    }
                                }
                            }
                            foreach ($toppings as $values) {
                                $new_price += $values['price'] * $values['count'];
                            }
                            $value->sum_price = $new_price;
                            $flag = true;
                            foreach ($out1 as $k => $o) {
                                if ($o->f_id == $value->f_id && $o->sum_price == $value->sum_price) {
                                    $flag = false;
                                    $out1[$k]->count += $value->count - 0;
                                }
                            }
                            if ($flag) {
                                array_push($out1, $value);
                            }
                        }

                        foreach ($out1 as $key => $value) {
                            $details = unserialize($value->details);
                            if (!$details) {
                                $details = array();
                            }
                            if ($value->toppings != '') {
                                $toppings = unserialize($value->toppings);
                            } else {
                                $toppings = array();
                            }
                            $is_details = false;
                            $new_price = 0;
                            $new_price += $value->price;
                            $detail_new = array();

                            foreach ($details as $e) {
                                $flag = false;
                                foreach ($e->sub as $value5) {
                                    if ($value5->selected - 0 == 1) {
                                        $flag = true;
                                    }
                                }
                                if ($flag) {
                                    $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                                    foreach ($e->sub as $v) {
                                        if ($v->selected - 0 == 1) {
                                            if ($flag) {
                                                $flag = false;
                                                $tt['text'] .= array_lang_byarray($v->name);
                                            } else {
                                                $tt['text'] .= ", " . array_lang_byarray($v->name);
                                            }
                                            $tt['price'] += $v->price;
                                        }
                                    }
                                    if ($tt['price'] - 0 != 0) {
                                        $new_price += $tt['price'];
                                        array_push($detail_new, $tt);
                                    }
                                }
                            }
                            foreach ($toppings as $values) {
                                $new_price += $values['price'] * $values['count'];
                            }
                            $sum_price = $value->count * $new_price;
                            $sum += $sum_price;
                            if ($key + 1 == count($order_detail)) {
                                ?>
                                <tr>
                                    <td style="border-bottom: solid 1px #000;"><?php
                                        echo show_text_by_lang($value->name);
                                        if (count($detail_new)) {
                                            echo " (" . $value->price . ")";
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($detail_new as $de) {
                                                    echo "  " . $de['text'] . " (" . show_pre_int($de['price']) . ")<br>";
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        if (count($toppings) > 0) {
                                            if (!count($detail_new)) {
                                                echo " (" . $value->price . ")";
                                            }
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($toppings as $value2) {
                                                    $tps = $this->db->select("title")->get_where("topping_sub", array('id_res_auto' => res_id(), 'tps_id' => $value2['tps_id']));
                                                    if ($tps->num_rows() > 0) {
                                                        echo " [" . $value2['count'] . "] " . array_lang($tps->row()->title) . "(" . show_pre_int($value2['price'] * $value2['count']) . ")<br>";
                                                    }
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        ?></td>
                                    <td  class="text-center"><?php echo $value->count; ?></td>
                                    <td  class="right"><?php echo number_format($sum_price, 2); ?></td>
                                </tr>
                                <?php
                            } else {
                                ?>
                                <tr>
                                    <td ><?php
                                        echo show_text_by_lang($value->name);
                                        if (count($detail_new)) {
                                            echo " (" . $value->price . ")";
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($detail_new as $de) {
                                                    echo "  " . $de['text'] . " (" . show_pre_int($de['price']) . ")<br>";
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        if (count($toppings) > 0) {
                                            if (!count($detail_new)) {
                                                echo " (" . $value->price . ")";
                                            }
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($toppings as $value2) {
                                                    $tps = $this->db->select("title")->get_where("topping_sub", array('id_res_auto' => res_id(), 'tps_id' => $value2['tps_id']));
                                                    if ($tps->num_rows() > 0) {
                                                        echo "  [" . $value2['count'] . "] " . array_lang($tps->row()->title) . "(" . show_pre_int($value2['price'] * $value2['count']) . ")<br>";
                                                    }
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        ?></td>
                                    <td  class="text-center"><?php echo $value->count; ?></td>
                                    <td  class="right"><?php echo number_format($sum_price, 2); ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr class="price">
                            <td colspan="2" style="padding-left: 30px; "><?php echo l("price"); ?> </td>
                            <td  class="right"><?php echo number_format($sum, 2); ?></td>
                        </tr>
                        <?php
                        if ($data->fine - 0 != 0) {
                            ?>
                            <tr>
                                <td colspan="2" style="padding-left: 30px; "><?php echo l('fine'); ?> </td>
                                <td  class="right"><?php echo number_format($data->fine, 2); ?></td>
                            </tr>
                            <?php
                            $sum += $data->fine;
                        }
                        if ($data->discounts != '') {
                            foreach (unserialize($data->discounts) as $dis) {
                                ?>
                                <tr>
                                    <td colspan="2" style="padding-left: 30px; "><?php echo $dis->title; ?> </td>
                                    <td  class="right"><?php echo $dis->op . " " . number_format($dis->money, 2); ?></td>
                                </tr>
                                <?php
                                if ($dis->op == '-') {
                                    $sum -= $dis->money;
                                } else {
                                    $sum += +$dis->money;
                                }
                            }
                        }
                        $charge = $this->Pos_model->sum_charge_bill($sum, $order_id, $data->id_res_auto);
                        foreach ($charge as $value) {
                            if ($value['type'] == '-') {
                                $sum -= $value['value'];
                            } else {
                                $sum += $value['value'];
                            }
                            ?>
                            <tr>
                                <td colspan="2" ><?php
                                    echo $value['name'] . " (" . $value['type'] . $value['number'] . " ";
                                    echo $value['per'] == 'persen' ? '%' : get_currency();
                                    echo ")";
                                    ?></td>
                                <td  class="right"><?php echo $value['type'] . number_format($value['value'], 2); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td colspan="2" class="text-bold"><?php echo l("net_price"); ?></td>
                            <td  class="right text-bold"><?php echo number_format($sum, 2); ?>
                            </td>
                        </tr>
                        <?php
                        if ($bill->bill->show_add_bottom - 0 == 1) {
                            ?>
                            <tr  class="text-center" >
                                <td colspan="3"  >
                                    <?php echo $bill->address; ?>
                                </td>
                            </tr>
                            <?php
                        }
                        if ($bill->bill->footer != '') {
                            ?>
                            <tr class="text-center" >
                                <td colspan="3"   >
                                    <?php echo nl2br($bill->bill->footer); ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>

                <?php
            } else {
                echo l("cannot_find_bill");
            }
        } else {
            echo l("cannot_find_bill");
        }
//        $this->db->close();
    }

}
