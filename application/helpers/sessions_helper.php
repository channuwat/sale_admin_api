<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function get_currency() {
    $ci = & get_instance();
    $user = $ci->session->userdata();
    return array_lang($user['currency_full']);
}

function base_url2() {
    return 'https://sale.deltafood.co/';
}

function get_time_text($min) {
    $diff_date = $min * 60;
    $num_years = $diff_date / 31536000;
    $num_months = ($diff_date % 31536000) / 2628000;
    $num_days = floor((($diff_date % 31536000) % 2628000) / 86400);
    $num_hour = floor(((($diff_date % 31536000) % 2628000) % 86400) / 3600);
    $num_minutes = floor((((($diff_date % 31536000) % 2628000) % 86400) % 3600) / 60);
    $num_seconds = round(($diff_date) % 60);
    $out = '';
    if ($num_days > 0) {
        $out .= $num_days . " " . l('day') . " ";
    }
    if ($num_hour > 0) {
        $out .= $num_hour . " " . l('hr') . " ";
    }
    if ($num_minutes > 0) {
        $out .= $num_minutes . " " . l('minute');
    }
    return $out;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => l('year'),
        'm' => l('month'),
        'w' => l("week"),
        'd' => l('day'),
        'h' => l('hr'),
        'i' => l('minute'),
        's' => l('second'),
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? '' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ' . l('ago') : l('moment'); //just now
}

function show_pre_int($int) {
    if ($int == 0) {
        return l('free');
    } else if ($int > 0) {
        return "+" . $int;
    } else {
        return $int;
    }
}

function date_to_slad($date) {
    $a = explode("-", $date);
    if (count($a) == 3) {
        return $a[2] . "/" . $a[1] . "/" . $a[0];
    } else {
        return "00/00/0000";
    }
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function random_table_code() {
    $CI = & get_instance();
    while (true) {
        $ran = generateRandomString(10);
        $table = $CI->db->get_where("table_table", array('code' => $ran));
        if ($table->num_rows() == 0) {
            return $ran;
        }
    }
}

function date_to_str($date) {
    $thai_month_arr = array(
        "0" => "",
        "1" => "มกราคม",
        "2" => "กุมภาพันธ์",
        "3" => "มีนาคม",
        "4" => "เมษายน",
        "5" => "พฤษภาคม",
        "6" => "มิถุนายน",
        "7" => "กรกฎาคม",
        "8" => "สิงหาคม",
        "9" => "กันยายน",
        "10" => "ตุลาคม",
        "11" => "พฤศจิกายน",
        "12" => "ธันวาคม"
    );

    $months = array(
        '0' => '',
        '1' => 'January',
        '2' => 'February',
        '3' => 'March',
        '4' => 'April',
        '5' => 'May',
        '6' => 'June',
        '7' => 'July ',
        '8' => 'August',
        '9' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    );
    $la_month_arr = array(
        "0" => "",
        "1" => "ມັງກອນ",
        "2" => "ກຸມພາ",
        "3" => "ມີນາ",
        "4" => "ເມສາ",
        "5" => "ພຶດສະພາ",
        "6" => "ມິຖຸນາ",
        "7" => "ກໍລະກົດ",
        "8" => "ສິງຫາ",
        "9" => "ກັນຍາ",
        "10" => "ຕຸລາ",
        "11" => "ພະຈິກ",
        "12" => "ທັນວາ"
    );
    $date_temp = explode("-", $date);
    $out = '';
    if (strtolower(lang()) == 'th') {
        if (count($date_temp) === 2) {
            $out = $thai_month_arr[$date_temp[1]] . " " . ($date_temp[0] + 543);
        } else if (count($date_temp) === 3) {
//            echo $thai_month_arr[$date_temp[1]-0].'-'.$date_temp[1] ;
            $out = $date_temp[2] . " "
                    . $thai_month_arr[$date_temp[1] - 0] .
                    " " .
                    ($date_temp[0] + 543);
        } else {
            return $date_temp[0] + 543;
        }
    } else if (strtolower(lang()) == 'la') {
        if (count($date_temp) === 2) {
            $out = $la_month_arr[$date_temp[1]] . " " . ($date_temp[0] + 543);
        } else if (count($date_temp) === 3) {
//            echo $thai_month_arr[$date_temp[1]-0].'-'.$date_temp[1] ;
            $out = $date_temp[2] . " "
                    . $la_month_arr[$date_temp[1] - 0] .
                    " " .
                    ($date_temp[0] + 543);
        } else {
            return $date_temp[0] + 543;
        }
    } else {
        if (count($date_temp) === 2) {
            $out = $months[$date_temp[1] - 0] . " " . $date_temp[0];
        } else if (count($date_temp) === 3) {
            $out = $date[2] . " " . $months[$date_temp[1] - 0] . " " . $date_temp[0];
        } else {
            return $date_temp[0];
        }
    }
    return $out;
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function res_id() {
    $ci = & get_instance();
    return $ci->session->userdata("id_res_auto");
}

function file_url() {

    return get_server() . 'upload/server/php/files/';
}

function get_server() {
    $actual_link = "http://$_SERVER[HTTP_HOST]";
    $ci = &get_instance();
    return $actual_link . "/" . $ci->config->config['uri'];
}

function _post() {
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);
    return $request;
}

function get_time_now() {
    return date("Y-m-d H:i:s");
}

function res_arr() {
    $ci = & get_instance();
    return $ci->session->userdata("res_arr");
}

function post($index = "") {
    $ci = &get_instance();
    return $index == "" ? $ci->input->post() : $ci->input->post($index);
}

function get($index) {
    $ci = &get_instance();
    return $index == "" ? $ci->input->get() : $ci->input->get($index);
}

function userdata($str) {
    $ci = & get_instance();
    return $ci->session->userdata($str);
}

function user_id() {
    $ci = & get_instance();
    return $ci->session->userdata("admin_id");
}

function nick_name($id = 0) {
    $ci = & get_instance();
    if ($id == 0) {
        $ci = & get_instance();
        return $ci->session->userdata("name");
    } else {
//        return $id;
        $admin = $ci->db->get_where("admin", array('admin_id' => $id));
        if ($admin->num_rows() > 0) {
            return $admin->row()->name;
        } else {
            return "";
        }
    }
}

function set_userdata($str, $val) {
    $ci = & get_instance();
    return $ci->session->set_userdata($str, $val);
}

function lang() {
    $ci = & get_instance();
    if ($ci->session->userdata("lang")) {
        return $ci->session->userdata("lang");
    } else {
        $ci->session->set_userdata('lang', 'th');
        return 'th';
    }
}

function l($word) {
    $ci = & get_instance();
    return $ci->lang->line($word);
}

function date_to_db($date) {
    $a = explode("/", $date);
    if (count($a) == 3) {
        if ($a[2] - 0 > 2500) {
            $a[2] -= 543;
        }
        return $a[2] . '-' . $a[1] . "-" . $a[0];
    } else {
        return '0000-00-00';
    }
}

function array_lang($a = '', $lang = '') {
    if ($a == "") {
        return "";
    }
    if ($lang == '') {
        $lang = userdata("lang");
    }
    $b = unserialize($a);
//    echo userdata('lang');
    if (isset($b[strtolower($lang)]) && $b[strtolower($lang)] !== "") {
        return $b[strtolower($lang)];
    } else if (isset($b[strtoupper($lang)]) && $b[strtoupper($lang)] !== "") {
        return $b[strtoupper($lang)];
    } else {
        $ci = & get_instance();
        $aa = $ci->db->select('shot')->limit(1)->order_by("sort", 'asc')->get_where("lang_res", array('id_res_auto' => res_id()));
        if ($aa->num_rows() > 0) {
            if (isset($b[$aa->row()->shot])) {
                return $b[$aa->row()->shot];
            } else {
                return array_lang_not($b);
            }
        } else {
            return array_lang_not($b);
        }
    }
}

function array_lang_byarray($b, $lang = '') {
    if ($b == "" || count($b) == 0) {
        return "";
    }
//    return 'xxx';
    if ($lang == '') {
        $lang = userdata("lang");
    }
//    pre($b);
    foreach ($b as $value) {
        if ($value->shot == strtolower($lang) || $value->shot == strtoupper($lang)) {
            return $value->title;
        }
    }

    $ci = & get_instance();
    $aa = $ci->db->select('shot')->limit(1)->order_by("sort", 'asc')->get_where("lang_res", array('id_res_auto' => res_id()));
    if ($aa->num_rows() > 0) {
        if (isset($b[$aa->row()->shot])) {
            return $b[$aa->row()->shot]->title;
        } else {
            return array_lang_not($b);
        }
    } else {
        return array_lang_not($b);
    }
}

function get_long_text($shot) {
    $ci = & get_instance();
    $a = $ci->db->get_where("lang_res", array('id_res_auto' => res_id(), 'shot' => $shot));
    if ($a->num_rows() > 0) {
        return $a->row()->long_text;
    } else {
        return "";
    }
}

function array_lang_not($b) {
    foreach ($b as $value) {
        if ($value !== "") {
            return $value;
        }
    }
    return 'Not set';
}

function pre($array) {
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function date_time_now() {
    return date("Y-m-d H:i:s");
}

function show_text_by_lang($text) {
    $text2 = unserialize($text);
//    pre($text2);
//    echo lang();
    if (isset($text2[strtolower(lang())])) {
        return $text2[lang()];
    } else {
        return l('not_set');
    }
}

function get_name_show($name) {
    $ci = & get_instance();
    $name = unserialize($name);
    $temp = array();
    $b = $ci->db->order_by("sort", 'asc')->get_where("lang_res", array('id_res_auto' => res_id()));
    if ($b->num_rows() > 0) {
        foreach ($b->result() as $val) {
            $a = array('shot' => strtoupper($val->shot), 'title' => isset($name[strtolower($val->shot)]) ? $name[strtolower($val->shot)] : '', 'long_text' => get_long_text($val->shot));
            array_push($temp, $a);
        }
    }
    return $temp;
}

function get_pic_show($pic_id) {
    $ci = & get_instance();
    if ($pic_id != 0) {
        $a = $ci->db->get_where("pictures", array('id_res_auto' => res_id(), 'pic_id' => $pic_id));
        if ($a->num_rows() > 0) {
            $p = $a->row()->name;
        } else {
            $p = "14577250065834.jpg";
        }
    } else {
        $p = "14577250065834.jpg";
    }
    return $p;
}

function mullti_lang_todb($text) {
    $temp_name = array();
    foreach ($text as $val) {
        $temp_name[strtolower($val->shot)] = $val->title;
    }
    return serialize($temp_name);
}

function checkRemoteFile($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    // don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    if (curl_exec($ch) !== FALSE) {
        return true;
    } else {
        return false;
    }
}

?>