<?php

function get_balance_sale($f_id) {
    $ci = & get_instance();
    $st = $ci->db->order_by('st_id', 'desc')->limit(1)->get_where("stock_transaction", array('f_id' => $f_id, 'id_res_auto' => res_id()));
    if ($st->num_rows() > 0) {
        return $st->row()->balance_sale - 0;
    } else {
        return 0;
    }
}

function get_balance_buy($f_id) {
    $ci = & get_instance();
    $st = $ci->db->order_by('st_id', 'desc')->limit(1)->get_where("stock_transaction", array('f_id' => $f_id, 'id_res_auto' => res_id()));
    if ($st->num_rows() > 0) {
        return $st->row()->balance_buy - 0;
    } else {
        return 0;
    }
}
