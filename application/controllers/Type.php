<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Type extends CI_Controller {

    public function index() {
        echo json_encode(array("a" => "ss4555"));
    }

    public function load_type() {
        $output = array();
        $a = $this->db->order_by('sort', 'asc')->get_where("supper_group", array('id_res_auto' => res_id()))->result();
        foreach ($a as $value) {
            if($value->zone==''){
                $value->zone = array();
            }else{
                $value->zone = unserialize($value->zone);
            }
            $output[] = array('name' => get_name_show($value->title), 'id' => $value->sg_id - 0, 'sort' => $value->sort - 0, 'zone'=>$value->zone,'pic_id' => $value->pic_id - 0, 'status' => $value->status - 0, 'show_cus' => $value->show_cus - 0);
        }
        echo json_encode($output);
    }

    public function save_type() {
        $a = json_decode($this->input->post('data'));
        //pre($a);
        echo $this->db->update("supper_group", array("title" => mullti_lang_todb($a->name), 'sort' => $a->sort, 'pic_id' => $a->pic_id, 'status' => $a->status, 'zone'=> serialize($a->zone),'show_cus' => $a->show_cus), array('sg_id' => $a->id, 'id_res_auto' => res_id()));
    }

    function get_sort() {
        $a = $this->db->select_max("sort")->get_where("supper_group", array('id_res_auto' => res_id()));
        if ($a->num_rows() > 0) {
            return $a->row()->sort + 1;
        } else {
            return 1;
        }
    }

    public function add_type() {
        $aa = json_decode($this->input->post('data'));
        echo $this->db->insert("supper_group", array('title' => mullti_lang_todb($aa->name), 'sort' => $aa->sort, 'zone'=> serialize($aa->zone),'id_res_auto' => res_id(), 'sort' => $this->get_sort(), 'pic_id' => $aa->pic_id, 'status' => $aa->status, 'show_cus' => $aa->show_cus));
    }

    public function remove_type($id = 0) {
        echo $this->db->delete("supper_group", array('sg_id' => $id, 'id_res_auto' => res_id()));
    }

    public function load_type_show() {
        $output = array();
        $a = $this->db->order_by("sg_id", 'desc')->get_where("supper_group", array('id_res_auto' => res_id()))->result();
        foreach ($a as $value) {
            $output[] = array('name' => array_lang($value->name), 'id' => $value->sg_id - 0, 'sort' => $value->sort - 0, 'show_cus' => $value->show_cus - 0, 'status' => $value->status - 0);
        }
        echo json_encode($output);
    }

}
