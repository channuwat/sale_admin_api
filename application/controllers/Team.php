<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Team extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    public function getChildTeam()
    {
        $p = _post();
        $child = $this->db
        ->select('*')
        ->get_where('sale_saler',array('parent' => $p->user))
        ->result();
        echo json_encode($child);
    }

    public function getGrandChildTeam()
    {
        $p = _post();
        $child_id = $this->db
        ->select('s_id')
        ->get_where('sale_saler',array('parent' => $p->user))
        ->result();
        $childArray = array();
        if(count($child_id) != 0)
        {
            foreach($child_id as $index => $val){  
                array_push($childArray , $val->s_id); // แตก obj เหลือแค่ value เเล้วไปเก็บเป็น array
            }
        }
        else
        {
            // ใช้ 99999 เพื่อให้มัน qry ใน where in ได้ถ้ากำนดเป็น 0 มันจะไปดึงพวก parent id ที่เป็นแม่ออกมา หรือ -1 มันจะ error
            // case นี้เราต้องการ id ตัวลูก
            $childArray = [99999]; 
        }
        $grandChild = $this->db
        ->select('*')
        ->from('sale_saler')
        ->where_in('parent',$childArray)
        ->get()->result();
        echo json_encode($grandChild);
    }

    // new app//

    public function getTeam($user)
    {
        $grandChild = array();
        $team = $this->db
        ->select('*')
        ->get_where('sale_saler',array('parent' => $user))
        ->result();
        echo json_encode($team);
    }

    public function getCountTeam($user)
    {
        // หาจำนวน team head
        $total = 0;
        $team = $this->db
        ->select('COUNT(s_id) as count_main,s_id')
        ->get_where('sale_saler',array('parent' => $user))
        ->result();
        foreach($team as $team_k => $team_v){
            $total = $total + $team_v->count_main;
        }
        
        // หาจำนวน team sub
        $team = $this->db
        ->select('s_id')
        ->get_where('sale_saler',array('parent' => $user))
        ->result();
        foreach($team as $team_k => $team_v){
            $grandChild = $this->db
            ->select('COUNT(s_id) as count_sub')
            ->from('sale_saler')
            ->where('parent',$team_v->s_id)
            ->get()->result();
            $total = $total + $grandChild[0]->count_sub;
        }

        // เอาจำนวนทีม Head มาบวกกันกับทีม Sub
        $teamArray = array('title'=>'ทีม','detail'=>$total,'unit'=>'คน');
        echo json_encode($teamArray);
    }
    
    public function getDetailTeam($s_id)
    {
        $show = $this->db->select('s_id,s_code,name,email,phone,picture,address,nick_name,s_position')
        ->get_where('sale_saler',array('s_id' => $s_id))
        ->row();
        echo json_encode($show);
    }
}
