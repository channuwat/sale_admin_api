<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require __DIR__ . '/../autoload.php';

use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
//use Mike42\Escpos\PrintConnectors\FilePrintConnector;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;

class Test extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function get_not_print_test() {
        $id_res_auto = res_id();
        $res = $this->db->select("set_printorder")->get_where("restaurant", array('id_res_auto' => $id_res_auto));
        if ($res->num_rows() > 0) {
            if ($res->row()->set_printorder != "0000-00-00 00:00:00") {
                $this->db->where_not_in("d.status", array(4, 3, 2));
                $date = date('Y-m-d H:i:s', strtotime("-2 day", time()));
                $this->db->where("d.up_date >", $date);
                $data = $this->db->order_by('k.ok_id', "asc")
                        ->select("d.od_id,d.order_id,d.comment,d.toppings,d.count,d.status,k.ok_id,f.name,f.cooks,d.status,d.up_by,o.sum_table,o.title,o.comment as comment_table,f.pic_id,d.details,k.date_time,k.is_print")->join("order_data o", "o.order_id=d.order_id")->join("foods f", "f.f_id=d.f_id")
                        ->join("order_kitchen k", 'k.ok_id=d.ok_id')
                        ->get_where("order_detail d", array('d.id_res_auto' => $id_res_auto, 'o.status' => 2, 'k.is_print' => 0, 'k.date_time >='=> $res->row()->set_printorder));
                
                
                $out = array(
                    'flag' => false,
                    'ok_id' => 0,
                );
                if ($data->num_rows() > 0) {
                    $out['flag'] = true;
                    $out['ok_id'] = $data->row()->ok_id;
                }
                echo json_encode($out);
            }
        }
    }

    public function get_print_kitchen($ok_id = 0, $paper_width = '1') {
//        echo $paper_width . "-" . $ok_id;
        $id_res_auto = res_id();
        $out = array();
        $this->db->update("order_kitchen", array('is_print' => 1), array('ok_id' => $ok_id, 'id_res_auto' => res_id()));
        $this->db->where_not_in("d.status", array(4, 3, 2));
        $date = date('Y-m-d H:i:s', strtotime("-2 day", time()));
        $this->db->where("d.up_date >", $date);
        $order = $this->db->order_by('k.ok_id', "asc")->select("d.od_id,d.order_id,d.comment,d.toppings,d.count,d.status,k.ok_id,f.name,f.cooks,d.status,d.up_by,o.sum_table,o.title,o.comment as comment_table,f.pic_id,d.details,k.date_time")->join("order_data o", "o.order_id=d.order_id")->join("foods f", "f.f_id=d.f_id")->join("order_kitchen k", 'k.ok_id=d.ok_id')->get_where("order_detail d", array('d.id_res_auto' => $id_res_auto, 'o.status' => 2, 'k.ok_id' => $ok_id))->result();

        foreach ($order as $key => $value) {

            $value->cooks = unserialize($value->cooks);
            $value->details = unserialize($value->details);
            if (!$value->details) {
                $value->details = array();
            }
            if ($value->toppings != '') {
                $value->toppings = unserialize($value->toppings);
            } else {
                $value->toppings = array();
            }
            $value->name = array_lang($value->name);
            $value->sum_table = unserialize($value->sum_table);
            foreach ($value->toppings as $k => $to) {
                $tps = $this->db->select('title')->get_where("topping_sub", array('tps_id' => $to['tps_id'], 'id_res_auto' => $id_res_auto));
                if ($tps->num_rows() > 0) {
                    $value->toppings[$k]['name'] = array_lang($tps->row()->title);
                }
            }
            array_push($out, $value);
        }
        $ok_id_temp = array();
        $out_final = array();
        foreach ($out as $key => $value) {
            $flag = true;
            foreach ($ok_id_temp as $key_od => $od) {
                if ($od - 0 == $value->ok_id - 0) {
                    $flag = false;
                    array_push($out_final[$key_od]['data'], $value);
                }
            }
            if ($flag) {
                array_push($ok_id_temp, $value->ok_id);
                $title = $value->title;
                if (count($value->sum_table) != 0) {
                    $title = "โต๊ะ ";
                    foreach ($value->sum_table as $k => $table) {
                        $tab = $this->db->get_where('table_table', array('table_id_auto' => $table, 'id_res_auto' => $id_res_auto));
                        if ($tab->num_rows() > 0) {
                            $tt = $tab->row()->name;
                            if ($k == 0) {
                                $title .= $tt;
                            } else {
                                $title . ", " . $tt;
                            }
                        }
                    }
                }
                array_push($out_final, array('ok_id' => $value->ok_id, 'date' => $value->date_time, 'order_id' => $value->order_id, 'title' => $title, 'sum_table' => $value->sum_table, 'comment_table' => $value->comment_table, 'data' => array($value)));
            }
        }
//        pre($out_final);
//        exit();
        if ($paper_width == '1') {
            ?>
            <style>       
                @page{
                    margin-left: 0px;
                    margin-right: 0px;
                    margin-top: 0px;
                    margin-bottom: 50px;
                    font-size:10px;
                }
                table{
                    margin-left: 5px;
                }
                *{
                    font-size: 16px;

                }
            </style>
        <?php } else if ($paper_width == '2') { ?>
            <style>
                @page{
                    margin-left: -10px;
                    margin-right: 0px;
                    margin-top: 0px;
                    margin-bottom: 100px;
                    font-size:10px;
                }
                table{
                    margin-left: 0px;
                }
                *{
                    font-size: 12px;
                }         
            </style>             
        <?php } ?>
        <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
        <style>
            *{
                font-family: 'Kanit', sans-serif;
            }
        </style>
        <table border="0" style="width: <?php
        if ($paper_width == '1') {
            echo '270px';
        } else if ($paper_width == '2') {
            echo '180px';
        }
        ?>;">
            <thead >
                <tr>
                    <th colspan="2" class="text-center">
                        <?php echo $out_final[0]['title']; ?>
                    </th>
                </tr>
                <tr>
                    <th colspan="2" class="text-center" style="border-bottom: solid #000 1px;">
                        <?php echo l('date') . " " . $out_final[0]['date']; ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($out_final[0]['data'] as $value) {
                    ?>
                    <tr>
                        <td colspan="2"><?php
                            echo "<b>[" . $value->count . "] " . $value->name . "</b>";
                            if (count($value->details) > 0) {
                                foreach ($value->details as $de) {
                                    echo "<br>";
                                    echo " - " . array_lang_byarray($de->name) . " : ";
                                    $flag_comm = false;
                                    foreach ($de->sub as $sub) {
                                        if ($sub->selected - 0 == 1) {
                                            if ($flag_comm) {
                                                echo ", ";
                                            }
                                            echo array_lang_byarray($sub->name);
                                            $flag_comm = true;
                                        }
                                    }
                                }
                            }
                            if (count($value->toppings) > 0) {
                                foreach ($value->toppings as $top) {
                                    echo '<br> - [' . $top['count'] . "] " . $top['name'];
                                }
                            }
                            if ($value->comment != '') {
                                echo "<br> - " . $value->comment;
                            }
                            ?></td>

                    </tr>
                <?php } ?>
            </tbody>
        </table>
        <br>
        <br>
        <br>
        <br>
        .
        <?php
    }

    public function print_food_bill($order_id, $paper_width) {


        $order = $this->db->get_where("order_data", array('order_id' => $order_id));
        if ($order->num_rows() > 0) {
            $data = $order->row();
            $res = $this->db->select("notice,name,address,logo_url,currency_shot")->get_where("restaurant", array('id_res_auto' => $data->id_res_auto));
            if ($res->num_rows() > 0) {
                $bill = $res->row();
                $bill->name = array_lang($bill->name);
                $bill->address = array_lang($bill->address);
                $bill->bill = unserialize($bill->notice);
                $bill->bill->footer = array_lang($bill->bill->footer);
                $table = unserialize($data->sum_table);
                $buffet_temp = unserialize($data->people);
                $buffet = array();
                $sum = 0;
                foreach ($buffet_temp as $bu) {
                    $name = $this->db->select("name")->get_where("buffet", array('buffet_id' => $bu['buffet_id'], 'id_res_auto' => $data->id_res_auto));
                    if ($name->num_rows() > 0 && $bu['number'] - 0 != 0) {
                        $sum += $bu['price'] * $bu['number'];
                        array_push($buffet, array('name' => array_lang($name->row()->name), 'price' => $bu['price'], 'number' => $bu['number']));
                    }
                }
                $table_text = $data->title;
                $t = array();
                $price_hour = array();
                $price_start = 0;
                $is_table = false;
                if (count($table) > 0) {
                    $is_table = true;
                    foreach ($table as $t_v) {
                        array_push($t, $t_v);
                    }
                    $table_temp = $this->db->select("name,price_per_hour,price_start")->where_in("table_id_auto", $t)->get("table_table")->result();
                    foreach ($table_temp as $key => $t_temp) {
                        if ($key == 0) {
                            $table_text = $t_temp->name;
                            if ($t_temp->price_per_hour - 0 > 0) {
                                if ($data->hour_number != 0) {
                                    array_push($price_hour, array('text' => l("price_house"), 'number' => $data->hour_number, 'price' => $t_temp->price_per_hour));
                                } else {
                                    $time = time() - strtotime($data->time_start);
                                    array_push($price_hour, array('text' => l("price_house"), 'number' => floor($time / 60), 'price' => $t_temp->price_per_hour));
                                }
                            }
                        } else {
                            $table_text .= ", " . $t_temp->name;
                        }
                        $price_start += $t_temp->price_start - 0;
                    }
                } else {
//                    $table_text = $data->title;
                }
//                pre($price_hour);
                $order_detail = $this->db->select("d.price,f.name,d.count,d.details,d.toppings,f.f_id")->join("foods f", "f.f_id=d.f_id")->get_where("order_detail d", array('d.order_id' => $order_id, 'd.status!=' => -1, 'd.del' => 0, 'd.status !=' => 4, 'd.is_buffet' => 0))->result();

                if ($paper_width == '1') {
                    ?>
                    <style>       
                        @page{
                            margin-left: 0px;
                            margin-right: 0px;
                            margin-top: 0px;
                            margin-bottom: 50px;
                            font-size:10px;
                        }
                        table{
                            margin-left: 5px;
                        }
                        *{
                            font-size: 16px;

                        }
                    </style>
                <?php } else if ($paper_width == '2') { ?>
                    <style>
                        @page{
                            margin-left: -15px;
                            margin-right: 0px;
                            margin-top: 0px;
                            margin-bottom: 100px;
                            font-size:10px;
                        }
                        table{
                            margin-left: 0px;
                        }
                        *{
                            font-size: 12px;

                        }     
                    </style>     

                <?php } ?>
                <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
                <style>
                    *{
                        font-family: 'Kanit', sans-serif;
                    }
                </style>
                <table border="0" style="width: <?php
                if ($paper_width == '1') {
                    echo '270px';
                } else if ($paper_width == '2') {
                    echo '170px';
                }
                ?>;">
                    <thead>
                        <?php
                        if ($bill->bill->show_logo - 0 == 1) {
                            ?>
                            <tr>
                                <th colspan="3" class="text-center">
                                    <?php if ($paper_width == '1') { ?>
                                        <img src="<?php
                                        echo get_server();
                                        echo $bill->logo_url;
                                        ?>" style="width: 140px;">
                                         <?php } else if ($paper_width == '2') { ?>
                                        <img src="<?php
                                        echo get_server();
                                        echo $bill->logo_url;
                                        ?>" style="width: 100px;">

                                    <?php } ?>
                                </th>
                            </tr>
                            <?php
                        }
                        if ($bill->bill->show_name - 0 == 1) {
                            ?>
                            <tr>
                                <th colspan="3" class="text-center">
                                    <?php echo $bill->name; ?>
                                </th>
                            </tr>
                        <?php } ?>
                        <tr>
                            <th colspan="3" style="text-align: center;">
                                <b> <?php echo l("food_bill"); ?>  </b>
                            </th>
                        </tr>
                        <?php
                        if ($bill->bill->show_add_top - 0 == 1) {
                            ?>
                            <tr>
                                <th colspan="3" class="text-center" style="font-weight: 100;">
                                    <?php echo $bill->address; ?>
                                </th>
                            </tr>
                            <?php
                        }
                        ?>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="3" > Order ID : <?php echo $data->order_id_res; ?></td>
                        </tr>
                        <tr>
                            <td colspan="3" > <?php echo l("date"); ?> :<?php echo date("d/m/") . (date("Y") + 543) . " " . date("H:i:s"); ?></td>
                        </tr>
                        <?php if ($is_table) { ?>
                            <tr>
                                <td colspan="3" > <?php echo l("table"); ?> : <?php echo $table_text; ?></td>
                            </tr>
                        <?php } else { ?>
                            <tr>
                                <td colspan="3" > <?php echo l("cus"); ?> : <?php echo $table_text; ?></td>
                            </tr>
                        <?php } ?>
                        <tr style="border: 1px solid #000;">
                            <td style="border-top: solid #000 1px; border-bottom: solid #000 1px;"><?php echo l("menu_goods"); ?></td>
                            <td style="border-top: solid #000 1px; border-bottom: solid #000 1px; text-align: center;"><?php echo l("number"); ?></td>
                            <td style="text-align: right;border-top: solid #000 1px; border-bottom: solid #000 1px;"><?php echo l("price"); ?></td>
                        </tr>
                        <?php
                        if ($price_start != 0) {
                            $sum += $price_start;
                            ?>
                            <tr >
                                <td><?php echo l('open_table'); ?></td>
                                <td></td>
                                <td style="text-align: right;"><?php echo number_format($price_start, 2); ?></td>
                            </tr>
                            <?php
                        }
                        foreach ($price_hour as $value) {
                            $price_hour2 = round(($value['price'] / 60) * $value['number']);
                            $sum += $price_hour2;
                            ?>
                            <tr >
                                <td><?php echo $value['text']; ?></td>
                                <td style="text-align: center;"><?php echo get_time_text($value['number']); ?></td>
                                <td style="text-align: right;"><?php echo number_format($price_hour2, 2); ?></td>
                            </tr>
                            <?php
                        }
                        foreach ($buffet as $value) {
                            ?>
                            <tr >
                                <td><?php echo l("buffet"); ?> <?php echo $value['name']; ?></td>
                                <td style="text-align: center;"><?php echo $value['number']; ?></td>
                                <td style="text-align: right;"><?php echo number_format($value['price'] * $value['number'], 2); ?></td>
                            </tr>
                            <?php
                        }
                        $out1 = array();

                        foreach ($order_detail as $key => $value) {
                            $details = unserialize($value->details);
                            if (!$details) {
                                $details = array();
                            }
                            if ($value->toppings != '') {
                                $toppings = unserialize($value->toppings);
                            } else {
                                $toppings = array();
                            }
                            $value->count -= 0;
                            $is_details = false;
                            $new_price = 0;
                            $new_price += $value->price;
                            $detail_new = array();

                            foreach ($details as $e) {
                                $flag = false;
                                foreach ($e->sub as $value5) {
                                    if ($value5->selected - 0 == 1) {
                                        $flag = true;
                                    }
                                }
                                if ($flag) {
                                    $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                                    foreach ($e->sub as $v) {
                                        if ($v->selected - 0 == 1) {
                                            if ($flag) {
                                                $flag = false;
                                                $tt['text'] .= array_lang_byarray($v->name);
                                            } else {
                                                $tt['text'] .= ", " . array_lang_byarray($v->name);
                                            }
                                            $tt['price'] += $v->price;
                                        }
                                    }
                                    if ($tt['price'] - 0 != 0) {
                                        $new_price += $tt['price'];
                                        array_push($detail_new, $tt);
                                    }
                                }
                            }
                            foreach ($toppings as $values) {
                                $new_price += $values['price'] * $values['count'];
                            }
                            $value->sum_price = $new_price;
                            $flag = true;
                            foreach ($out1 as $k => $o) {
                                if ($o->f_id == $value->f_id && $o->sum_price == $value->sum_price) {
                                    $flag = false;
                                    $out1[$k]->count += $value->count - 0;
                                }
                            }
                            if ($flag) {
                                array_push($out1, $value);
                            }
                        }

                        foreach ($out1 as $key => $value) {
                            $details = unserialize($value->details);
                            if (!$details) {
                                $details = array();
                            }
                            if ($value->toppings != '') {
                                $toppings = unserialize($value->toppings);
                            } else {
                                $toppings = array();
                            }
                            $is_details = false;
                            $new_price = 0;
                            $new_price += $value->price;
                            $detail_new = array();

                            foreach ($details as $e) {
                                $flag = false;
                                foreach ($e->sub as $value5) {
                                    if ($value5->selected - 0 == 1) {
                                        $flag = true;
                                    }
                                }
                                if ($flag) {
                                    $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                                    foreach ($e->sub as $v) {
                                        if ($v->selected - 0 == 1) {
                                            if ($flag) {
                                                $flag = false;
                                                $tt['text'] .= array_lang_byarray($v->name);
                                            } else {
                                                $tt['text'] .= ", " . array_lang_byarray($v->name);
                                            }
                                            $tt['price'] += $v->price;
                                        }
                                    }
                                    if ($tt['price'] - 0 != 0) {
                                        $new_price += $tt['price'];
                                        array_push($detail_new, $tt);
                                    }
                                }
                            }
                            foreach ($toppings as $values) {
                                $new_price += $values['price'] * $values['count'];
                            }
                            $sum_price = $value->count * $new_price;
                            $sum += $sum_price;
                            if ($key + 1 == count($order_detail)) {
                                ?>
                                <tr>
                                    <td style="border-bottom: solid 1px #000;"><?php
                                        echo show_text_by_lang($value->name);
                                        if (count($detail_new)) {
                                            echo " (" . $value->price . ")";
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($detail_new as $de) {
                                                    echo "  " . $de['text'] . " (" . show_pre_int($de['price']) . ")<br>";
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        if (count($toppings) > 0) {
                                            if (!count($detail_new)) {
                                                echo " (" . $value->price . ")";
                                            }
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($toppings as $value2) {
                                                    $tps = $this->db->select("title")->get_where("topping_sub", array('id_res_auto' => res_id(), 'tps_id' => $value2['tps_id']));
                                                    if ($tps->num_rows() > 0) {
                                                        echo " [" . $value2['count'] . "] " . array_lang($tps->row()->title) . "(" . show_pre_int($value2['price'] * $value2['count']) . ")<br>";
                                                    }
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        ?></td>
                                    <td style="text-align: center;border-bottom: solid 1px #000; vertical-align: top;"><?php echo $value->count; ?></td>
                                    <td style="text-align: right;border-bottom: solid 1px #000; vertical-align: top;"><?php echo number_format($sum_price, 2); ?></td>
                                </tr>
                                <?php
                            } else {
                                ?>
                                <tr>
                                    <td ><?php
                                        echo show_text_by_lang($value->name);
                                        if (count($detail_new)) {
                                            echo " (" . $value->price . ")";
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($detail_new as $de) {
                                                    echo "  " . $de['text'] . " (" . show_pre_int($de['price']) . ")<br>";
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        if (count($toppings) > 0) {
                                            if (!count($detail_new)) {
                                                echo " (" . $value->price . ")";
                                            }
                                            ?>
                                            <div style="padding-left: 10px;">
                                                <?php
                                                foreach ($toppings as $value2) {
                                                    $tps = $this->db->select("title")->get_where("topping_sub", array('id_res_auto' => res_id(), 'tps_id' => $value2['tps_id']));
                                                    if ($tps->num_rows() > 0) {
                                                        echo "  [" . $value2['count'] . "] " . array_lang($tps->row()->title) . "(" . show_pre_int($value2['price'] * $value2['count']) . ")<br>";
                                                    }
                                                }
                                                ?>                                          
                                            </div>
                                            <?php
                                        }
                                        ?></td>
                                    <td style="text-align: center; vertical-align: top;"><?php echo $value->count; ?></td>
                                    <td style="text-align: right; vertical-align: top;"><?php echo number_format($sum_price, 2); ?></td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr>
                            <td colspan="2" style="padding-left: 30px; "><?php echo l("price"); ?> </td>
                            <td style="text-align: right;"><?php echo number_format($sum, 2); ?></td>
                        </tr>
                        <?php
                        if ($data->fine - 0 != 0) {
                            ?>
                            <tr>
                                <td colspan="2" style="padding-left: 30px; "><?php echo l('fine'); ?> </td>
                                <td style="text-align: right;"><?php echo number_format($data->fine, 2); ?></td>
                            </tr>
                            <?php
                            $sum += $data->fine;
                        }
                        if ($data->discounts != '') {
                            foreach (unserialize($data->discounts) as $dis) {
                                ?>
                                <tr>
                                    <td colspan="2" style="padding-left: 30px; "><?php echo $dis->title; ?> </td>
                                    <td style="text-align: right;"><?php echo $dis->op . " " . number_format($dis->money, 2); ?></td>
                                </tr>
                                <?php
                                if ($dis->op == '-') {
                                    $sum -= $dis->money;
                                } else {
                                    $sum += +$dis->money;
                                }
                            }
                        }
                        $charge = $this->Pos_model->sum_charge_bill($sum, $order_id, res_id());
                        foreach ($charge as $value) {
                            if ($value['type'] == '-') {
                                $sum -= $value['value'];
                            } else {
                                $sum += $value['value'];
                            }
                            ?>
                            <tr>
                                <td colspan="2" style="padding-left: 30px;"><?php
                                    echo $value['name'] . " (" . $value['type'] . $value['number'] . " ";
                                    echo $value['per'] == 'persen' ? '%' : get_currency();
                                    echo ")";
                                    ?></td>
                                <td style="text-align: right;"><?php echo $value['type'] . number_format($value['value'], 2); ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td style="padding-left: 30px;"><?php echo l("net_price"); ?></td>
                            <td colspan="2" style="text-align: right;font-weight: bold;"><?php echo number_format($sum, 2) . " " . array_lang($bill->currency_shot); ?>
                            </td>
                        </tr>
                        <?php
                        $cur = $this->db->get_where("currency", array('id_res_auto' => res_id()))->result();
                        foreach ($cur as $vv) {
                            ?>
                            <tr 
                                >
                                <td colspan="1"  style="font-weight: 100;padding-left: 40px; text-align: left;" >
                                    <?php
                                    echo array_lang($vv->title);
                                    ?>
                                </td>
                                <td colspan="2"  style="font-weight: 100;text-align: right;" >
                                    <?php
                                    echo number_format($sum * $vv->mul, 2) . " " . $vv->unit;
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                        if ($bill->bill->show_add_bottom - 0 == 1) {
                            ?>
                            <tr>
                                <td colspan="3"  style="font-weight: 100;text-align: center;">
                                    <?php echo $bill->address; ?>
                                </td>
                            </tr>
                            <?php
                        }
                        if ($bill->bill->footer != '') {
                            ?>
                            <tr >
                                <td colspan="3"  style="font-weight: 100;text-align: center;" >
                                    <?php echo nl2br($bill->bill->footer); ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <br>
                <br>
                <br>
                <br>
                .
                <?php
            } else {
                echo l("cannot_find_bill");
            }
        } else {
            echo l("cannot_find_bill");
        }
//        $this->db->close();
    }

    public function test() {
        try {
//            echo 'sxxx';exit();
            // Enter the share name for your USB printer here
//            $connector = null;
//            $connector = new WindowsPrintConnector("XP-80C_share");
            $connector = new NetworkPrintConnector("192.168.1.230", 9100);
//  $connector = new FilePrintConnector("/dev/usb/lp0");
            /* Print a "Hello world" receipt" */
            $printer = new Printer($connector);
            $printer->text("Hello World!\n");
            $printer->text("Hello World!\n");

            $printer->text("Hello World!\n\n\n\n\n");
            $printer->cut();

            /* Close printer */
            $printer->close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e->getMessage() . "\n";
        }
    }

    function resizePng($im, $dst_width, $dst_height) {
        $width = imagesx($im);
        $height = imagesy($im);

        $newImg = imagecreatetruecolor($dst_width, $dst_height);

        imagealphablending($newImg, true);
        imagesavealpha($newImg, true);
        $transparent = imagecolorallocatealpha($newImg, 255, 255, 255, 20);
        imagefilledrectangle($newImg, 0, 0, $width, $height, $transparent);
        imagecopyresampled($newImg, $im, 0, 0, 0, 0, $dst_width, $dst_height, $width, $height);

        return $newImg;
    }

    public function get_x_ritht($im, $text) {
        $font = '../fonts/THSarabun Bold.ttf';
        $dimensions = imagettfbbox(25, 0, $font, $text);
        $textWidth = abs($dimensions[4] - $dimensions[0]);
        $x = imagesx($im) - $textWidth;
        if ($text == "00.00") {
            return $x - 4;
        } else {
            return $x;
        }
    }

    public function get_x_center($im, $text, $size) {
        $font = '../fonts/THSarabun Bold.ttf';
        $dimensions = imagettfbbox($size, 0, $font, $text);
        $textWidth = abs($dimensions[4] - $dimensions[0]);
        $x = imagesx($im) / 2 - ($textWidth / 2);
        return $x;
    }

    public function cre_pic($order_id = 0) {
        $order = $this->db->get_where("order_data", array('order_id' => $order_id));
        if ($order->num_rows() > 0) {
            $data = $order->row();
            $res = $this->db->select("bill,name,address,logo_url,img_bg_eslip")->get_where("restaurant", array('id_res_auto' => $data->id_res_auto));
            if ($res->num_rows() > 0) {
                $bill = $res->row();
                $bill->name = array_lang($bill->name);
                $bill->address = array_lang($bill->address);
                $bill->bill = unserialize($bill->bill);
                $bill->bill->footer = array_lang($bill->bill->footer);
                $id_table = false;

                $table = unserialize($data->sum_table);
                $buffet_temp = unserialize($data->people);
                $buffet = array();
                $sum = 0;
                foreach ($buffet_temp as $bu) {
                    $name = $this->db->select("name")->get_where("buffet", array('buffet_id' => $bu['buffet_id'], 'id_res_auto' => $data->id_res_auto));
                    if ($name->num_rows() > 0 && $bu['number'] - 0 != 0) {
                        $sum += $bu['price'] * $bu['number'];
                        array_push($buffet, array('name' => array_lang($name->row()->name), 'price' => $bu['price'], 'number' => $bu['number']));
                    }
                }
                $table_text = $data->title;
                $price_hour = array();
                $price_start = 0;
                $t = array();
                $is_table = false;
                if (count($table) > 0) {
                    $is_table = true;
                    foreach ($table as $t_v) {
                        array_push($t, $t_v);
                    }
                    $table_temp = $this->db->select("name,price_per_hour,price_start")->where_in("table_id_auto", $t)->get("table_table")->result();
                    foreach ($table_temp as $key => $t_temp) {
                        if ($key == 0) {
                            $table_text = $t_temp->name;
                            if ($t_temp->price_per_hour - 0 > 0) {
                                array_push($price_hour, array('text' => l("price_house"), 'number' => $data->hour_number, 'price' => $t_temp->price_per_hour));
                            }
                        } else {
                            $table_text .= ", " . $t_temp->name;
                        }
                        $price_start += $t_temp->price_start - 0;
                    }
                }

                $order_detail = $this->db->select("d.price,f.name,d.count,d.toppings,d.details,f.f_id")->join("foods f", "f.f_id=d.f_id")->get_where("order_detail d", array('d.order_id' => $order_id, 'd.status!=' => -1, 'd.del' => 0, 'd.status !=' => 4, 'd.is_buffet' => 0))->result();
                header('Content-type: image/png');
                ////////////////////////////
                $y = 40;
                if ($bill->bill->show_name - 0 == 1) {
                    $y += 30;
                }
                $h = 25;
                if ($bill->bill->show_add_top - 0 == 1) {
                    $y += $h;
                }
                $y += $h;
                $y += $h;
                if ($is_table) {
                    $y += $h;
                } else {
                    $y += $h;
                }
                if (nick_name($data->payment_by) != '') {
                    $y += $h;
                }
                $y += $h;
                if ($price_start != 0) {
                    $y += $h;
                }

                foreach ($price_hour as $value) {

                    $y += $h;
                }
                foreach ($buffet as $value) {
                    $y += $h;
                }
                $out1 = array();
                foreach ($order_detail as $key => $value) {
                    $details = unserialize($value->details);
                    if (!$details) {
                        $details = array();
                    }
                    if ($value->toppings != '') {
                        $toppings = unserialize($value->toppings);
                    } else {
                        $toppings = array();
                    }
                    $value->count -= 0;
                    $is_details = false;
                    $new_price = 0;
                    $new_price += $value->price;
                    $detail_new = array();
                    foreach ($details as $e) {
                        $flag = false;
                        foreach ($e->sub as $value5) {
                            if ($value5->selected - 0 == 1) {
                                $flag = true;
                            }
                        }
                        if ($flag) {
                            $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                            foreach ($e->sub as $v) {
                                if ($v->selected - 0 == 1) {
                                    if ($flag) {
                                        $flag = false;
                                        $tt['text'] .= array_lang_byarray($v->name);
                                    } else {
                                        $tt['text'] .= ", " . array_lang_byarray($v->name);
                                    }
                                    $tt['price'] += $v->price;
                                }
                            }
                            if ($tt['price'] - 0 != 0) {
                                $new_price += $tt['price'];
                                array_push($detail_new, $tt);
                            }
                        }
                    }
                    foreach ($toppings as $values) {
                        $new_price += $values['price'] * $values['count'];
                    }
                    $value->sum_price = $new_price;
                    $flag = true;
                    foreach ($out1 as $k => $o) {
                        if ($o->f_id == $value->f_id && $o->sum_price == $value->sum_price) {
                            $flag = false;
                            $out1[$k]->count += $value->count - 0;
                        }
                    }
                    if ($flag) {
                        array_push($out1, $value);
                    }
                }
                foreach ($out1 as $key => $value) {
                    $details = unserialize($value->details);
                    if (!$details) {
                        $details = array();
                    }
                    if ($value->toppings != '') {
                        $toppings = unserialize($value->toppings);
                    } else {
                        $toppings = array();
                    }
                    $is_details = false;
                    $new_price = 0;
                    $new_price += $value->price;
                    $detail_new = array();
                    foreach ($details as $e) {
                        $flag = false;
                        foreach ($e->sub as $value5) {
                            if ($value5->selected - 0 == 1) {
                                $flag = true;
                            }
                        }
                        if ($flag) {
                            $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                            foreach ($e->sub as $v) {
                                if ($v->selected - 0 == 1) {
                                    if ($flag) {
                                        $flag = false;
                                        $tt['text'] .= array_lang_byarray($v->name);
                                    } else {
                                        $tt['text'] .= ", " . array_lang_byarray($v->name);
                                    }
                                    $tt['price'] += $v->price;
                                }
                            }
                            if ($tt['price'] - 0 != 0) {
                                $new_price += $tt['price'];
                                array_push($detail_new, $tt);
                            }
                        }
                    }
                    foreach ($toppings as $values) {
                        $new_price += $values['price'] * $values['count'];
                    }
                    $sum_price = $value->count * $new_price;
                    $sum += $sum_price;
                    $y += $h;
                    $text = show_text_by_lang($value->name);
                    if (count($detail_new)) {
                        $text .= " (" . $value->price . ")";
                    }
                    foreach ($detail_new as $de) {
                        $y += $h;
                    }
                    if (count($toppings) > 0) {
                        if (!count($detail_new)) {
                            $y += $h;
                        }
                        foreach ($toppings as $value2) {
                            $tps = $this->db->select("title")->get_where("topping_sub", array('id_res_auto' => $data->id_res_auto, 'tps_id' => $value2['tps_id']));
                            if ($tps->num_rows() > 0) {
                                $y += $h;
                            }
                        }
                    }
                }
                $y += $h;
                if ($data->fine - 0 != 0) {
                    $y += $h;
                }
                if ($data->discounts != '') {
                    foreach (unserialize($data->discounts) as $dis) {
                        $y += $h;
                    }
                }
                $charge = $this->Pos_model->sum_charge_bill($sum, $order_id);
                foreach ($charge as $value) {
                    $y += $h;
                }
                $y += $h;
                $payments = $this->Pos_model->get_payment_bill($data->payments);
                foreach ($payments as $payment) {
                    $y += $h;
                }
                if ($data->change_money - 0 != 0) {
                    $y += $h;
                }
                $y += 10;
                if ($bill->bill->show_add_bottom - 0 == 1) {
                    $y += $h;
                }
                if ($bill->bill->footer != '') {

                    $footer = nl2br($bill->bill->footer);
                    $footers = explode("<br />", $footer);
                    foreach ($footers as $val) {
                        $y += $h;
                    }
                }
                /////////////////////////////
                $im = imagecreatetruecolor(350, $y + 20);
                $white = imagecolorallocate($im, 255, 255, 255);
                $grey = imagecolorallocate($im, 0, 0, 0);
                $black = imagecolorallocate($im, 0, 0, 0);
                imagefilledrectangle($im, 0, 0, 350, $y + 20, $white);
                if ($bill->img_bg_eslip != '') {
                    $src = imagecreatefrompng($bill->img_bg_eslip);
                    imagecopymerge($im, $this->resizePng($src, 220, 220), 60, 50, 0, 0, 220, 220, 10);
                }
                $font = '../fonts/THSarabun Bold.ttf';
                $y = 40;
                if ($bill->bill->show_name - 0 == 1) {
                    imagettftext($im, 30, 0, $this->get_x_center($im, $bill->name, 30), 40, $black, $font, $bill->name);
                    $y += 30;
                }
                imagettftext($im, 22, 0, $this->get_x_center($im, l("receipt"), 22), $y, $black, $font, l("receipt"));
                $h = 25;
                if ($bill->bill->show_add_top - 0 == 1) {
                    $y += $h;
                    imagettftext($im, 20, 0, $this->get_x_center($im, $bill->address, 20), $y, $grey, $font, $bill->address);
                }
                $y += $h;
                imagettftext($im, 20, 0, 10, $y, $grey, $font, "Order ID ");
                imagettftext($im, 20, 0, 100, $y, $grey, $font, ": " . $data->order_id_res);
                $y += $h;
                imagettftext($im, 20, 0, 10, $y, $grey, $font, l("date"));
                imagettftext($im, 20, 0, 100, $y, $grey, $font, ": " . date("d/m/Y H:i:s"));
                if ($is_table) {
                    $y += $h;
                    imagettftext($im, 20, 0, 10, $y, $grey, $font, l("table"));
                    imagettftext($im, 20, 0, 100, $y, $grey, $font, ": " . $table_text);
                } else {
                    $y += $h;
                    imagettftext($im, 20, 0, 10, $y, $grey, $font, l('cus'));
                    imagettftext($im, 20, 0, 100, $y, $grey, $font, ": " . $table_text);
                }
                if (nick_name($data->payment_by) != '') {
                    $y += $h;
                    imagettftext($im, 20, 0, 10, $y, $grey, $font, l("emp"));
                    imagettftext($im, 20, 0, 100, $y, $grey, $font, ": " . nick_name($data->payment_by));
                }
                $y += $h;
                imagettftext($im, 20, 0, 10, $y, $grey, $font, "รายการอาหาร             จำนวน        ราคา");
                imageline($im, 5, $y - 18, 340, $y - 18, $black);
                imageline($im, 5, $y + 5, 340, $y + 5, $black);
                if ($price_start != 0) {
                    $sum += $price_start;
                    $y += $h;
                    imagettftext($im, 20, 0, 10, $y, $grey, $font, l('open_table'));
                    imagettftext($im, 20, 0, 210, $y, $grey, $font, "1");
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($price_start, 2)), $y, $grey, $font, number_format($price_start, 2));
                }

                foreach ($price_hour as $value) {
                    $price_hour2 = round(($value['price'] / 60) * $value['number']);
                    $sum += $price_hour2;
                    $y += $h;
                    imagettftext($im, 20, 0, 10, $y, $grey, $font, $value['text']);
                    imagettftext($im, 20, 0, 210, $y, $grey, $font, get_time_text($value['number']));
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($price_hour2, 2)), $y, $grey, $font, number_format($price_hour2, 2));
                }
                foreach ($buffet as $value) {
                    $y += $h;
                    imagettftext($im, 20, 0, 10, $y, $grey, $font, l("fuffet") . $value['name']);
                    imagettftext($im, 20, 0, 210, $y, $grey, $font, $value['number']);
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($value['price'] * $value['number'], 2)), $y, $grey, $font, number_format($value['price'] * $value['number'], 2));
                }
                $out1 = array();
                foreach ($order_detail as $key => $value) {
                    $details = unserialize($value->details);
                    if (!$details) {
                        $details = array();
                    }
                    if ($value->toppings != '') {
                        $toppings = unserialize($value->toppings);
                    } else {
                        $toppings = array();
                    }
                    $value->count -= 0;
                    $is_details = false;
                    $new_price = 0;
                    $new_price += $value->price;
                    $detail_new = array();
                    foreach ($details as $e) {
                        $flag = false;
                        foreach ($e->sub as $value5) {
                            if ($value5->selected - 0 == 1) {
                                $flag = true;
                            }
                        }
                        if ($flag) {
                            $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                            foreach ($e->sub as $v) {
                                if ($v->selected - 0 == 1) {
                                    if ($flag) {
                                        $flag = false;
                                        $tt['text'] .= array_lang_byarray($v->name);
                                    } else {
                                        $tt['text'] .= ", " . array_lang_byarray($v->name);
                                    }
                                    $tt['price'] += $v->price;
                                }
                            }
                            if ($tt['price'] - 0 != 0) {
                                $new_price += $tt['price'];
                                array_push($detail_new, $tt);
                            }
                        }
                    }
                    foreach ($toppings as $values) {
                        $new_price += $values['price'] * $values['count'];
                    }
                    $value->sum_price = $new_price;
                    $flag = true;
                    foreach ($out1 as $k => $o) {
                        if ($o->f_id == $value->f_id && $o->sum_price == $value->sum_price) {
                            $flag = false;
                            $out1[$k]->count += $value->count - 0;
                        }
                    }
                    if ($flag) {
                        array_push($out1, $value);
                    }
                }
                foreach ($out1 as $key => $value) {
                    $details = unserialize($value->details);
                    if (!$details) {
                        $details = array();
                    }
                    if ($value->toppings != '') {
                        $toppings = unserialize($value->toppings);
                    } else {
                        $toppings = array();
                    }
                    $is_details = false;
                    $new_price = 0;
                    $new_price += $value->price;
                    $detail_new = array();
                    foreach ($details as $e) {
                        $flag = false;
                        foreach ($e->sub as $value5) {
                            if ($value5->selected - 0 == 1) {
                                $flag = true;
                            }
                        }
                        if ($flag) {
                            $tt = array('text' => array_lang_byarray($e->name) . " : ", 'price' => 0);
                            foreach ($e->sub as $v) {
                                if ($v->selected - 0 == 1) {
                                    if ($flag) {
                                        $flag = false;
                                        $tt['text'] .= array_lang_byarray($v->name);
                                    } else {
                                        $tt['text'] .= ", " . array_lang_byarray($v->name);
                                    }
                                    $tt['price'] += $v->price;
                                }
                            }
                            if ($tt['price'] - 0 != 0) {
                                $new_price += $tt['price'];
                                array_push($detail_new, $tt);
                            }
                        }
                    }
                    foreach ($toppings as $values) {
                        $new_price += $values['price'] * $values['count'];
                    }
                    $sum_price = $value->count * $new_price;
                    $sum += $sum_price;
                    $y += $h;
                    $text = show_text_by_lang($value->name);
                    if (count($detail_new)) {
                        $text .= " (" . $value->price . ")";
                    }
                    imagettftext($im, 20, 0, 10, $y, $grey, $font, $text);
                    imagettftext($im, 20, 0, 210, $y, $grey, $font, $value->count);
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($sum_price, 2)), $y, $grey, $font, number_format($sum_price, 2));
                    foreach ($detail_new as $de) {
                        $y += $h;
                        imagettftext($im, 20, 0, 10, $y, $grey, $font, "  " . $de['text'] . " (" . show_pre_int($de['price']) . ")");
                    }
                    if (count($toppings) > 0) {
                        if (!count($detail_new)) {
                            $y += $h;
                            imagettftext($im, 20, 0, 10, $y, $grey, $font, " (" . $value->price . ")");
                        }
                        foreach ($toppings as $value2) {
                            $tps = $this->db->select("title")->get_where("topping_sub", array('id_res_auto' => $data->id_res_auto, 'tps_id' => $value2['tps_id']));
                            if ($tps->num_rows() > 0) {
                                $y += $h;
                                imagettftext($im, 20, 0, 10, $y, $grey, $font, "  [" . $value2['count'] . "] " . array_lang($tps->row()->title) . "(" . show_pre_int($value2['price'] * $value2['count']) . ")");
                            }
                        }
                    }
                }

                $y += $h;
                imageline($im, 5, $y - 18, 340, $y - 18, $black);
                imagettftext($im, 20, 0, 100, $y, $grey, $font, l("price"));
                imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($sum, 2)), $y, $grey, $font, number_format($sum, 2));
                if ($data->fine - 0 != 0) {
                    $y += $h;
                    imagettftext($im, 20, 0, 100, $y, $grey, $font, l('fine'));
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($data->fine, 2)), $y, $grey, $font, number_format($data->fine, 2));
                    $sum += $data->fine;
                }
                if ($data->discounts != '') {
                    foreach (unserialize($data->discounts) as $dis) {
                        $y += $h;
                        imagettftext($im, 20, 0, 100, $y, $grey, $font, $dis->title);
                        imagettftext($im, 20, 0, $this->get_x_ritht($im, $dis->op . " " . number_format($dis->money, 2)), $y, $grey, $font, $dis->op . " " . number_format($dis->money, 2));

                        if ($dis->op == '-') {
                            $sum -= $dis->money;
                        } else {
                            $sum += +$dis->money;
                        }
                    }
                }

                $charge = $this->Pos_model->sum_charge_bill($sum, $order_id);
                foreach ($charge as $value) {
                    if ($value['type'] == '-') {
                        $sum -= $value['value'];
                    } else {
                        $sum += $value['value'];
                    }
                    $y += $h;
                    $textx = $value['name'] . " (" . $value['type'] . $value['number'] . " ";
                    $textx .= $value['per'] == 'persen' ? '%' : l('money_1');
                    $textx .= ")";
//                    $textx = $value['name'] . " (" . $value['type'] . $value['number'] . " " . $value['per'] == 'persen' ? '%' : l('money_1') . ")";
                    imagettftext($im, 20, 0, 100, $y, $grey, $font, $textx);
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, $value['type'] . number_format($value['value'], 2)), $y, $grey, $font, $value['type'] . number_format($value['value'], 2));
                }
                $y += $h;
                imagettftext($im, 20, 0, 100, $y, $grey, $font, l("net_price"));
                imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($sum, 2)), $y, $grey, $font, number_format($sum, 2));
                $payments = $this->Pos_model->get_payment_bill($data->payments);
                foreach ($payments as $payment) {
                    $y += $h;
                    imagettftext($im, 20, 0, 100, $y, $grey, $font, $payment['bank']);
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($payment['amount'], 2)), $y, $grey, $font, number_format($payment['amount'], 2));
                }
                if ($data->change_money - 0 != 0) {
                    $y += $h;
                    imagettftext($im, 20, 0, 100, $y, $grey, $font, l('change'));
                    imagettftext($im, 20, 0, $this->get_x_ritht($im, number_format($data->change_money, 2)), $y, $grey, $font, number_format($data->change_money, 2));
                }
                $y += 10;
                if ($bill->bill->show_add_bottom - 0 == 1) {
                    $y += $h;
                    imagettftext($im, 20, 0, $this->get_x_center($im, $bill->address, 20), $y, $grey, $font, $bill->address);
                }
                if ($bill->bill->footer != '') {
                    $footer = nl2br($bill->bill->footer);
                    $footers = explode("<br />", $footer);
                    foreach ($footers as $key => $val) {
                        if ($key != 1) {
                            $y += $h;
                        }
                        imagettftext($im, 20, 0, $this->get_x_center($im, $val, 20), $y, $grey, $font, $val);
                    }
                }
            }
        }
        imagepng($im);
        imagedestroy($im);
    }

}
