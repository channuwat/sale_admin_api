<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function index() {
        echo form_open("user/login");
        echo form_input("email");
        echo form_password("password");
        echo form_submit();
        echo form_close();
    }

    public function screen_login() {
        header('Access-Control-Allow-Origin: *');
        $a = $this->db->select('a.admin_id,a.name,a.username,a.tel,a.email,p_group,a.id_res_auto,pro_file_url,a.active,r.code,a.nick_name,r.count_sub_group,r.currency_full,r.pay_before')->join("restaurant r", "r.id_res_auto=a.id_res_auto")->get_where("admin a", array('username' => $this->input->get("email"), 'password' => $this->input->get('password')));
        if ($a->num_rows() > 0) {
            $data = $a->row();
            if ($data->active - 0 == 1) {
                $per = $this->db->select("permission,active,is_admin")->get_where("g_permission", array('id' => $data->p_group));
                if ($per->num_rows() > 0) {
                    if ($per->row()->active - 0 == 1) {
                        $user_data = $a->row_array();
                        $user_data['is_admin'] = $per->row()->is_admin;
                        $data->is_admin = $per->row()->is_admin;
                        $this->session->set_userdata($user_data);
                        $permission = unserialize($per->row()->permission);
                        $this->db->update("admin", array('last_login' => date("Y-m-d H:i:s")), array('admin_id' => $data->admin_id));
                        echo json_encode(array('flag' => 1, 'data' => $data, 'permission' => $permission));
                    } else {
                        echo json_encode(array('flag' => 0, "error" => l('per_login')));
                        $permission = array();
                    }
                } else {
                    $permission = array();
                }
            } else {
                echo json_encode(array('flag' => 0, "error" => l('per_login')));
            }
        } else {
            echo json_encode(array('flag' => 0, "error" => l('login_error')));
        }
    }

    public function test() {
        $a = serialize(array("aaa" => 'bbb', 'ccc' => 'ดดดด'));
        echo $a;
        print_r(unserialize($a));
    }

    public function permission() {
        $per = $this->db->select("permission")->get_where("g_permission", array('id' => $this->session->userdata("p_group")));
        if ($per->num_rows() > 0) {
            $permission = unserialize($per->row()->permission);
        } else {
            $permission = array();
        }
        echo json_encode($permission);
    }

//    public function login() {
//        $a = $this->db->select("ul.user_id,ul.user_name,ul.name as username,ul.pic,ul.user_pass,ul.id_res_auto,r.code,r.name,r.lang")->join("restaurant r", "r.id_res_auto=ul.id_res_auto")->get_where("user_login ul", array("user_name" => $this->input->post("email"), 'user_pass' => $this->input->post("password")));
//        if ($a->num_rows() > 0) {
//            $this->session->set_userdata($a->row_array());
//            echo json_encode($a->row());
//        } else {
//            echo json_encode(array("error" => 'user หรือ password ไม่ถูกต้อง'));
//        }
//    }
    
    public function login_kitchen() {
//        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Methods : GET,PUT,POST,DELETE");
        header("Access-Control-Allow-Headers : Content-Type, Authorization");

//          pre($this->input->post());
        $a = $this->db->select('a.admin_id,a.name,a.username,a.tel,a.email,p_group,a.id_res_auto,pro_file_url,a.active,r.code,r.name,a.nick_name,r.noti_bill,r.noti_table')->join("restaurant r", "r.id_res_auto=a.id_res_auto")->get_where("admin a", array('username' => $this->input->post("username"), 'password' => md5($this->input->post('password'))));
        $this->db->last_query();
        if ($a->num_rows() > 0) {
            $data = $a->row();
            $data->noti_bill -= 0;
            $data->noti_table -= 0;
//            $data->pic_emp = $data->pic_emp - 0 == 1;
            $data->name = array_lang($data->name);
            if ($data->active - 0 == 1) {
                $per = $this->db->select("permission,active")->get_where("g_permission", array('id' => $data->p_group));
                if ($per->num_rows() > 0) {
                    if ($per->row()->active - 0 == 1) {
                        $this->session->set_userdata($a->row_array());
                        $permission = unserialize($per->row()->permission);
                        $this->db->update("admin", array('last_login' => date("Y-m-d H:i:s")), array('admin_id' => $data->admin_id));
                        echo json_encode(array('flag' => 1, 'data' => $data, 'permission' => $permission));
                    } else {
                        echo json_encode(array('flag' => 0, "error" => l('per_login')));
                        $permission = array();
                    }
                } else {
                    $permission = array();
                }
            } else {
                echo json_encode(array('flag' => 0, "error" => l('per_login')));
            }
        } else {
            echo json_encode(array('flag' => 0, "error" => l('login_error')));
        }
    }

    public function login() {
        header('Access-Control-Allow-Origin: *');
        $lang = $this->input->post('lang');
        $a = $this->db->select('a.admin_id,a.name,a.username,a.tel,a.email,p_group,a.id_res_auto,pro_file_url,a.active,r.code,a.nick_name,r.count_sub_group,r.currency_full,r.pay_before')->join("restaurant r", "r.id_res_auto=a.id_res_auto")->get_where("admin a", array('username' => $this->input->post("email"), 'password' => $this->input->post('password')));
        if ($a->num_rows() > 0) {
            $data = $a->row();
            $data->pro_file_url = base_url2() . $data->pro_file_url;
            if ($data->active - 0 == 1) {
                $per = $this->db->select("permission,active,is_admin")->get_where("g_permission", array('id' => $data->p_group));
                if ($per->num_rows() > 0) {
                    if ($per->row()->active - 0 == 1) {
                        $user_data = $a->row_array();
                        $user_data['is_admin'] = $per->row()->is_admin;
                        $data->is_admin = $per->row()->is_admin;
                        $this->session->set_userdata($user_data);
                        $permission = unserialize($per->row()->permission);
                        $this->db->update("admin", array('last_login' => date("Y-m-d H:i:s")), array('admin_id' => $data->admin_id));
                        echo json_encode(array('flag' => 1, 'data' => $data, 'permission' => $permission));
                    } else {
                        echo json_encode(array('flag' => 0, "error" => l('per_login')));
                        $permission = array();
                    }
                } else {
                    $permission = array();
                }
            } else {
                echo json_encode(array('flag' => 0, "error" => l('per_login')));
            }
        } else {
            echo json_encode(array('flag' => 0, "error" => l('login_error')));
        }
    }

    public function load_res_data() {
        $res = $this->db->get_where("restaurant", array('id_res_auto' => res_id()))->result();
        foreach ($res as $value) {
            $value->logo_url = base_url2() . $value->logo_url;
        }
        echo json_encode($res);
    }

    public function session_id() {
        echo session_id();
    }

    public function show() {
        $user = get_currency();
        echo array_lang($user['currency_full']);
//        pre(res_arr());
    }

    public function is_login() {
        if ($this->session->userdata('admin_id') == null) {
            echo 0;
        } else {
            $this->db->update("restaurant", array('last_login' => date("Y-m-d H:i:s")), array('id_res_auto' => res_id()));
            echo 1;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        echo 1;
    }

    public function get_token() {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers:Content-Type,Accept');
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        echo json_encode($csrf);
    }

    public function load_users() {
        $a = $this->db->select('admin_id,name,username,password,name,pic,lang_default,nick_name')->get_where("admin", array('id_res_auto' => res_id()));
        echo json_encode($a->result());
    }

}
