<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accounting_documents extends CI_Controller {

    function __construct() {
        parent::__construct();
    //        $this->load->library('Pdf'); //
    //        $this->Pdf->fontpath = 'fonts/'; // Create folder fonts at Codeigniter
    }
    // ----------------------------- Quotation ----------------------------- //
    public function quotation($qt_id = 0 , $qt_code = '') {

        $chk_url = $this->db->select('quotation_id')
        ->get_where('res_quotation_data',array('quotation_id' => $qt_id, 'id_res_code' => $qt_code))->num_rows();

        if($chk_url > 0){
            
            // start setting data //
            $qt_data = $this->getQuotationToPDF($qt_id);
            $qt_data->address = explode(PHP_EOL, $qt_data->address);
            $qt_data->quotation_date = date("d-m-Y",strtotime($qt_data->quotation_date));
            $qt_data->contents = json_decode($qt_data->contents);
            // end setting data //

            require('application/libraries/fpdf.php');
            $pdf = new FPDF();
            $pdf->AddPage();
            //date_default_timezone_set("Asia/Bangkok");
            $company = "บริษัท พุ่มทอง 88 จำกัด";
            $id = "QU6202002";
            $address1 = " 304/1 หมู่ 5 ต. อ่าวนาง อ. เมืองกระบี่ จ. กระบี่ 81180  ";
            $address2 = " 304/1 Moo 5, Tambon Ao Nang Mouang Krabi 81180";
            $tax_id = "0815562002402";
            $date = date("d/m/") . (date('Y') + 543);
            $pdf->Image('https://app.deltafood.co/img/logo.png', 10, 10, 30, 0, 'PNG');
            $price = 12000;
            $pdf->AddFont('THSarabun', '', 'THSarabun.php'); //ธรรมดา
            $pdf->AddFont('THSarabun', 'b', 'THSarabun Bold.php'); //หนา
            $pdf->SetFont('THSarabun', 'b', 22);
            $pdf->Cell(0, 20, '                    บริษัท เดลต้า ซอฟต์ (ประเทศไทย) จำกัด');
            $pdf->SetFillColor(192);
            $pdf->RoundedRect(145, 10, 50, 20, 3, '1234', 'D');
            $pdf->Ln(7);
            $pdf->SetLineWidth(0.3);
            $pdf->SetFont('THSarabun', 'b', 20);
            $pdf->Cell(0, 0, 'ต้นฉบับ             ', 0, 0, 'R');
            $pdf->Ln(7);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(0, 0, ' ใบเสนอราคา                ', 0, 0, 'R');
            $pdf->Ln(-7);
            $pdf->Line(45, 24, 135, 24);
            $pdf->SetFont('THSarabun', 'b', 22);
            $pdf->Cell(0, 20, '                     DELTA SOFT (THAILAND) CO., LTD.');
            $pdf->Ln(30);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(0, 0, '15 ม. 7 ต. หนองกุงใหญ่ อ. กระนวน จ. ขอนแก่น 40170 (สำนักงานใหญ่)');
            $pdf->Ln(7);
            $pdf->Cell(0, 0, '15 Moo 7 Tumbon Hnongkrungyai District Kranuan Provice Khonkaen 40170');
            $pdf->Ln(7);
            $pdf->Cell(0, 0, 'Tel : +66(0)83-236-9566  E-mail : deltasoftth@gmail.com');
            $pdf->Ln(7);
            $pdf->Cell(0, 0, 'เลขประจำตัวผู้เสียภาษี/Tax ID : 0-4005-61003-57-3');
            $pdf->SetFillColor(192);
            $pdf->RoundedRect(10, 75, 185, 38, 3, '1234', 'D');
            $pdf->Ln(12);
            $pdf->Cell(0, 0, ' ชื่อลูกค้า/Customers :  ' . $qt_data->name);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(0, 0, 'เลขที่/No : ' . $qt_data->quotation_queue . '     ', 0, 0, 'R');
            $pdf->Ln(7);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(80, 0, ' ที่อยู่/Address :  ' .$qt_data->address[0]);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(0, 0, 'วันที่/Date : ' . $qt_data->quotation_date . '     ', 0, 0, 'R');
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Ln(7);
            if(isset($qt_data->address[1])){
              $pdf->Cell(0, 0, '                       ' . $qt_data->address[1]);
            }else{
                 $qt_data->address[1] = '';
                $pdf->Cell(0, 0, '                    ' . $qt_data->address[1]);
            }
            $pdf->Ln(7);
            if(isset($qt_data->address[2])){
                $pdf->Cell(0, 0, '                       ' . $qt_data->address[2]);
              }else{
                   $qt_data->address[2] = '';
                  $pdf->Cell(0, 0, '                    ' . $qt_data->address[2]);
              }
            $pdf->Ln(7);

            $tax_id = " ";
            if($tax_id != '-'){
                $tax_id = $qt_data->tax_id;
            }
            $pdf->Cell(0, 0, ' เลขประจำตัวผู้เสียภาษี / Tax ID : ' . $tax_id);
            $pdf->Ln(10);
            $pdf->Cell(25, 7, ' ลำดับที่', 'LT', 0, 'C', 0);
            $pdf->Cell(110, 7, 'รายการ', 'LT', 0, 'C', 0);
            $pdf->Cell(25, 7, 'ลงวันที่', 'LT', 0, 'C', 0);
            $pdf->Cell(25, 7, 'จำนวนเงิน', 'LTR', 1, 'C', 0);
            $pdf->Cell(25, 7, ' No.', 'LB', 0, 'C', 0);
            $pdf->Cell(110, 7, ' Description', 'LB', 0, 'C', 0);
            $pdf->Cell(25, 7, 'Date', 'LB', 0, 'C', 0);
            $pdf->Cell(25, 7, 'Amount', 'LBR', 0, 'C', 0);
            $pdf->Ln();

            $sum = 0;
            for ($i = 0; $i < 5; $i++) {
                if ($i < 1) {
                    if(!empty($qt_data->contents)){
                        foreach($qt_data->contents as $cont_k => $cont_v){
                            $sum = $sum + $cont_v->price;

                            $pdf->Cell(25, 8, $cont_k + 1, 'RL', 0, 'C', 0);
                            $pdf->Cell(110, 8, $cont_v->detail, 'R', 0, 'L', 0);
                            if($cont_k == 0){
                                $pdf->Cell(25, 8, $qt_data->quotation_date, 'R', 0, 'C', 0);
                            }else{
                                $pdf->Cell(25, 8, '', 'R', 0, 'C', 0);
                            }
                            $pdf->Cell(25, 8, number_format($cont_v->price), 'R', 0, 'R', 0);
                            $pdf->Ln();
                        }
                    }
                    
                } else {
                    $pdf->Cell(25, 8, '', 'RL', 0, 'C', 0);
                    $pdf->Cell(110, 8, '', 'R', 0, 'L', 0);
                    $pdf->Cell(25, 8, '', 'R', 0, 'C', 0);
                    $pdf->Cell(25, 8, '', 'R', 0, 'R', 0);
                    $pdf->Ln();
                }
            }
            $pdf->Cell(25, 8, '', 'T', 0, 'C', 0);
            $pdf->Cell(110, 8, '', 'T', 0, 'L', 0);
            $pdf->Cell(25, 8, '', 'T', 0, 'C', 0);
            $pdf->Cell(25, 8, '', 'T', 0, 'R', 0);
            $payment_type = 1;
            $pdf->Ln();
            $pdf->Cell(125, 7, '', 0, 0, 'L', 0);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(35, 8, 'รวมทั้งสิ้น ', 1, 0, 'L');
            $pdf->Cell(25, 8, number_format($sum, 2), 1, 0, 'R');
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Ln(8);
            $pdf->Cell(125, 7, '', 0, 0, 'L', 0);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(35, 8, 'VAT 7% ', 1, 0, 'L');
            $pdf->Cell(25, 8, number_format($sum * 0.07, 2), 1, 0, 'R');
            $pdf->Ln(8);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(125, 7, '  ', 0, 0, 'L', 0);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(35, 8, 'จำนวนเงินที่จ่ายสุทธิ ', 1, 0, 'L');
            $pdf->Cell(25, 8, number_format($sum + ($sum * 0.07), 2), 1, 0, 'R');
            $pdf->Cell(125, 7, '', 0, 0, 'L', 0);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Ln(16);
            $pdf->Cell(125, 7, 'ตัวอักษร    ' . $this->convert(number_format($sum + ($sum * 0.07), 2)), 0, 0, 'L', 1);
            $pdf->Ln(10);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(110, 7, 'วิธีชำระเงิน ธนาคารไทยพาณิชย์  หมายเลขบัญชี 422-020112-1 ', 0, 0, 'L', 0);
            $pdf->Cell(75, 8, 'ในนาม บริษัท เดลต้า ซอฟต์ (ประเทศไทย) จำกัด ', 'TRL', 0, 'C');
            $pdf->Ln(8);
            $pdf->Cell(110, 7, 'สาขาเซ็นทรัลพลาซ่าขอนแก่น  ประเภทบัญชี ออมทรัพย์ ', 0, 0, 'L', 0);

            $pdf->Cell(75, 18, ' ', 'RL', 0, 'C');
            $pdf->SetDash(0.8, 0.8);
            $pdf->Ln(0);
            $pdf->Cell(110, 8, '', 0, 0, 'L', 0);
            $pdf->Cell(75, 18, ' ', 'B', 0, 'C');
            $pdf->Ln(8);
            $pdf->Cell(110, 7, 'ชื่อบัญชี บริษัท เดลต้า ซอฟต์ (ประเทศไทย) จํากัด', 0, 0, 'L', 0);
            $pdf->Ln(10);

            $pdf->SetDash();
            $pdf->Cell(110, 7, '', 0, 0, 'L', 0);


            $pdf->Cell(75, 8, ' ผู้มีอำนาจลงนาม', 'RLB', 0, 'C');

            $pdf->Output();
        }
        else{
            echo "ไม่พบเอกสาร....!";
        }
        
    }

    public function getQuotationToPDF($qt_id)
    {
        $condition = array(
            'quotation_id' => $qt_id,
            'quotation_status' => 1,
        );
        $qt_qry = $this->db
        ->select('*')
        ->get_where('res_quotation_data',$condition)
        ->row();
        return $qt_qry;
    }
    // ----------------------------- Invoice ----------------------------- //
    public function get_invoice($id, $date) {
        $r = $this->db->order_by('rp_id', 'desc')->limit(1)->get_where('res_payment', array('rp_id <' => $id, 'status' => 'confirm', 'cre_date >' => date("Y-m", strtotime($date)) . "-01"));
        echo $this->db->last_query();
        if ($r->num_rows() > 0) {
            return $r->row()->invoice_id + 1;
        } else {
            return 1;
        }
    }

    public function create_id() {
        $res = $this->db->get_where("res_payment", array('status' => 'confirm'))->result();
        foreach ($res as $value) {
            $value->invoice = $this->get_invoice($value->rp_id, $value->cre_date);
            $this->db->update("res_payment", array('invoice_id' => $value->invoice), array('rp_id' => $value->rp_id));
        }
        pre($res);
    }

    public function index() {
        $res = $this->db->select('r.name,r.address,rp.*')->join('restaurant r', 'r.id_res_auto=rp.id_res_auto')->get_where("res_payment rp", array('rp.cre_date >' => "2019-12-01", 'status' => 'confirm'))->result();
        foreach ($res as $value) {
            $value->name = unserialize($value->name)['th'];
        }
        pre($res);
    }

    public function invoice_all($month = '') {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);

        require('application/libraries/fpdf.php');
        $pdf = new FPDF();
        $this->db->like("rp.cre_date", $month);
        $res = $this->db->select('r.name,r.address,rp.*')->join('restaurant r', 'r.id_res_auto=rp.id_res_auto')->get_where("res_payment rp", array('rp.del' => 0, 'rp.invoice_id !=' => 0))->result();
        foreach ($res as $data) {


            $data->address = unserialize($data->address)['th'];
            $data->name = unserialize($data->name)['th'];

            if ($data->id_res_auto == '245') {
                $_GET['cut'] = 45;
            }

            $pdf->AddPage();
            $pdf->Image('https://app.deltafood.co/img/logo.png', 10, 10, 30, 0, 'PNG');
            $pdf->AddFont('THSarabun', '', 'THSarabun.php'); //ธรรมดา
            $pdf->AddFont('THSarabun', 'b', 'THSarabun Bold.php'); //หนา
            $pdf->SetFont('THSarabun', 'b', 22);
            $pdf->Cell(0, 20, '                    บริษัท เดลต้า ซอฟต์ (ประเทศไทย) จำกัด');
            $pdf->SetFillColor(192);
            $pdf->RoundedRect(145, 10, 50, 20, 3, '1234', 'D');
            $pdf->Ln(7);
            $pdf->SetLineWidth(0.3);
            $pdf->SetFont('THSarabun', 'b', 20);
            if (isset($_GET['copy'])) {
                $pdf->Cell(0, 0, 'สำเนา               ', 0, 0, 'R');
            } else {
                $pdf->Cell(0, 0, 'ต้นฉบับ             ', 0, 0, 'R');
            }
            $pdf->Ln(7);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(0, 0, 'ใบเสร็จรับเงิน/ใบกำกับภาษี      ', 0, 0, 'R');
            $pdf->Ln(-7);
            $pdf->Line(45, 24, 135, 24);
            $pdf->SetFont('THSarabun', 'b', 22);
            $pdf->Cell(0, 20, '                     DELTA SOFT (THAILAND) CO., LTD.');
            $pdf->Ln(30);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(0, 0, '15 ม. 7 ต. หนองกุงใหญ่ อ. กระนวน จ. ขอนแก่น 40170 (สำนักงานใหญ่)');
            $pdf->Ln(7);
            $pdf->Cell(0, 0, '15 Moo 7 Tumbon Hnongkrungyai District Kranuan Provice Khonkaen 40170');
            $pdf->Ln(7);
            $pdf->Cell(0, 0, 'Tel : +66(0)83-236-9566  E-mail : deltasoftth@gmail.com');
            $pdf->Ln(7);
            $pdf->Cell(0, 0, 'เลขประจำตัวผู้เสียภาษี   0-4005-61003-57-3');
            $pdf->SetFillColor(192);
            $pdf->RoundedRect(10, 75, 185, 32, 3, '1234', 'D');
            $pdf->Ln(12);
            $pdf->Cell(0, 0, ' ชื่อลูกค้า/Customers :  ' . $data->name);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(0, 0, 'เลขที่ / No.  INV' . ((date('y', strtotime($data->cre_date)) + 543) % 100) . (date("m", strtotime($data->cre_date))) . sprintf("%03d", $data->invoice_id) . '     ', 0, 0, 'R');
            $pdf->Ln(7);
            $pdf->SetFont('THSarabun', '', 16);
            if (isset($_GET['cut'])) {
                $pdf->Cell(0, 0, ' ที่อยู่/ Address : ' . substr($data->address, 0, $_GET['cut']));
            } else {
                $pdf->Cell(0, 0, ' ที่อยู่/ Address : ' . $data->address);
            }
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(0, 0, 'วันที่ / Date : ' . date('d/m/', strtotime($data->cre_date)) . (date("Y", strtotime($data->cre_date)) + 543) . '     ', 0, 0, 'R');
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Ln(7);
            if (isset($_GET['cut'])) {
                $pdf->Cell(0, 0, '                       ' . substr($data->address, $_GET['cut']));
            }
            $pdf->Ln(7);

            $pdf->Ln(10);
            $pdf->Cell(25, 7, ' ลำดับที่', 'LT', 0, 'C', 0);
            $pdf->Cell(110, 7, 'รายการ', 'LT', 0, 'C', 0);
            $pdf->Cell(25, 7, 'ลงวันที่', 'LT', 0, 'C', 0);
            $pdf->Cell(25, 7, 'จำนวนเงิน', 'LTR', 1, 'C', 0);
            $pdf->Cell(25, 7, ' No.', 'LB', 0, 'C', 0);
            $pdf->Cell(110, 7, ' Description', 'LB', 0, 'C', 0);
            $pdf->Cell(25, 7, 'Date', 'LB', 0, 'C', 0);
            $pdf->Cell(25, 7, 'Amount', 'LBR', 0, 'C', 0);
            $pdf->Ln();
            for ($i = 0; $i < 5; $i++) {
                if ($i < 1) {
                    $pdf->Cell(25, 8, $i + 1, 'RL', 0, 'C', 0);
                    $pdf->Cell(110, 8, 'ค่าบริการรายเดือนระบบร้านอาหาร Deltafood', 'R', 0, 'L', 0);
                    $pdf->Cell(25, 8, date('d/m/', strtotime($data->cre_date)) . (date("Y", strtotime($data->cre_date)) + 543), 'R', 0, 'C', 0);
                    $pdf->Cell(25, 8, number_format($data->number * 100 / 107, 2), 'R', 0, 'R', 0);
                    $pdf->Ln();
                } else {
                    $pdf->Cell(25, 8, '', 'RL', 0, 'C', 0);
                    $pdf->Cell(110, 8, '', 'R', 0, 'L', 0);
                    $pdf->Cell(25, 8, '', 'R', 0, 'C', 0);
                    $pdf->Cell(25, 8, '', 'R', 0, 'R', 0);
                    $pdf->Ln();
                }
            }
            $pdf->Cell(25, 8, '', 'T', 0, 'C', 0);
            $pdf->Cell(110, 8, '', 'T', 0, 'L', 0);
            $pdf->Cell(25, 8, '', 'T', 0, 'C', 0);
            $pdf->Cell(25, 8, '', 'T', 0, 'R', 0);
            $pdf->Ln();
            $pdf->Cell(185, 7, 'รายการรับชำระเงิน          เงินสด          โอนเงิน           เช็ค', 0, 0, 'L', 0);
            $pdf->Ln(1);
            $pdf->Cell(35, 7, ' ', 0, 0, 'L', 0);
            $pdf->Cell(5, 5, ' ', 1, 0, 'L', 0);
            $pdf->Cell(15, 7, ' ', 0, 0, 'L', 0);
            $pdf->Cell(5, 5, ' ', 1, 0, 'L', 0);
            $pdf->Cell(20, 7, ' ', 0, 0, 'L', 0);
            $pdf->Cell(5, 5, ' ', 1, 0, 'L', 0);
            $payment_type = 1;
            if ($payment_type == 0) {
                $pdf->Image('https://app.deltafood.co/img/check_true.png', 45, 173, 6, 0, 'PNG');
            } else if ($payment_type == 1) {
                $pdf->Image('https://app.deltafood.co/img/check_true.png', 65, 173, 6, 0, 'PNG');
            } else {
                $pdf->Image('https://app.deltafood.co/img/check_true.png', 90, 173, 6, 0, 'PNG');
            }
            $pdf->Ln();
            $pdf->Cell(125, 7, '', 0, 0, 'L', 0);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(35, 8, 'รวมทั้งสิ้น ', 1, 0, 'L');
            $pdf->Cell(25, 8, number_format($data->number * 100 / 107, 2), 1, 0, 'R');
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Ln(8);
            $pdf->Cell(125, 7, 'จำนวนเงิน ' . number_format($data->number, 2) . " วันที่ " . date('d/m/', strtotime($data->cre_date)) . (date("Y", strtotime($data->cre_date)) + 543), 0, 0, 'L', 0);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(35, 8, 'VAT 7% ', 1, 0, 'L');
            $pdf->Cell(25, 8, number_format($data->number - ($data->number * 100 / 107), 2), 1, 0, 'R');
            $pdf->Ln(8);
            $pdf->Cell(125, 7, '', 0, 0, 'L', 0);
            $pdf->SetFont('THSarabun', 'b', 16);
            $pdf->Cell(35, 8, 'จำนวนเงินที่จ่ายสุทธิ ', 1, 0, 'L');
            $pdf->Cell(25, 8, number_format($data->number, 2), 1, 0, 'R');
            $pdf->Ln(16);
            $pdf->Cell(125, 7, 'ตัวอักษร    ' . number_to_text(number_format($data->number, 2)), 0, 0, 'L', 1);
            $pdf->Ln(10);
            $pdf->SetFont('THSarabun', '', 16);
            $pdf->Cell(110, 7, '', 0, 0, 'L', 0);
            $pdf->Cell(75, 8, 'ในนาม บริษัท เดลต้า ซอฟต์ (ประเทศไทย) จำกัด ', 'TRL', 0, 'C');
            $pdf->Ln(8);
            $pdf->Cell(110, 7, '', 0, 0, 'L', 0);
            if (!isset($_GET['cen']) || (isset($_GET['cen']) && $_GET['cen'] == '1')) {
                $pdf->Image('img/cen2.png', 135, 225, 45, 0, 'PNG');
            }
            $pdf->Cell(75, 18, ' ', 'RL', 0, 'C');
            $pdf->SetDash(0.8, 0.8);
            $pdf->Ln(0);
            $pdf->Cell(110, 8, '', 0, 0, 'L', 0);
            $pdf->Cell(75, 18, ' ', 'B', 0, 'C');
            $pdf->Ln(18);
            $pdf->SetDash();
            $pdf->Cell(110, 7, '', 0, 0, 'L', 0);
            $pdf->Cell(75, 8, ' ผู้มีอำนาจลงนาม', 'RLB', 0, 'C');
        }
        $pdf->Output();
    }

    public function invoice($id = '') {

        $res = $this->db->select('r.name,r.address,rp.*')->join('restaurant r', 'r.id_res_auto=rp.id_res_auto')->get_where("res_payment rp", array('rp.rp_id' => $id));
        $data = $res->row();
        $data->address = unserialize($data->address)['th'];
        $data->name = unserialize($data->name)['th'];

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        require('application/libraries/fpdf.php');
        $pdf = new FPDF();
        $pdf->AddPage();
        $pdf->Image('https://app.deltafood.co/img/logo.png', 10, 10, 30, 0, 'PNG');
        $pdf->AddFont('THSarabun', '', 'THSarabun.php'); //ธรรมดา
        $pdf->AddFont('THSarabun', 'b', 'THSarabun Bold.php'); //หนา
        $pdf->SetFont('THSarabun', 'b', 22);
        $pdf->Cell(0, 20, '                    บริษัท เดลต้า ซอฟต์ (ประเทศไทย) จำกัด');
        $pdf->SetFillColor(192);
        $pdf->RoundedRect(145, 10, 50, 20, 3, '1234', 'D');
        $pdf->Ln(7);
        $pdf->SetLineWidth(0.3);
        $pdf->SetFont('THSarabun', 'b', 20);
        if (isset($_GET['copy'])) {
            $pdf->Cell(0, 0, 'สำเนา               ', 0, 0, 'R');
        } else {
            $pdf->Cell(0, 0, 'ต้นฉบับ             ', 0, 0, 'R');
        }
        $pdf->Ln(7);
        $pdf->SetFont('THSarabun', '', 16);
        $pdf->Cell(0, 0, 'ใบเสร็จรับเงิน/ใบกำกับภาษี      ', 0, 0, 'R');
        $pdf->Ln(-7);
        $pdf->Line(45, 24, 135, 24);
        $pdf->SetFont('THSarabun', 'b', 22);
        $pdf->Cell(0, 20, '                     DELTA SOFT (THAILAND) CO., LTD.');
        $pdf->Ln(30);
        $pdf->SetFont('THSarabun', '', 16);
        $pdf->Cell(0, 0, '15 ม. 7 ต. หนองกุงใหญ่ อ. กระนวน จ. ขอนแก่น 40170 (สำนักงานใหญ่)');
        $pdf->Ln(7);
        $pdf->Cell(0, 0, '15 Moo 7 Tumbon Hnongkrungyai District Kranuan Provice Khonkaen 40170');
        $pdf->Ln(7);
        $pdf->Cell(0, 0, 'Tel : +66(0)83-236-9566  E-mail : deltasoftth@gmail.com');
        $pdf->Ln(7);
        $pdf->Cell(0, 0, 'เลขประจำตัวผู้เสียภาษี   0-4005-61003-57-3');
        $pdf->SetFillColor(192);
        $pdf->RoundedRect(10, 75, 185, 32, 3, '1234', 'D');
        $pdf->Ln(12);
        $pdf->Cell(0, 0, ' ชื่อลูกค้า/Customers :  ' . $data->name);
        $pdf->SetFont('THSarabun', 'b', 16);
        $pdf->Cell(0, 0, 'เลขที่ / No.  INV' . ((date('y', strtotime($data->cre_date)) + 543) % 100) . (date("m", strtotime($data->cre_date))) . sprintf("%03d", $data->invoice_id) . '     ', 0, 0, 'R');
        $pdf->Ln(7);
        $pdf->SetFont('THSarabun', '', 16);
        if (isset($_GET['cut'])) {
            $pdf->Cell(0, 0, ' ที่อยู่/ Address : ' . substr($data->address, 0, $_GET['cut']));
        } else {
            $pdf->Cell(0, 0, ' ที่อยู่/ Address : ' . $data->address);
        }
        $pdf->SetFont('THSarabun', 'b', 16);
        $pdf->Cell(0, 0, 'วันที่ / Date : ' . date('d/m/', strtotime($data->cre_date)) . (date("Y", strtotime($data->cre_date)) + 543) . '     ', 0, 0, 'R');
        $pdf->SetFont('THSarabun', '', 16);
        $pdf->Ln(7);
        if (isset($_GET['cut'])) {
            $pdf->Cell(0, 0, '                       ' . substr($data->address, $_GET['cut']));
        }
        $pdf->Ln(7);
        $pdf->Ln(10);
        $pdf->Cell(25, 7, ' ลำดับที่', 'LT', 0, 'C', 0);
        $pdf->Cell(110, 7, 'รายการ', 'LT', 0, 'C', 0);
        $pdf->Cell(25, 7, 'ลงวันที่', 'LT', 0, 'C', 0);
        $pdf->Cell(25, 7, 'จำนวนเงิน', 'LTR', 1, 'C', 0);
        $pdf->Cell(25, 7, ' No.', 'LB', 0, 'C', 0);
        $pdf->Cell(110, 7, ' Description', 'LB', 0, 'C', 0);
        $pdf->Cell(25, 7, 'Date', 'LB', 0, 'C', 0);
        $pdf->Cell(25, 7, 'Amount', 'LBR', 0, 'C', 0);
        $pdf->Ln();
        for ($i = 0; $i < 5; $i++) {
            if ($i < 1) {
                $pdf->Cell(25, 8, $i + 1, 'RL', 0, 'C', 0);
                $pdf->Cell(110, 8, 'ค่าบริการรายเดือนระบบร้านอาหาร Deltafood', 'R', 0, 'L', 0);
                $pdf->Cell(25, 8, date('d/m/', strtotime($data->cre_date)) . (date("Y", strtotime($data->cre_date)) + 543), 'R', 0, 'C', 0);
                $pdf->Cell(25, 8, number_format($data->number * 100 / 107, 2), 'R', 0, 'R', 0);
                $pdf->Ln();
            } else {
                $pdf->Cell(25, 8, '', 'RL', 0, 'C', 0);
                $pdf->Cell(110, 8, '', 'R', 0, 'L', 0);
                $pdf->Cell(25, 8, '', 'R', 0, 'C', 0);
                $pdf->Cell(25, 8, '', 'R', 0, 'R', 0);
                $pdf->Ln();
            }
        }
        $pdf->Cell(25, 8, '', 'T', 0, 'C', 0);
        $pdf->Cell(110, 8, '', 'T', 0, 'L', 0);
        $pdf->Cell(25, 8, '', 'T', 0, 'C', 0);
        $pdf->Cell(25, 8, '', 'T', 0, 'R', 0);
        $pdf->Ln();
        $pdf->Cell(185, 7, 'รายการรับชำระเงิน          เงินสด          โอนเงิน           เช็ค', 0, 0, 'L', 0);
        $pdf->Ln(1);
        $pdf->Cell(35, 7, ' ', 0, 0, 'L', 0);
        $pdf->Cell(5, 5, ' ', 1, 0, 'L', 0);
        $pdf->Cell(15, 7, ' ', 0, 0, 'L', 0);
        $pdf->Cell(5, 5, ' ', 1, 0, 'L', 0);
        $pdf->Cell(20, 7, ' ', 0, 0, 'L', 0);
        $pdf->Cell(5, 5, ' ', 1, 0, 'L', 0);
        $payment_type = 1;
        if ($payment_type == 0) {
            $pdf->Image('https://app.deltafood.co/img/check_true.png', 45, 173, 6, 0, 'PNG');
        } else if ($payment_type == 1) {
            $pdf->Image('https://app.deltafood.co/img/check_true.png', 65, 173, 6, 0, 'PNG');
        } else {
            $pdf->Image('https://app.deltafood.co/img/check_true.png', 90, 173, 6, 0, 'PNG');
        }
        $pdf->Ln();
        $pdf->Cell(125, 7, '', 0, 0, 'L', 0);
        $pdf->SetFont('THSarabun', 'b', 16);
        $pdf->Cell(35, 8, 'รวมทั้งสิ้น ', 1, 0, 'L');
        $pdf->Cell(25, 8, number_format($data->number * 100 / 107, 2), 1, 0, 'R');
        $pdf->SetFont('THSarabun', '', 16);
        $pdf->Ln(8);
        $pdf->Cell(125, 7, 'จำนวนเงิน ' . number_format($data->number, 2) . " วันที่ " . date('d/m/', strtotime($data->cre_date)) . (date("Y", strtotime($data->cre_date)) + 543), 0, 0, 'L', 0);
        $pdf->SetFont('THSarabun', 'b', 16);
        $pdf->Cell(35, 8, 'VAT 7% ', 1, 0, 'L');
        $pdf->Cell(25, 8, number_format($data->number - ($data->number * 100 / 107), 2), 1, 0, 'R');
        $pdf->Ln(8);
        $pdf->Cell(125, 7, '', 0, 0, 'L', 0);
        $pdf->SetFont('THSarabun', 'b', 16);
        $pdf->Cell(35, 8, 'จำนวนเงินที่จ่ายสุทธิ ', 1, 0, 'L');
        $pdf->Cell(25, 8, number_format($data->number, 2), 1, 0, 'R');
        $pdf->Ln(16);
        $pdf->Cell(125, 7, 'ตัวอักษร    ' . $this->convert(number_format($data->number, 2)), 0, 0, 'L', 1);
        $pdf->Ln(10);
        $pdf->SetFont('THSarabun', '', 16);
        $pdf->Cell(110, 7, '', 0, 0, 'L', 0);
        $pdf->Cell(75, 8, 'ในนาม บริษัท เดลต้า ซอฟต์ (ประเทศไทย) จำกัด ', 'TRL', 0, 'C');
        $pdf->Ln(8);
        $pdf->Cell(110, 7, '', 0, 0, 'L', 0);
        if (!isset($_GET['cen']) || (isset($_GET['cen']) && $_GET['cen'] == '1')) {
            $pdf->Image('img/cen2.png', 135, 225, 45, 0, 'PNG');
        }
        $pdf->Cell(75, 18, ' ', 'RL', 0, 'C');
        $pdf->SetDash(0.8, 0.8);
        $pdf->Ln(0);
        $pdf->Cell(110, 8, '', 0, 0, 'L', 0);
        $pdf->Cell(75, 18, ' ', 'B', 0, 'C');
        $pdf->Ln(18);
        $pdf->SetDash();
        $pdf->Cell(110, 7, '', 0, 0, 'L', 0);
        $pdf->Cell(75, 8, ' ผู้มีอำนาจลงนาม', 'RLB', 0, 'C');
        $pdf->Output();
    }
    /////////////////////////////////////////////////////////////////////////////////////
    
    public function convert($number) {
        $txtnum1 = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
        $txtnum2 = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
        $number = str_replace(",", "", $number);
        $number = str_replace(" ", "", $number);
        $number = str_replace("บาท", "", $number);
        $number = explode(".", $number);
        if (sizeof($number) > 2) {
            return 'ทศนิยมหลายตัวนะจ๊ะ';
            exit;
        }
        $strlen = strlen($number[0]);
        $convert = '';
        for ($i = 0; $i < $strlen; $i++) {
            $n = substr($number[0], $i, 1);
            if ($n != 0) {
                if ($i == ($strlen - 1) AND $n == 1) {
                    $convert .= 'เอ็ด';
                } elseif ($i == ($strlen - 2) AND $n == 2) {
                    $convert .= 'ยี่';
                } elseif ($i == ($strlen - 2) AND $n == 1) {
                    $convert .= '';
                } else {
                    $convert .= $txtnum1[$n];
                }
                $convert .= $txtnum2[$strlen - $i - 1];
            }
        }

        $convert .= 'บาท';
        if ($number[1] == '0' OR $number[1] == '00' OR
                $number[1] == '') {
            $convert .= 'ถ้วน';
        } else {
            $strlen = strlen($number[1]);
            for ($i = 0; $i < $strlen; $i++) {
                $n = substr($number[1], $i, 1);
                if ($n != 0) {
                    if ($i == ($strlen - 1) AND $n == 1) {
                        $convert .= 'เอ็ด';
                    } elseif ($i == ($strlen - 2) AND
                            $n == 2) {
                        $convert .= 'ยี่';
                    } elseif ($i == ($strlen - 2) AND
                            $n == 1) {
                        $convert .= '';
                    } else {
                        $convert .= $txtnum1[$n];
                    }
                    $convert .= $txtnum2[$strlen - $i - 1];
                }
            }
            $convert .= 'สตางค์';
        }
        return $convert;
    }

}
