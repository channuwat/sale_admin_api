<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    public function index() {
//        pre($_SESSION);
        $u = $this->session->userdata('data');
//        pre($u);
        echo json_encode($u);
    }

    public function login() {
        $p = _post();
        $this->db->select("s_id,s_code,name,parent,username,email,phone,picture,address,nick_name,register_date");
        $this->db->group_start();
        $this->db->or_where("username", $p->email);
        $this->db->or_where("email", $p->email);
        $this->db->or_where("phone", $p->email);
        $this->db->group_end();
        $this->db->where("password", md5($p->password));
        $s = $this->db->get("sale_saler");
        if ($s->num_rows() > 0) {
            $this->session->set_userdata(array('data' => $s->row()));
            echo json_encode(array('flag' => true, 'data' => $s->row()));
        } else {
            echo json_encode(array('flag' => false, 'message' => "ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง"));
        }
    }

    public function getProfile()
    {
        $p = _post();
        $user = $this->db->select("s_id,s_code,name,parent,username,email,phone,picture,address,nick_name,register_date")
        ->get_where('sale_saler',array('s_id' => $p->user));
        echo json_encode($user->row());
    }


    public function updateProfile()
    {
        $p = _post();
        $updateArry = array(
            'nick_name' => $p->nick_name,
            'name' => $p->name,
            'email' => $p->email,
            'phone' => $p->phone,
            'address' => $p->address,
        );
        if($p->password != '')
        {
            $updateArry['password'] = md5($p->password);
        }
        
        $update = $this->db
        ->where('s_id',$p->s_id)
        ->update('sale_saler', $updateArry);
        if($update){
            $user = $this->db
            ->select("s_id,s_code,name,parent,username,email,phone,picture,address,nick_name,register_date")
            ->from('sale_saler')
            ->where('s_id',$p->s_id)->get()->row();
            echo json_encode(array('flag'=>true,'data'=>$user));
        }
        
    }

    public function updatePosition()
    {
        $p = _post();
        $update = $this->db
        ->where('s_id',$p->s_id)
        ->update('sale_saler',array('s_position' => $p->s_position));
    }

    public function createUser()
    {
        $p = _post();
        $state = false;

        $p->password = md5($p->password);
        $data = $p;
        $create = $this->db->insert('sale_saler', $data);
        if($create){
            $state = true;
        }else{
            $state = false;
        }

        $id_user_new = $this->db->insert_id();
        if($p->bank_id != 0 || $p->bank_id != ''){
            $create_bb = $this->db
            ->insert('sale_bookbank',array('sale_id'=>$id_user_new,'sb_ref_id'=>$p->bank_id,'sb_account'=>$p->bank_account));
            if($create_bb){
                $state = true;
            }else{
                $state = false;
            }
        }
        
        echo json_encode($state);
    }

}
