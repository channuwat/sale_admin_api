<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    function fb_date($timestamp) {
        $difference = time() - $timestamp;
        $difference2 = time() - $timestamp;
        $difference3 = time() - $timestamp;
        $periods = array("วินาที", "นาที", "ชม.");
        $ending = ''; // " ที่ผ่านมา...";
        if ($difference < 60) {
            $j = 0;
            $periods[$j] .= ($difference != 1) ? "" : "";
            $difference = ($difference == 3 || $difference == 4) ? "3 - 4 " : $difference;
            $text = "$difference $periods[$j] $ending";
        } elseif ($difference < 3600) {
            $j = 1;
            $difference = round($difference / 60);
            $periods[$j] .= ($difference != 1) ? "" : "";
            $difference = ($difference == 3 || $difference == 4) ? "3 - 4 " : $difference;
            $text = "$difference $periods[$j] $ending";
        } elseif ($difference < 86400) {
            $j = 2;
            $difference = round($difference / 3600);
            $min = round($difference2 % 86400 % 3600 / 60);
            $periods[$j] .= ($difference != 1) ? "" : "";
            $difference = ($difference != 1) ? $difference : "1";
            $text = "$difference $periods[$j] $min นาที $ending";
        } elseif ($difference < 172800) {
            $difference = round($difference / 86400);
            $hour = round($difference3 % 86400 / 3600);
            $min = round($difference2 % 86400 % 3600 / 60);
//            $periods[$j] .= ($difference != 1) ? ".." : "";
            $text = "$difference วัน";
            //$text = "$difference วัน  $hour ชม.  $min น."; ตัวเดิม
        } elseif ($difference != 172800) {
            $difference = round($difference / 86400);
            $hour = round($difference3 % 86400 / 3600);
            $min = round($difference2 % 86400 % 3600 / 60);
//            $periods[$j] .= ($difference != 1) ? ".." : "";
            $text = "$difference วันที่เเล้ว";
        } else {
            if ($timestamp < strtotime(date("Y-01-01 00:00:00"))) {
                $text = date("l j, Y", $timestamp) . " at " . date("g:ia", $timestamp);
            } else {
                $text = date("l j", $timestamp) . " at " . date("g:ia", $timestamp);
            }
        }
        return $text;
    }

    public function index() {
//        pre($_SESSION);
        $u = $this->session->userdata('data');
//        pre($u);
        echo json_encode($u);
    }

    public function load_article($number = 1) 
    { 
        $this->db->limit(20, ($number - 1) * 20);

        $article = $this->db->order_by('sa_id','desc')
        ->select('sa.*,ss.name')
        ->join('sale_saler ss', 'ss.s_id=sa.saler_id')
        ->get_where("sale_article sa", array())
        ->result();

        foreach ($article as $value) {
            $value->time = $this->fb_date(strtotime($value->cre_date));

            $value->count_comment = $this->db
            ->get_where("sale_comments", array('sa_id' => $value->sa_id))
            ->num_rows();
        }
        echo json_encode($article);
    }

    public function load_comment($sa_id,$limit) {
        if($limit == '2'){
            $sale_comment = $this->db
            ->limit(2)
            ->select('sc.*,ss.name')
            ->join('sale_saler ss', 'ss.s_id=sc.saler_id')
            ->get_where("sale_comments sc", array('sa_id' => $sa_id, 'del' => 0))
            ->result();
        }else if($limit == 'all'){
            $sale_comment = $this->db->select('sc.*,ss.name')
            ->join('sale_saler ss', 'ss.s_id=sc.saler_id')
            ->get_where("sale_comments sc", array('sa_id' => $sa_id, 'del' => 0))
            ->result();
        }
        

        foreach ($sale_comment as $value) {
            $value->time = $this->fb_date(strtotime($value->cre_date));
        }

        echo json_encode($sale_comment);
    }

    public function add_article() {
        $p = _post();
        $p->cre_date = date("Y-m-d H:i:s");
        $p->status = 1;
        echo $this->db->insert("sale_article", $p);
    
    }

    public function answerArticle()
    {
        $p = _post();

        $article = $this->db
        ->select('sa.*,ss.s_id,ss.name,ss.nick_name')
        ->join('sale_saler ss', 'ss.s_id=sa.saler_id')
        ->get_where("sale_article sa", array('sa_id' => $p->articleId->sa_id))
        ->result();

        foreach ($article as $value) {
            $value->time = $this->fb_date(strtotime($value->cre_date));
            $value->count_comment = $this->db->get_where("sale_comments", array('sa_id' => $value->sa_id))->num_rows();
        }
        //pre($p->articleId->sa_id);
        echo json_encode($article);
    }

    public function addRecomment()
    {
        $p = _post();
        $p->cre_date = date("Y-m-d H:i:s");
        $p->del = 0;
        $status = ['status' => false] ;
        if($p->content != ''){
            if($this->db->insert("sale_comments",$p))
            {
                $status = ['status' => true] ;
            }
        }
        echo json_encode($status);
    }

    public function updateRecomment()
    {
        $status = ['status' => false] ; // เช็คว่า update มั้ย

        $p = _post();
        
        $id_saller_count = $this->db
        ->select('saler_id')
        ->where('sa_id' , $p->sa_id)
        ->where('saler_id' , $p->saler_id)
        ->get('sale_comments');
        
        if($id_saller_count->num_rows() > 0)
        {
            $updateArray = array('content' => $p->content);
            $this->db->where('sc_id' , $p->sc_id)
            ->update('sale_comments', $updateArray);
        }
        else
        {
        }
    }

    public function delRecomment()
    {
        $p = _post();
        if($p->user_id == $p->saler_id)
        {
            $this->db->where('sc_id',  $p->sc_id);
            $this->db->delete('sale_comments');
        }
        
    }

    public function getBadgesArticle($user_id)
    {
        $re_id = $this->db->select('id_res_auto')
        ->get_where('restaurant',array('saler_id' => $user_id))
        ->result();
        $re_id_array = array();
        foreach($re_id as $i => $re_id_v){ $re_id_array[$i] = $re_id_v->id_res_auto; }
        $re_id_new = implode(',',$re_id_array);

        $qryBadges = $this->db
        ->select('COUNT(c_id) as badges')   
        ->where("id_res_auto in(".$re_id_new.")")
        ->get_where('chat',array('readed' => 0,'by_id <>' => 0))
        ->row();
        echo json_encode($qryBadges->badges);
    }

    public function del_article()
    {
        $p = _post();
        $this->db->delete('sale_article', array('sa_id' => $p->sa_id)); 
        $this->db->delete('sale_comments', array('sa_id' => $p->sa_id)); 
    }

    public function edit_article()
    {
        $p = _post();
        $updated = $this->db
        ->where('sa_id',$p->sa_id)
        ->update('sale_article', array('content'=>$p->content));
        if($updated){
            echo json_encode(true);
        }
    }

    public function edit_comment()
    {
        $p = _post();
        $updated = $this->db
        ->where('sc_id',$p->sc_id)
        ->update('sale_comments', array('content'=>$p->content));
        if($updated){
            echo json_encode(true);
        }
    }

    public function del_comment()
    {
        $p = _post();
        $this->db->delete('sale_comments', array('sc_id' => $p->sc_id)); 
    }

}
