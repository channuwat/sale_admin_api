<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurant extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    public function index() {
        echo json_encode($u);
    }

    public function manual() {
        $out = array(
            array(
                'title' => 'การติดตั้ง',
                'videos' => array(
                    array('title' => 'vidwo1', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c'),
                    array('title' => 'ติดตั้งโปรแกรม', 'url' => 'https://www.youtube.com/embed/hM0TsOj2d5k'),
                    array('title' => 'test', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c'))
            ),
            array(
                'title' => 'การตั้งค่า',
                'videos' => array(
                    array('title' => 'test', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c'),
                    array('title' => 'test', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c'),
                    array('title' => 'test', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c'))
            ),
            array(
                'title' => 'การใช้งาน',
                'videos' => array(
                    array('title' => 'test', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c'),
                    array('title' => 'test', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c'),
                    array('title' => 'test', 'url' => 'https://www.youtube.com/embed/th3Kkr0M09c')
                )
            )
        );
        echo json_encode($out);
    }

    public function dashboard($sale_id = 0) {
        $income = $this->db->select('sum(amount) as sum_sum')->get_where("sale_transition", array('sale_id' => $sale_id, 'type' => 1));
        if ($income->num_rows() > 0) {
            $qryIncome = $income->row()->sum_sum; // รายได้รวมทั้งหมด
        }

        $customer = $this->db->select('COUNT(id_res_auto) as sum_customer')->get_where("restaurant", array('saler_id' => $sale_id));
        if ($customer->num_rows() > 0) {
            $qrycustomer = $customer->row()->sum_customer; // จำนวนลูกค้า
        }

        $child = $this->db->select('COUNT(s_id) as sum_child')->get_where("sale_saler", array('parent' => $sale_id));
        if ($child->num_rows() > 0) {
            $qryChild = $child->row()->sum_child; // จำนวนลูกทีม
        }
        //////// หา pk id ของตัวลูก ///////
        $saleChild_id = $this->db
                ->select('s_id')
                ->get_where('sale_saler', array('parent' => $sale_id))
                ->result();
        $pkIDarray = array();
        if (count($saleChild_id) != 0) {
            foreach ($saleChild_id as $index => $val) {
                array_push($pkIDarray, $val->s_id); // แตก obj เหลือแค่ value เเล้วไปเก็บเป็น array
            }
        } else {
            // ใช้ 99999 เพื่อให้มัน qry ใน where in ได้ถ้ากำนดเป็น 0 มันจะไปดึงพวก parent id ที่เป็นแม่ออกมา หรือ -1 มันจะ error
            // case นี้เราต้องการ id ตัวลูก
            $pkIDarray = [99999];
        }
        $pkChildID = implode(",", $pkIDarray); // จะแปลง array s_id ให้มาต่อ str กันโดยมี , คั้นในเเต่ลละ array
        //////// หา pk id ของตัวหลาน ///////
        /*
          หลังจากมาทำงานในตำแหน่งนี้เป็นต้นไป $strIdGrandChild , $pkIDarray จะโดนรีค่าใหม่เป็นการ qry เเละเก็บ array ใหม่
          เพื่อจะ qry เอาข้อมูลของหลาน
         */
        $strIdGrandChild = "SELECT s_id FROM `sale_saler` WHERE `parent` IN(" . $pkChildID . ")";
        $qryIdGrandChild = $this->db->query($strIdGrandChild)->result();
        $pkIDarray = array();
        if (count($qryIdGrandChild) != 0) {
            foreach ($qryIdGrandChild as $inde => $val) {
                array_push($pkIDarray, $val->s_id); // แตก obj เหลือแค่ value เเล้วไปเก็บเป็น array
            }
        } else {
            // ใช้ 99999 เพื่อให้มัน qry ใน where in ได้ถ้ากำนดเป็น 0 มันจะไปดึงพวก parent id ที่เป็นแม่ออกมา หรือ -1 มันจะ error
            // case นี้เราต้องการ id ตัวลูก
            $pkIDarray = [99999];
        }

        $pkGrandChildID = implode(",", $pkIDarray); // จะแปลง array s_id ให้มาต่อ str กันโดยมี , คั้นในเเต่ลละ array
        $grandchild = $this->db->select('COUNT(s_id) as sum_grandchild')->from("sale_saler")->where("s_id IN(" . $pkGrandChildID . ")")->get();
        if ($grandchild->num_rows() > 0) {
            $qryTeam = $grandchild->row()->sum_grandchild + $qryChild; // จำนวนหลานทีม
        }

        /**
         * set ค่าเดือนเพื่อไปคำนวนหาลูกค้าหรือรายได้ในเเต่ละเดือนของ sale คนนั้นๆ
         * อนาคตหากจะค้นหาตามปีให้ไป set ตัวเเปร year ให้รับค่า year เข้ามาจาก input 
         * ก่อนเอาตัวแปร year ไปใส่ qry เงื่อนไข
         */
        $qryCusPerMount[1] = '';
        $qryIncomePerMount[1] = '';


        for ($i = 1; $i <= 12; $i++) {
            $mount = $i;
            if ($mount < 10) {
                $mount = '0' . $mount;
            }
            $year = date("Y") . "-" . $mount;

            // ลูกค้า/เดือน
            $qryCusPerMount[$i] = $this->db
                            ->select('count(id_res_auto) as cus_count')
                            ->from('restaurant')
                            ->where("creat_date LIKE '" . $year . "%'")
                            ->where("saler_id = " . $sale_id)
                            ->get()->row();

            // รายได้/เดือน
            $qryIncomePerMount[$i] = $this->db
                            ->select('SUM(st.balance) as sum_pay')
                            ->from('sale_transition as st')
                            ->join('restaurant as res', 'st.id_res_auto = res.id_res_auto')
                            ->where("st.sale_id  = " . $sale_id . "")
                            ->where("st.date_time LIKE '" . $year . "%'")
                            ->where("st.id_res_auto <> 0")
                            ->get()->row();
            $qryIncomePerMount[$i] = $qryIncomePerMount[$i]->sum_pay + 0; // set str ให้เป็นตัวเลข +0 เพื่อตัดทศนิยม
        }

        $out = array(
            'data' => array(
                array('link' => '/income', 'icon' => 'monetization_on', 'value' => $qryIncome, 'title' => 'รายได้'),
                array('link' => '/customer', 'icon' => 'people_alt', 'value' => $qrycustomer, 'title' => 'ลูกค้า'),
                array('link' => '/myteam', 'icon' => 'share', 'value' => $qryTeam, 'title' => 'ลูกทีม'),
            ),
            'grahp' =>
            array(
                array(
                    'title' => 'จำนวนร้าน',
                    'data' => array(
                        array('name' => 'จำนวน', 'series' => array(
                                array('name' => 'ม.ค', 'value' => $qryCusPerMount[1]->cus_count),
                                array('name' => 'ก.พ', 'value' => $qryCusPerMount[2]->cus_count),
                                array('name' => 'มี.ค', 'value' => $qryCusPerMount[3]->cus_count),
                                array('name' => 'เม.ย', 'value' => $qryCusPerMount[4]->cus_count),
                                array('name' => 'พ.ค', 'value' => $qryCusPerMount[5]->cus_count),
                                array('name' => 'มิ.ย', 'value' => $qryCusPerMount[6]->cus_count),
                                array('name' => 'ก.ค', 'value' => $qryCusPerMount[7]->cus_count),
                                array('name' => 'ส.ค', 'value' => $qryCusPerMount[8]->cus_count),
                                array('name' => 'ก.ย', 'value' => $qryCusPerMount[9]->cus_count),
                                array('name' => 'ต.ค', 'value' => $qryCusPerMount[10]->cus_count),
                                array('name' => 'พ.ย', 'value' => $qryCusPerMount[11]->cus_count),
                                array('name' => 'ธ.ค', 'value' => $qryCusPerMount[12]->cus_count)
                            )
                        )
                    )
                ),
                array(
                    'title' => 'รายได้',
                    'data' => array(
                        array('name' => 'จำนวน', 'series' => array(
                                array('name' => 'ม.ค', 'value' => $qryIncomePerMount[1]),
                                array('name' => 'ก.พ', 'value' => $qryIncomePerMount[2]),
                                array('name' => 'มี.ค', 'value' => $qryIncomePerMount[3]),
                                array('name' => 'เม.ย', 'value' => $qryIncomePerMount[4]),
                                array('name' => 'พ.ค', 'value' => $qryIncomePerMount[5]),
                                array('name' => 'มิ.ย', 'value' => $qryIncomePerMount[6]),
                                array('name' => 'ก.ค', 'value' => $qryIncomePerMount[7]),
                                array('name' => 'ส.ค', 'value' => $qryIncomePerMount[8]),
                                array('name' => 'ก.ย', 'value' => $qryIncomePerMount[9]),
                                array('name' => 'ต.ค', 'value' => $qryIncomePerMount[10]),
                                array('name' => 'พ.ย', 'value' => $qryIncomePerMount[11]),
                                array('name' => 'ธ.ค', 'value' => $qryIncomePerMount[12])
                            )
                        )
                    )
                )
            )
        );
        echo json_encode($out);
    }

    public function selectGhrapByYear($sale_id = 0, $yearPick) {
        for ($i = 1; $i <= 12; $i++) {
            $mount = $i;
            if ($mount < 10) {
                $mount = '0' . $mount;
            }
            $year = $yearPick . "-" . $mount;

            // ลูกค้า/เดือน
            $qryCusPerMount[$i] = $this->db
                            ->select('count(id_res_auto) as cus_count')
                            ->from('restaurant')
                            ->where("creat_date LIKE '" . $year . "%'")
                            ->where("saler_id = " . $sale_id)
                            ->get()->row();

            // รายได้/เดือน
            $qryIncomePerMount[$i] = $this->db
                            ->select('SUM(st.balance) as sum_pay')
                            ->from('sale_transition as st')
                            ->join('restaurant as res', 'st.id_res_auto = res.id_res_auto')
                            ->where("st.sale_id  = " . $sale_id . "")
                            ->where("st.date_time LIKE '" . $year . "%'")
                            ->where("st.id_res_auto <> 0")
                            ->get()->row();
            $qryIncomePerMount[$i] = $qryIncomePerMount[$i]->sum_pay + 0; // set str ให้เป็นตัวเลข +0 เพื่อตัดทศนิยม
        }

        $out = array(
            'grahp' =>
            array(
                array(
                    'title' => 'จำนวนร้าน',
                    'data' => array(
                        array('name' => 'จำนวน', 'series' => array(
                                array('name' => 'ม.ค', 'value' => $qryCusPerMount[1]->cus_count),
                                array('name' => 'ก.พ', 'value' => $qryCusPerMount[2]->cus_count),
                                array('name' => 'มี.ค', 'value' => $qryCusPerMount[3]->cus_count),
                                array('name' => 'เม.ย', 'value' => $qryCusPerMount[4]->cus_count),
                                array('name' => 'พ.ค', 'value' => $qryCusPerMount[5]->cus_count),
                                array('name' => 'มิ.ย', 'value' => $qryCusPerMount[6]->cus_count),
                                array('name' => 'ก.ค', 'value' => $qryCusPerMount[7]->cus_count),
                                array('name' => 'ส.ค', 'value' => $qryCusPerMount[8]->cus_count),
                                array('name' => 'ก.ย', 'value' => $qryCusPerMount[9]->cus_count),
                                array('name' => 'ต.ค', 'value' => $qryCusPerMount[10]->cus_count),
                                array('name' => 'พ.ย', 'value' => $qryCusPerMount[11]->cus_count),
                                array('name' => 'ธ.ค', 'value' => $qryCusPerMount[12]->cus_count)
                            )
                        )
                    )
                ),
                array(
                    'title' => 'รายได้',
                    'data' => array(
                        array('name' => 'จำนวน', 'series' => array(
                                array('name' => 'ม.ค', 'value' => $qryIncomePerMount[1]),
                                array('name' => 'ก.พ', 'value' => $qryIncomePerMount[2]),
                                array('name' => 'มี.ค', 'value' => $qryIncomePerMount[3]),
                                array('name' => 'เม.ย', 'value' => $qryIncomePerMount[4]),
                                array('name' => 'พ.ค', 'value' => $qryIncomePerMount[5]),
                                array('name' => 'มิ.ย', 'value' => $qryIncomePerMount[6]),
                                array('name' => 'ก.ค', 'value' => $qryIncomePerMount[7]),
                                array('name' => 'ส.ค', 'value' => $qryIncomePerMount[8]),
                                array('name' => 'ก.ย', 'value' => $qryIncomePerMount[9]),
                                array('name' => 'ต.ค', 'value' => $qryIncomePerMount[10]),
                                array('name' => 'พ.ย', 'value' => $qryIncomePerMount[11]),
                                array('name' => 'ธ.ค', 'value' => $qryIncomePerMount[12])
                            )
                        )
                    )
                )
            )
        );
        echo json_encode($out);
    }

    public function get_res($sale_id, $typeExp) {


        if ($typeExp == 1) {
            $customer = $this->db->limit(50)->order_by("expired_date", 'asc')
                            ->select('id_res_auto,name,address,logo_url,expired_date,count_table,count_sub_group,p_pay,pay_before,creat_date,tel')
                            ->from("restaurant")
                            ->where("saler_id = " . $sale_id)
                            ->where('date(expired_date) > date(NOW())')->get()->result();
        } else {
            $customer = $this->db->limit(50)->order_by("expired_date", 'asc')
                            ->select('id_res_auto,name,address,logo_url,expired_date,count_table,count_sub_group,p_pay,pay_before,creat_date,tel')
                            ->from("restaurant")
                            ->where("saler_id = " . $sale_id)
                            ->where('date(expired_date) <= date(NOW())')->get()->result();
        }
        foreach ($customer as $value) {
            $value->name = array_lang($value->name, 'th');
            $value->address = array_lang($value->address, 'th');
            // set date remaining
            $value->remaining = 0;
            $date1 = date_create(date('yy-m-d'));
            $date2 = date_create($value->expired_date);
            $diff = date_diff($date1, $date2);
            if ((int) $diff->format("%a") < 11) {
                // ให้เตือนเมื่อน้อยกว่าหรือเท่ากับ 10 วัน
                $value->remaining = (int) $diff->format("%R%a");
            }
        }
        echo json_encode($customer);
    }
    

    

    

    // new app
    public function getRes($sale_id,$type = 1,$page = 0) {
        if($type == 1){
            $customer = $this->db->limit(5,$page)->order_by("expired_date", 'asc')
            ->select('id_res_auto,name,address,logo_url,expired_date,count_table,count_sub_group,p_pay,pay_before,creat_date,tel')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->get()->result();

            $customer_page = $this->db
            ->select('id_res_auto')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->get()->num_rows();
        }else if($type == 2){
            $customer = $this->db->limit(5,$page)->order_by("expired_date", 'asc')
            ->select('id_res_auto,name,address,logo_url,expired_date,count_table,count_sub_group,p_pay,pay_before,creat_date,tel')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->where("expired_date >= ",date("Y-m-d"))
            ->get()->result();

            $customer_page = $this->db
            ->select('id_res_auto')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->where("expired_date >= ",date("Y-m-d"))
            ->get()->num_rows();
        }else if($type == 3){
            $customer = $this->db->limit(5,$page)->order_by("expired_date", 'asc')
            ->select('id_res_auto,name,address,logo_url,expired_date,count_table,count_sub_group,p_pay,pay_before,creat_date,tel')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->where("expired_date >= ",date("Y-m-d"))
            ->where("expired_date <=",date("Y-m-d",strtotime("+10 days")))
            ->get()->result();

            $customer_page = $this->db
            ->select('id_res_auto')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->where("expired_date >= ",date("Y-m-d"))
            ->where("expired_date <=",date("Y-m-d",strtotime("+10 days")))
            ->get()->num_rows();
        }else{
            $customer = $this->db->limit(5,$page)->order_by("expired_date", 'asc')
            ->select('id_res_auto,name,address,logo_url,expired_date,count_table,count_sub_group,p_pay,pay_before,creat_date,tel')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->where("expired_date <= ",date("Y-m-d"))
            ->get()->result();

            $customer_page = $this->db
            ->select('id_res_auto')
            ->from("restaurant")
            ->where("saler_id = ",$sale_id)
            ->where("expired_date <= ",date("Y-m-d"))
            ->get()->num_rows();
            
        }

        foreach ($customer as $value) {
            $value->name = array_lang($value->name, 'th');
            $value->address = array_lang($value->address, 'th');
            
            $date1 = date_create(date('yy-m-d'));
            $date2 = date_create($value->expired_date);
            $diff = date_diff($date1, $date2);
            
            // หาจำนวนร้านค้าที่จะหมดอายุภายใน 10 วัน //
            if($type == 3){
                // set date remaining
                $value->remaining = 0;
                if ((int) $diff->format("%a") < 11) {
                    // ให้เตือนเมื่อน้อยกว่าหรือเท่ากับ 10 วัน
                    $value->remaining = (int) $diff->format("%R%a");
                }
            }
        }
        $data['customer'] = $customer;
        $data['page'] = $customer_page;

        echo json_encode($data);
    }

    public function getIncome() {
        $p = _post();
        $qryIncome = $this->db->select('*')->from('sale_transition st')
        ->join('restaurant res','st.id_res_auto = res.id_res_auto','left')
        ->where('st.sale_id',$p->user)
        ->limit(5,$p->page)
        ->order_by('st.date_time','DESC')
        ->get()
        ->result();  
        foreach ($qryIncome as $qryIncomeV) {
            $qryIncomeV->name = unserialize($qryIncomeV->name);
        }

        $total = $this->db->select('amount,type')->from('sale_transition st')
        ->join('restaurant res','st.id_res_auto = res.id_res_auto','left')
        ->where('st.sale_id',$p->user)
        ->get()
        ->result();  

        $dataRow = $this->db->select('sale_id')->from('sale_transition st')
        ->join('restaurant res','st.id_res_auto = res.id_res_auto','left')
        ->where('st.sale_id',$p->user)
        ->get()
        ->num_rows(); 

        $income = array(
            'income_row' => $qryIncome,
            'total' => $total,
            'page' => $dataRow
        );

        echo json_encode($income);
    }

    public function getTotalMoneny() {
        $p = _post();
        $out = array('total_withdraw' => 0, "total" => 0);
        // รายได้รวมเบิกได้ qry
        $qryBalance = $this->db->select('balance')->order_by('st_id', 'desc')->limit(1)->get_where("sale_transition", array('sale_id' => $p->user, 'type' => 1));
        if ($qryBalance->num_rows() > 0) {
            $out['total_withdraw'] = $qryBalance->row()->balance;
        }
        //รายได้รวมทั้งหมด qry
        $qryTotal = $this->db->select('sum(amount) as sum_sum')->get_where("sale_transition", array('sale_id' => $p->user, 'type' => 1));
        if ($qryTotal->num_rows() > 0) {
            $out['total'] = $qryTotal->row()->sum_sum;
        }
        echo json_encode($out);
    }

    public function getCountCustomer($user) {
        $countCustomer = [0, 0];
        $customerNM = $this->db
                        ->select('COUNT(id_res_auto) as count_cus')
                        ->from("restaurant")
                        ->where("saler_id = " . $user)
                        ->where('date(expired_date) > date(NOW())')->get()->row();
        $customerEXP = $this->db
                        ->select('COUNT(id_res_auto) as count_cus')
                        ->from("restaurant")
                        ->where("saler_id = " . $user)
                        ->where('date(expired_date) <= date(NOW())')->get()->row();
        $countCustomer[0] = $customerNM->count_cus;
        $countCustomer[1] = $customerEXP->count_cus;
        echo json_encode($countCustomer);
    }

    public function get_Res_Income($user)
    {
        $statusTable = array();
        $income = $this->db->select('sum(amount) as sum_sum')->get_where("sale_transition", array('sale_id' => $user, 'type' => 0));
        if ($income->num_rows() > 0) {
            $qryIncome = $income->row()->sum_sum; // รายได้รวมทั้งหมด
            $statusTable[0] = array( 'title'=> 'รายได้เบิกเเล้ว', 'detail'=> $qryIncome, 'unit'=> 'บาท' );
        }

        $customer = $this->db->select('COUNT(id_res_auto) as sum_customer')->get_where("restaurant", array('saler_id' => $user));
        if ($customer->num_rows() > 0) {
            $qrycustomer = $customer->row()->sum_customer; // จำนวนลูกค้า
            $statusTable[1] = array( 'title'=> 'ร้านอาหาร', 'detail'=> $qrycustomer, 'unit'=> 'ราย' );
        }
        echo json_encode($statusTable);
    }

    public function getValueChart($user,$year)
    {
        $setYear = array();
        for($i = 1;$i <= 12;$i++){
            $mount = $i;
            if($mount<10){
                $mount = '0'.$mount;
            }
            $setYear[$i] = $year.'-'.$mount;

            // รายได้/เดือน
            $qryIncomePerMount[$i] = $this->db
                            ->select('SUM(st.balance) as sum_pay')
                            ->from('sale_transition as st')
                            ->join('restaurant as res', 'st.id_res_auto = res.id_res_auto')
                            ->where("st.sale_id  = " . $user . "")
                            ->where("st.date_time LIKE '" . $setYear[$i] . "%'")
                            ->where("st.id_res_auto <> 0")
                            ->get()->row();
            $qryIncomePerMount[$i] = $qryIncomePerMount[$i]->sum_pay + 0; // set str ให้เป็นตัวเลข +0 เพื่อตัดทศนิยม
            
            // ลูกค้า/เดือน
            $qryCusPerMount[$i] = $this->db
                            ->select('count(id_res_auto) as cus_count')
                            ->from('restaurant')
                            ->where("creat_date LIKE '" . $setYear[$i] . "%'")
                            ->where("saler_id = " . $user)
                            ->get()->row();
        }

        $data = array(
            array(
                'labelY' => 'รายได้/เดือน',
                'labelX' => 'เดือน',
                'line' => 
                array(
                    array(
                        'name' => 'รายได้',
                        "series" => 
                        array(
                            array('name' => 'ม.ค', 'value' => $qryIncomePerMount[1]),
                            array('name' => 'ก.พ', 'value' => $qryIncomePerMount[2]),
                            array('name' => 'มี.ค', 'value' => $qryIncomePerMount[3]),
                            array('name' => 'เม.ย', 'value' => $qryIncomePerMount[4]),
                            array('name' => 'พ.ค', 'value' => $qryIncomePerMount[5]),
                            array('name' => 'มิ.ย', 'value' => $qryIncomePerMount[6]),
                            array('name' => 'ก.ค', 'value' => $qryIncomePerMount[7]),
                            array('name' => 'ส.ค', 'value' => $qryIncomePerMount[8]),
                            array('name' => 'ก.ย', 'value' => $qryIncomePerMount[9]),
                            array('name' => 'ต.ค', 'value' => $qryIncomePerMount[10]),
                            array('name' => 'พ.ย', 'value' => $qryIncomePerMount[11]),
                            array('name' => 'ธ.ค', 'value' => $qryIncomePerMount[12])
                        )
                    )
                )
            ),
            array(
                'labelY' => 'ลูกค้า/เดือน',
                'labelX' => 'เดือน',
                'line' => 
                array(
                    array(
                        'name' => 'จำนวนลูกค้า',
                        "series" => 
                        array(
                            array('name' => 'ม.ค', 'value' => intval($qryCusPerMount[1]->cus_count)),
                            array('name' => 'ก.พ', 'value' => intval($qryCusPerMount[2]->cus_count)),
                            array('name' => 'มี.ค', 'value' => intval($qryCusPerMount[3]->cus_count)),
                            array('name' => 'เม.ย', 'value' => intval($qryCusPerMount[4]->cus_count)),
                            array('name' => 'พ.ค', 'value' => intval($qryCusPerMount[5]->cus_count)),
                            array('name' => 'มิ.ย', 'value' => intval($qryCusPerMount[6]->cus_count)),
                            array('name' => 'ก.ค', 'value' => intval($qryCusPerMount[7]->cus_count)),
                            array('name' => 'ส.ค', 'value' => intval($qryCusPerMount[8]->cus_count)),
                            array('name' => 'ก.ย', 'value' => intval($qryCusPerMount[9]->cus_count)),
                            array('name' => 'ต.ค', 'value' => intval($qryCusPerMount[10]->cus_count)),
                            array('name' => 'พ.ย', 'value' => intval($qryCusPerMount[11]->cus_count)),
                            array('name' => 'ธ.ค', 'value' => intval($qryCusPerMount[12]->cus_count))
                        )
                    )
                )
            )
        );
        echo json_encode($data);
    }

    public function getResById($cus_id) {
        $qryCustomer = $this->db
        ->select('id_res_auto,name,address,logo_url,expired_date,count_table,count_sub_group,p_pay,pay_before,creat_date,tel')
        ->get_where("restaurant", array('id_res_auto' => $cus_id))
        ->row();
        
        $qryCustomer->name = array_lang($qryCustomer->name, 'th');
        $qryCustomer->address = array_lang($qryCustomer->address, 'th');
        echo json_encode($qryCustomer);
    }

    public function getTransitionById($cus_id) {
        $qryCusPay = $this->db
                        ->select('*')
                        ->order_by("cre_date", 'desc')
                        ->get_where("res_payment", array('id_res_auto' => $cus_id, 'status' => 'confirm'))->result();
        echo json_encode($qryCusPay);
    }

    public function getResLogin($id)
    {
        $res = $this->db->select('code')->get_where('restaurant',array('id_res_auto'=>$id))->row();
        $data = array(
            'code'=> $res->code,
            'md5' => md5(rand(0,100))
        );
        echo json_encode($data);
    }

    public function addQuotation()
    {
        $p = _post();
        // start generate code process //
        $y = (date('Y')+543)-2500;
        $YM = 'QU'.$y.date('m');
        $generate = $this->db->select('quotation_id')->from('res_quotation_data')->like('quotation_queue',$YM)->get()->num_rows();
        if($generate <= 0){
            $code = $YM.'001';
        }else {
            $generate = $generate+1;
            if($generate > 0 && $generate < 10 ){
                $code = $YM.'00'.$generate;
            }else if($generate > 9  && $generate < 100 ){
                $code = $YM.'0'.$generate;
            }
            else{
                $code = $YM.$generate;
            }
        }
        // end generate code process //
        $contents = array();
        if($p->mount == 12){
            $contents = array(
                array(
                    'detail' => 'ระบบร้านอาหาร Deltafood 1 ปี ฟรี 1 เดือน (รวม 13 เดือน)',
                    'price' => $p->package
                )
            );
        }
        else{
            $contents = array(
                array(
                    'detail' => 'ระบบร้านอาหาร Deltafood '.$p->mount.' เดือน',
                    'price' => $p->package
                )
            );
        }
        $content = json_encode($contents);

        $dataQT = array(
            'id_res_auto' => $p->cus_id,
            'rent_mount' => $p->mount,
            'package' => $p->package,
            'type_res' => $p->type_cus,
            'tax_id' => $p->tax_id,
            'name' => $p->names,
            'type_address' => $p->type_address,
            'address' => $p->address,
            'quotation_queue' => $code,
            'quotation_date' => date('Y-m-d'),
            'quotation_exp_date' => date('Y-m-d', strtotime(date('Y-m-d'). ' + '.$p->day.' days')),
            'quotation_status' => 1,
            'id_res_code' => md5(date("sm")),
            'contents' => $content
        );
        $this->db->insert('res_quotation_data', $dataQT);

        $total = $p->package + ((($p->package * $p->mount) * 7) / 100); // ค่าใช้จ่ายทั้งหมด
        $dataPM = array(
            'cre_date' => date('Y-m-d H:i:h'),
            'status' => 'qu',
            'number' => $total,
            'id_res_auto' => $p->cus_id,
            'quotation_url' => 'https://docs.flowaccount.com/share/html/th/cnacfpusugmcydi85b1fg',
            'month_number' => $p->mount
        );
        $this->db->insert('res_payment', $dataPM);

        $error = $this->db->error(); 
        if($error['code'] != 0){
            echo json_encode(false);
            exit();
        }
        echo json_encode(true);
        exit();
    }
    
    public function getQuotation($cus_id = '',$page = 0)
    {
        $qu = $this->db->select('quotation_id,id_res_auto,quotation_queue,quotation_date,id_res_code')
        ->from('res_quotation_data')
        ->where('id_res_auto',$cus_id)
        ->order_by("quotation_id","DESC")
        ->limit(5,$page)
        ->get()
        ->result();

        $qu_count = $this->db->select('quotation_id')
        ->from('res_quotation_data')
        ->where('id_res_auto',$cus_id)
        ->get()
        ->num_rows();

        $data = array(
            'quotation' => $qu,
            'quotation_rec' => $qu_count
        );

        echo json_encode($data);
    }

    public function getQuotationLastById($cus_id = '')
    {
        $qtLast = $this->db
        ->limit(1)->order_by('quotation_id',"DESC")
        ->select('*')
        ->get_where('res_quotation_data',array('id_res_auto'=>$cus_id))
        ->row();
        echo json_encode($qtLast);
    }

}
