<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Video extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    public function getVedio()
    {
        $link = $this->db->select('*')->from('install_system_video')->order_by('video_group','ASC')->get()->result();
        echo json_encode($link);
    }
}
