<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Topping_use extends CI_Controller {

    public function index() {
        echo json_encode(array("a" => "ss4555"));
    }

    public function load_topping($f_id = '0') {
        $foods = $this->db->select('toppings')->get_where("foods", array('f_id' => $f_id, 'id_res_auto' => res_id()));
        if ($foods->num_rows() > 0) {
            $f_top = $foods->row()->toppings;
            if ($f_top != 'a:0:{}' && $f_top != '') {
                $t = unserialize($f_top);
         
                $this->db->where_in('tp_id',$t);
                $topping = $this->db->select("tp_id,title")->order_by("sort", 'asc')->get_where("topping", array('id_res_auto' => res_id(), 'del' => 0))->result();
                $topping_sub = $this->db->select('tps_id,tp_id,price,title')->order_by("sort", 'asc')->get_where("topping_sub", array('id_res_auto' => res_id(), 'del' => 0))->result();
                foreach ($topping_sub as $key => $value) {
                    $topping_sub[$key]->title = array_lang($value->title);
                    $topping_sub[$key]->price -= 0;
                }
                foreach ($topping as $key => $value) {
                    $topping[$key]->title = array_lang($value->title);
                    $topping[$key]->sub = array();
                    foreach ($topping_sub as $key2 => $value2) {
                        if ($value2->tp_id == $value->tp_id) {
                            array_push($topping[$key]->sub, $value2);
                        }
                    }
                }
//        pre($topping);
                echo json_encode($topping);
            } else {
                echo json_encode(array());
            }
        } else {
            echo json_encode(array());
        }
    }

}
