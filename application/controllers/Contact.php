<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    public function listContact() {
        $p = _post();
        $qryListContact = $this->db
                ->select('id_res_auto, name , logo_url, saler_id')
                ->get_where('restaurant', array('saler_id' => $p->s_id))
                ->result();
        /** loop เพื่อนำเอาค่า name ภาษาไทยออกมา */
        foreach ($qryListContact as $value) {
            $value->name = array_lang($value->name, 'th');
            $qryListContactCount = $this->db
            ->select('COUNT(c_id) as count_id')
            ->get_where('chat', array('id_res_auto' => $value->id_res_auto,'sale_id' => 0, 'readed' => 0))->row();
            $value->dm_count = $qryListContactCount->count_id;
        }

        echo json_encode($qryListContact);
    }

    public function listContacts($s_id) {
        $qryListContacts = $this->db
                ->select('id_res_auto, name , logo_url, saler_id')
                ->get_where('restaurant', array('saler_id' => $s_id))
                ->result();
        /** loop เพื่อนำเอาค่า name ภาษาไทยออกมา */
        foreach ($qryListContacts as $value) {
            $value->name = array_lang($value->name, 'th');
            $qryListContactCounts = $this->db
            ->select('COUNT(c_id) as count_id')
            ->get_where('chat', array('id_res_auto' => $value->id_res_auto,'sale_id' => 0, 'readed' => 0))->row();
            $value->dm_count = $qryListContactCounts->count_id;
        }

        echo json_encode($qryListContacts);
    }

    public function add_chat() {
        $p = _post();
        $p->cre_date = date("Y-m-d H:i:s");
        $p->by_id = 0;
        echo $this->db->insert("chat",$p);
    }

    public function read_chat()
    {
        $p = _post();
        $qryRead = $this->db
        ->query('UPDATE chat SET readed = 1 WHERE id_res_auto = '.$p->id_res_auto.' AND sale_id = 0');
    }

    public function historyDM($user_id, $contact_id) {

        $qryhistoryDM = $this->db
                        ->select('c.*,r.logo_url')
                        ->from('chat as c')
                        ->join('restaurant as r','r.id_res_auto = c.id_res_auto')
                        ->where('c.id_res_auto = ' . $contact_id . '')
                        ->order_by('c_id', 'DESC')
                        ->limit(20)
                        ->get()->result();
        echo json_encode($qryhistoryDM);
    }

}
