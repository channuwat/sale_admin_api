<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bookbank extends CI_Controller {

    function __construct() {

        parent::__construct();
    }

    public function getMyBank()
    {   
        $p =  _post();
        $qryBank = $this->db
        ->select('*')
        ->get_where('sale_bookbank',array('sale_id' => $p->user))->result();

        echo json_encode($qryBank);
    }

    public function updateBookbank()
    {
        $p =  _post();
        $updateArry = array(
            'sb_ref_id' => $p->bank,
            'sb_account' => $p->num_bank,
        );
        $update = $this->db
        ->where('sale_id',$p->s_id)
        ->update('sale_bookbank', $updateArry);
        if($update){
            echo json_encode(true);
        }
        
    }
}
