<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Topping extends CI_Controller {

    public function index() {
        echo json_encode(array("a" => "ss4555"));
    }

    public function load_topping() {
        $topping = $this->db->order_by("sort", 'asc')->get_where("topping", array('id_res_auto' => res_id(), 'del' => 0))->result();
        foreach ($topping as $key => $value) {
            $topping[$key]->title = get_name_show($value->title);
            $topping[$key]->sub = $this->db->order_by('sort', 'asc')->get_where("topping_sub", array('tp_id' => $value->tp_id, 'id_res_auto' => res_id(), 'del' => 0))->result();
            foreach ($topping[$key]->sub as $key2 => $value2) {
                $topping[$key]->sub[$key2]->title = get_name_show($value2->title);
            }
        }
        echo json_encode($topping);
//        pre($topping);
    }

    public function load_toppings() {
        $topping = $this->db->order_by('ts.sort', 'asc')->select("ts.*,t.title as group")->join("topping t", 't.tp_id=ts.tp_id')->get_where("topping_sub ts", array('ts.id_res_auto' => res_id(), 'ts.del' => 0))->result();
        foreach ($topping as $key2 => $value2) {
            if ($value2->stock == '') {
                $value2->stock = array();
            } else {
                $value2->stock = unserialize($value2->stock);
            }
            $topping[$key2]->title = get_name_show($value2->title);
            $topping[$key2]->group = array_lang($value2->group);
            $topping[$key2]->edit = 0;
            $topping[$key2]->sort -= 0;
            $topping[$key2]->price -= 0;
        }
        echo json_encode($topping);
    }

    public function remove_group() {
        $p = _post();
//        pre($p);
        echo $this->db->update("topping", array('del' => 1), array('tp_id' => $p->tp_id));
    }

    public function add_group() {
        $p = _post();
//        pre($p);
        foreach ($p->data as $val) {
            if ($val->tp_id - 0 == 0) {
                echo $this->db->insert("topping", array('id_res_auto' => res_id(), 'sort' => $val->sort, 'title' => mullti_lang_todb($val->title), 'del' => 0));
            } else {
                $id = $val->tp_id;
                unset($val->tp_id);
                echo $this->db->update('topping', array('title' => mullti_lang_todb($val->title), 'sort' => $val->sort), array('id_res_auto' => res_id(), 'tp_id' => $id));
            }
        }
    }

    public function add_topping() {
        $p = _post();
//        pre($p);
        foreach ($p->data as $value) {
            if ($value->tps_id - 0 == 0) {
                $this->db->insert("topping_sub", array('tp_id' => $value->tp_id, 'id_res_auto' => res_id(), 'price' => $value->price, 'del' => 0, 'sort' => $value->sort, 'title' => mullti_lang_todb($value->title)));
            } else {
                $id = $value->tps_id;
                unset($value->tps_id);
                $this->db->update("topping_sub", array('tp_id' => $value->tp_id, 'price' => $value->price, 'sort' => $value->sort, 'title' => mullti_lang_todb($value->title)), array('id_res_auto' => res_id(), 'tps_id' => $id));
            }
        }
        echo 1;
    }

    public function load_toppings_group() {
        $topping = $this->db->order_by("sort", 'asc')->get_where("topping", array('id_res_auto' => res_id(), 'del' => 0))->result();
        foreach ($topping as $key => $value) {
            $title = $value->title;
            $topping[$key]->edit = 0;
            $topping[$key]->title = get_name_show($title);
            $topping[$key]->name = array_lang($title);
            $topping[$key]->sort -= 0;
        }
        echo json_encode($topping);
    }

    public function remove_tps() {
        $p = _post();
//        pre($p);
        echo $this->db->update("topping_sub", array('del' => 1), array('tps_id' => $p->tps_id, 'id_res_auto' => res_id()));
    }

    public function remove_tp() {
        $p = _post();
//        pre($p);
        echo $this->db->update("topping", array('del' => 1), array('tp_id' => $p->tp_id, 'id_res_auto' => res_id()));
    }

    public function save_tps() {
        $p = _post();
        if ($p->tps_id - 0 == 0) {
//            pre($p);
            echo $this->db->insert("topping_sub", array('tp_id' => $p->tp_id, 'id_res_auto' => res_id(), 'price' => $p->price, 'del' => 0, 'sort' => $p->sort, 'title' => mullti_lang_todb($p->title)));
        } else {
            $id = $p->tps_id;
            unset($p->tps_id);
            echo $this->db->update("topping_sub", array('tp_id' => $p->tp_id, 'price' => $p->price, 'sort' => $p->sort, 'title' => mullti_lang_todb($p->title)), array('id_res_auto' => res_id(), 'tps_id' => $id));
        }
    }

    public function save_tp() {
        $p = _post();
        if ($p->tp_id - 0 == 0) {
            echo $this->db->insert("topping", array('id_res_auto' => res_id(), 'sort' => $p->sort, 'title' => mullti_lang_todb($p->title), 'del' => 0));
        } else {
            $id = $p->tp_id;
            unset($p->tp_id);
//            pre($p);
            echo $this->db->update('topping', array('title' => mullti_lang_todb($p->title), 'sort' => $p->sort), array('id_res_auto' => res_id(), 'tp_id' => $id));
        }
    }

}
