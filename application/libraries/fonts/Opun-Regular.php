<?php
$type = 'TrueType';
$name = 'Opun-Regular';
$desc = array('Ascent'=>778,'Descent'=>-222,'CapHeight'=>778,'Flags'=>32,'FontBBox'=>'[-702 -784 1346 1433]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>364);
$up = -158;
$ut = 38;
$cw = array(
	chr(0)=>364,chr(1)=>364,chr(2)=>364,chr(3)=>364,chr(4)=>364,chr(5)=>364,chr(6)=>364,chr(7)=>364,chr(8)=>364,chr(9)=>364,chr(10)=>364,chr(11)=>364,chr(12)=>364,chr(13)=>364,chr(14)=>364,chr(15)=>364,chr(16)=>364,chr(17)=>364,chr(18)=>364,chr(19)=>364,chr(20)=>364,chr(21)=>364,
	chr(22)=>364,chr(23)=>364,chr(24)=>364,chr(25)=>364,chr(26)=>364,chr(27)=>364,chr(28)=>364,chr(29)=>364,chr(30)=>364,chr(31)=>364,' '=>239,'!'=>224,'"'=>444,'#'=>1024,'$'=>650,'%'=>963,'&'=>674,'\''=>202,'('=>383,')'=>383,'*'=>520,'+'=>657,
	','=>239,'-'=>547,'.'=>224,'/'=>496,'0'=>672,'1'=>290,'2'=>650,'3'=>672,'4'=>635,'5'=>672,'6'=>672,'7'=>584,'8'=>672,'9'=>672,':'=>224,';'=>239,'<'=>537,'='=>657,'>'=>537,'?'=>655,'@'=>1083,'A'=>643,
	'B'=>701,'C'=>721,'D'=>823,'E'=>660,'F'=>660,'G'=>762,'H'=>797,'I'=>268,'J'=>508,'K'=>660,'L'=>660,'M'=>909,'N'=>797,'O'=>850,'P'=>672,'Q'=>850,'R'=>660,'S'=>650,'T'=>652,'U'=>797,'V'=>643,'W'=>1161,
	'X'=>652,'Y'=>652,'Z'=>652,'['=>312,'\\'=>496,']'=>312,'^'=>555,'_'=>547,'`'=>293,'a'=>638,'b'=>638,'c'=>574,'d'=>638,'e'=>630,'f'=>462,'g'=>638,'h'=>643,'i'=>261,'j'=>261,'k'=>613,'l'=>261,'m'=>985,
	'n'=>643,'o'=>630,'p'=>638,'q'=>638,'r'=>420,'s'=>520,'t'=>386,'u'=>643,'v'=>545,'w'=>931,'x'=>528,'y'=>545,'z'=>562,'{'=>422,'|'=>261,'}'=>422,'~'=>638,chr(127)=>364,chr(128)=>821,chr(129)=>364,chr(130)=>364,chr(131)=>364,
	chr(132)=>364,chr(133)=>779,chr(134)=>364,chr(135)=>364,chr(136)=>364,chr(137)=>364,chr(138)=>364,chr(139)=>364,chr(140)=>364,chr(141)=>364,chr(142)=>364,chr(143)=>364,chr(144)=>364,chr(145)=>239,chr(146)=>239,chr(147)=>481,chr(148)=>481,chr(149)=>418,chr(150)=>657,chr(151)=>735,chr(152)=>364,chr(153)=>364,
	chr(154)=>364,chr(155)=>364,chr(156)=>364,chr(157)=>364,chr(158)=>364,chr(159)=>364,chr(160)=>364,chr(161)=>728,chr(162)=>669,chr(163)=>682,chr(164)=>728,chr(165)=>806,chr(166)=>709,chr(167)=>660,chr(168)=>669,chr(169)=>684,chr(170)=>669,chr(171)=>682,chr(172)=>1151,chr(173)=>1151,chr(174)=>748,chr(175)=>748,
	chr(176)=>669,chr(177)=>709,chr(178)=>1200,chr(179)=>1151,chr(180)=>709,chr(181)=>775,chr(182)=>728,chr(183)=>728,chr(184)=>606,chr(185)=>728,chr(186)=>728,chr(187)=>728,chr(188)=>806,chr(189)=>806,chr(190)=>843,chr(191)=>843,chr(192)=>748,chr(193)=>728,chr(194)=>709,chr(195)=>599,chr(196)=>728,chr(197)=>731,
	chr(198)=>748,chr(199)=>669,chr(200)=>728,chr(201)=>728,chr(202)=>731,chr(203)=>728,chr(204)=>843,chr(205)=>677,chr(206)=>677,chr(207)=>721,chr(208)=>444,chr(209)=>0,chr(210)=>542,chr(211)=>542,chr(212)=>0,chr(213)=>0,chr(214)=>0,chr(215)=>0,chr(216)=>0,chr(217)=>0,chr(218)=>0,chr(219)=>364,
	chr(220)=>364,chr(221)=>364,chr(222)=>364,chr(223)=>701,chr(224)=>264,chr(225)=>533,chr(226)=>354,chr(227)=>349,chr(228)=>347,chr(229)=>542,chr(230)=>775,chr(231)=>0,chr(232)=>0,chr(233)=>0,chr(234)=>0,chr(235)=>0,chr(236)=>0,chr(237)=>0,chr(238)=>0,chr(239)=>804,chr(240)=>674,chr(241)=>674,
	chr(242)=>777,chr(243)=>684,chr(244)=>652,chr(245)=>652,chr(246)=>630,chr(247)=>973,chr(248)=>684,chr(249)=>828,chr(250)=>997,chr(251)=>1391,chr(252)=>364,chr(253)=>364,chr(254)=>364,chr(255)=>364);
$enc = 'cp874';
$diff = '130 /.notdef /.notdef /.notdef 134 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 142 /.notdef 152 /.notdef /.notdef /.notdef /.notdef /.notdef 158 /.notdef /.notdef 161 /kokaithai /khokhaithai /khokhuatthai /khokhwaithai /khokhonthai /khorakhangthai /ngonguthai /chochanthai /chochingthai /chochangthai /sosothai /chochoethai /yoyingthai /dochadathai /topatakthai /thothanthai /thonangmonthothai /thophuthaothai /nonenthai /dodekthai /totaothai /thothungthai /thothahanthai /thothongthai /nonuthai /bobaimaithai /poplathai /phophungthai /fofathai /phophanthai /fofanthai /phosamphaothai /momathai /yoyakthai /roruathai /ruthai /lolingthai /luthai /wowaenthai /sosalathai /sorusithai /sosuathai /hohipthai /lochulathai /oangthai /honokhukthai /paiyannoithai /saraathai /maihanakatthai /saraaathai /saraamthai /saraithai /saraiithai /sarauethai /saraueethai /sarauthai /sarauuthai /phinthuthai /.notdef /.notdef /.notdef /.notdef /bahtthai /saraethai /saraaethai /saraothai /saraaimaimuanthai /saraaimaimalaithai /lakkhangyaothai /maiyamokthai /maitaikhuthai /maiekthai /maithothai /maitrithai /maichattawathai /thanthakhatthai /nikhahitthai /yamakkanthai /fongmanthai /zerothai /onethai /twothai /threethai /fourthai /fivethai /sixthai /seventhai /eightthai /ninethai /angkhankhuthai /khomutthai /.notdef /.notdef /.notdef /.notdef';
$uv = array(0=>array(0,128),128=>8364,133=>8230,145=>array(8216,2),147=>array(8220,2),149=>8226,150=>array(8211,2),160=>160,161=>array(3585,58),223=>array(3647,29));
$file = 'Opun-Regular.z';
$originalsize = 41776;
$subsetted = true;
?>
