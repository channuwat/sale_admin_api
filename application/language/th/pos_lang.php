<?php

$lang['notknow'] = "ไม่ทราบ";
$lang['fine'] = "ค่าปรับ";
$lang['change'] = "เงินทอน";
$lang["day"] = "วัน";
$lang['free'] = "ฟรี";
$lang['ago'] = "ที่ผ่านมา";
$lang['moment'] = "เมื่อซักครู่";
$lang['year'] = "ปี";
$lang['month'] = "เดือน";
$lang['week'] = "สัปดาห์";
$lang['all_group'] ="ทุกกลุ่ม";
$lang['second'] ="วินาที";




$lang['receipt'] = "ใบเสร็จรับเงิน";
$lang['date'] = "วันที่";
$lang['table'] = "โต๊ะ";
$lang['emp'] = "พนักงาน";
$lang['menu_goods'] = "รายการ";
$lang['number'] = "จำนวน";
$lang['price'] = "ราคา";
$lang['buffet'] = "บุบเฟต์";
$lang['Resive'] = "รับเงิน";
$lang['change'] = "เงินทอน";
$lang['thank'] = "ขอบคุณที่ใช้บริการ";
$lang['type_of_payment'] = "ประเภท";
$lang['cannot_find_bill'] = "ไม่มีบิลนี้";
$lang['food_bill'] = "ใบแจ้งค่าอาหาร";
$lang['net_price'] = "ราคาสุทธิ";
$lang['price_house'] = "ค่าชั่วโมง";
$lang['end_food_bill'] = "กรุณาตรวจสอบความถูกต้อง<br> หากผิดพลาดหรือไม่ถูกต้อง<br>ให้แจ้งเจ้าหน้าที่ทันที<br>ขอบคุณค่ะ";
$lang['w_1'] = "ไม่สามารถยกเลิกได้เนื่องจากได้ทำแล้ว";
$lang['e_1'] = "มีปัญหาในการลบสินค้า";
$lang['wait'] = "รอซักครู่";
$lang['e_2'] = "กรุณาเชื่อต่อ Wifi ทางร้านแล้วแสกน QRcode ใหม่อีกครั้ง";
$lang['e_3'] = "ไม่พอข้อมูล";
$lang['re_01'] = "รายงานยอดขายวันที่";
$lang['summery'] = "ยอดขายรวม";
$lang['to'] = "ถึง";
$lang['not_set'] = "ไม่ระบุ";
$lang["not_producer"] = "ไม่ทราบผู้ผลิต";
$lang['order_1'] = "สั่งซื้อ";
$lang['producer'] = "ผู้ผลิต";
$lang['note'] = "หมายเหตุ";
$lang['date'] = "วันที่";
$lang['items'] = "รายการ";
$lang['total'] = "ราคารวม";
$lang['call_emp'] = "เรียกพนักงาน";
$lang['coll_checkbill'] = "เรียกเช็คบิล";
$lang['form_emp'] = "จากพนักงาน";
$lang['q_number'] = "หมายเลคิว";
$lang['waiting'] = "รออีก";
$lang['seat_number'] = "จำนวนที่นั่ง";
$lang['unit_people'] = "ท่าน";
$lang['date_time'] = "วันที่และเวลา";
$lang['bottom_q'] = "  ขอสงวนสิทธิ์ในการข้ามคิว<br> ในกรณีลูกค้าไม่แสดงตน";
$lang['start_price'] = "ราคาเริ่มต้น";
$lang['cus'] = "ลูกค้า";
$lang['open_table'] = "ค่าเปิดโต๊ะ";
$lang['fine'] = "ค่าปรับ";
$lang['money_1'] = "จำนวนเงิน";
$lang['re_02'] = "รายงานแยกตามสินค้า วันที่";
$lang['re_03'] = "รายงาน 10 สินค้าขายดี วันที่ ";
$lang['bill'] = "บิล";
$lang['cash'] = "เงินสด";
$lang['sum_all'] = "รวมทั้งหมด";
$lang['number_order'] = "จำนวนออเดอร์";
$lang['expenditure'] = "รายจ่าย";
$lang['sale_m_p'] = "ซื้อวัตถุดิบ&สินค้า";
$lang['group_year'] = "แยกเป็นรายปี";
$lang['group_month'] = "แยกเป็นรายเดือน";
$lang['per_login'] = "ไม่อนุญาติให้เข้าใช้งานระบบ";
$lang['login_error'] = "ชื่อผู้ใช้ หรือ รหัสผ่าน ไม่ถูกต้อง";
$lang['hr'] = "ชม.";
$lang['minute'] = "นาที";
$lang['price_food_total'] = "ค่าอาหารรวม";
$lang['cooking'] = "ทำอาหาร";
$lang['serve'] = "เสริฟอาหาร";
$lang['cencel_order'] = "ยกเลิกออเดอร์";
$lang['re_04'] = "รายงานการทำพนักงาน";


